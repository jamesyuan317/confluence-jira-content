package it.com.atlassian.confluence.plugins.jiracreate.pageobjects;

import com.atlassian.confluence.pageobjects.component.ConfluenceAbstractPageComponent;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedQuery;

public class FeatureDiscoveryDialog extends ConfluenceAbstractPageComponent
{
    @ElementBy(id = "inline-dialog-create-issue-feature-discovery-dialog")
    private PageElement featureDiscoveryDialog;

    @ElementBy(id = "show-create-issue")
    private PageElement showCreateIssueButton;

    @ElementBy(id = "feature-discovery-close")
    private PageElement closeDialogButton;

    public TimedQuery<Boolean> isFeatureDiscoveryDialogVisible()
    {
        return featureDiscoveryDialog.timed().isVisible();
    }

    public void close()
    {
        closeDialogButton.click();
    }

    public CreateIssueDialog showCreateIssueDialog()
    {
        showCreateIssueButton.click();
        CreateIssueDialog createIssueDialog = pageBinder.bind(CreateIssueDialog.class);
        return createIssueDialog;
    }
}
