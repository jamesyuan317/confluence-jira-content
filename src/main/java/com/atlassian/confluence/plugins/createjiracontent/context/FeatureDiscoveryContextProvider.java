package com.atlassian.confluence.plugins.createjiracontent.context;

import java.util.Map;

import com.atlassian.confluence.plugins.createjiracontent.services.FeatureDiscoveryService;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import com.google.common.collect.Maps;

/**
 * TODO: copied implementation from https://stash.atlassian.com/projects/CONF/repos/confluence-jira-metadata/pull-requests/28/overview
 * remove it when have generic feature discovery service:  https://jira.atlassian.com/browse/CONFDEV-21132
 */
public class FeatureDiscoveryContextProvider implements ContextProvider
{
    private final FeatureDiscoveryService featureDiscoveryService;

    public FeatureDiscoveryContextProvider(FeatureDiscoveryService featureDiscoveryService)
    {
        this.featureDiscoveryService = featureDiscoveryService;
    }

    @Override
    public void init(Map<String, String> params) throws PluginParseException
    {
    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> params)
    {
        Map<String, Object> context = Maps.newHashMap();
        ConfluenceUser user = (ConfluenceUser) params.get("user");
        if (user != null)
        {
            context.put("shouldShowDiscovery", !featureDiscoveryService.hasUserDiscovered(user));
        }
        else
        {
            context.put("shouldShowDiscovery", false);
        }

        return context;
    }
}