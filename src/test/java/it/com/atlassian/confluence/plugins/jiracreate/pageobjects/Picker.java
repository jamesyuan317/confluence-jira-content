package it.com.atlassian.confluence.plugins.jiracreate.pageobjects;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.By;

import com.atlassian.confluence.pageobjects.component.ConfluenceAbstractPageComponent;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;

public class Picker extends ConfluenceAbstractPageComponent
{
    @ElementBy(id = "inline-dialog-create-issue-dialog")
    protected PageElement createIssueDialog;
    @ElementBy(id = "select2-drop")
    protected PageElement select2Dropdown;

    private String pickerId;

    public void bindingElements(String pickerId)
    {
        this.pickerId = pickerId;
        //select2Element = ;
    }

    public Picker openDropdown()
    {
        createIssueDialog.find(By.cssSelector("#s2id_" + pickerId + " .select2-choice")).click();
        Poller.waitUntilTrue(select2Dropdown.timed().isVisible());
        return this;
    }

    public void selectTheFirstOption()
    {
        PageElement firstOption = select2Dropdown.find(By.cssSelector(".select2-results > li"));
        firstOption.click();
    }

    public void chooseOption(String value)
    {
        select2Dropdown.click();
        List<PageElement> options = select2Dropdown.findAll(By.cssSelector(".select2-results > li"));
        for (PageElement option : options)
        {
            if(option.getText().equals(value))
            {
                option.click();
                break;
            }
        }
    }

    public List<String> getAllValues()
    {
        List<String> values = new ArrayList<String>();
        List<PageElement> options = select2Dropdown.findAll(By.cssSelector(".select2-results > li"));
        for (PageElement option : options)
        {
            values.add(option.getText());
        }
        return values;
    }

    public String getSelectedValue()
    {
        return getSelectedOption().getText();
    }

    public boolean isDisabled()
    {
        return createIssueDialog.find(By.cssSelector("#s2id_" + pickerId)).hasClass("select2-container-disabled");
    }

    public Picker waitUntilEnable()
    {
        Poller.waitUntil(createIssueDialog.find(By.cssSelector("#s2id_" + pickerId)).timed().getAttribute("class"), Matchers.not(Matchers.containsString("select2-container-disabled")),
                         Poller.by(20000));
        return this;
    }

    public PageElement getSelectedOption()
    {
        return createIssueDialog.find(By.cssSelector("#s2id_" + pickerId + " .select2-choice > .select2-chosen"));
    }

}
