package com.atlassian.confluence.plugins.createjiracontent;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.atlassian.confluence.plugins.createjiracontent.rest.beans.CachableJiraServerBean;

public class JIRASupportedConditionTest extends TestCase
{
    private static final String PARAM_BUILD_NUMBER = "minimumBuildNumber";

    private static final Long DEFAULT_MIN_BUILD_NUMBER = 770L;

    @Mock private JiraResourcesManager jiraResourcesManager;

    private CachableJiraServerBean server;

    private JIRASupportedCondition jiraSupportedCondition;

    protected void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);

        server = new CachableJiraServerBean("39d02cfc-ce59-31a8-81ab-621294388590",
                "http://localhost:2991/jira", "Your Company JIRA", true, "", 770L);
        jiraSupportedCondition = new JIRASupportedCondition(jiraResourcesManager);

        Map<String, String> params = new HashMap<String, String>();
        params.put(PARAM_BUILD_NUMBER, String.valueOf(DEFAULT_MIN_BUILD_NUMBER));
        jiraSupportedCondition.init(params);
}

    public void testInit()
    {
        Long buildNumber = 711L;
        Map<String, String> params = new HashMap<String, String>();
        params.put(PARAM_BUILD_NUMBER, String.valueOf(buildNumber));

        jiraSupportedCondition.init(params);
    }

    public void testShouldDisplayNoServerSupported()
    {
        Map<String, Object> params = new HashMap<String, Object>();
        List<CachableJiraServerBean> servers = new ArrayList<CachableJiraServerBean>();

        when(jiraResourcesManager.getSupportedJiraServers()).thenReturn(servers);

        assertFalse(jiraSupportedCondition.shouldDisplay(params));
    }

    public void testShouldDisplayHasSupportedServer()
    {
        Map<String, Object> params = new HashMap<String, Object>();
        List<CachableJiraServerBean> servers = new ArrayList<CachableJiraServerBean>();
        servers.add(server);

        when(jiraResourcesManager.getSupportedJiraServers()).thenReturn(servers);

        assertTrue(jiraSupportedCondition.shouldDisplay(params));
    }

}
