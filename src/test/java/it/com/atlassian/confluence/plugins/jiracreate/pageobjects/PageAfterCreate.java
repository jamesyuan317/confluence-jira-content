package it.com.atlassian.confluence.plugins.jiracreate.pageobjects;

import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.atlassian.confluence.pageobjects.component.ConfluenceAbstractPageComponent;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;

public class PageAfterCreate extends ConfluenceAbstractPageComponent {

    @ElementBy(cssSelector = "div.jira-issues-created p.title")
    protected PageElement msgPagragraph;

    @ElementBy(cssSelector = "div.jira-issues-created p.title a")
    protected PageElement issuesLink;

    @ElementBy(cssSelector = "div.warning.jira-issues-created")
    protected PageElement warningMsgPanel;

    @ElementBy(cssSelector = "div.success.jira-issues-created")
    protected PageElement successMsgPanel;
    
    public String getMsg() {
        return msgPagragraph.getText();
    }

    public String getCreatedIssueKey()
    {
        Pattern issuePattern = Pattern.compile("([\\w]+-[\\d]+)");
        String resultText = msgPagragraph.getText();
        Matcher matcher = issuePattern.matcher(resultText);
        if (matcher.find()) {
            return matcher.group(1);
        }

        return null;
    }

    public String[] getCreatedIssueKeys()
    {
        Pattern issuePattern = Pattern.compile("jql=key%20in%20\\(([^)]*)\\)");
        String link = issuesLink.getAttribute("href");
        Matcher matcher = issuePattern.matcher(link);
        if (matcher.find()) {
            return matcher.group(1).split("%2C");
        }

        return new String[]{};
    }

    public boolean isMsgPanelPresent()
    {
        return msgPagragraph.timed().isPresent().by(30, TimeUnit.SECONDS);
    }

    public boolean isMsgPanelPresentLongWait()
    {
        return msgPagragraph.timed().isPresent().by(5, TimeUnit.MINUTES);
    }
    
    public boolean isWarningMsgPanelDisplay() 
    {
        return warningMsgPanel.isVisible();
    }

    public boolean isWarningMsgPanelVisible()
    {
        return warningMsgPanel.isVisible();
    }
    
    public boolean isSuccessMsgPanelDisplay()
    {
        return successMsgPanel.isVisible();
    }

    public void waitUntilWarningMsgPanelPresent()
    {
        Poller.waitUntilTrue(warningMsgPanel.timed().isPresent());
    }

    public void waitUntilSuccessMsgPanelPresent()
    {
        Poller.waitUntilTrue(successMsgPanel.timed().isPresent());
    }
}