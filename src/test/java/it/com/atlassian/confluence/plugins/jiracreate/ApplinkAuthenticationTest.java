package it.com.atlassian.confluence.plugins.jiracreate;

import it.com.atlassian.confluence.plugins.jiracreate.pageobjects.CreateIssueDialog;
import it.com.atlassian.confluence.plugins.jiracreate.rest.CreateJiraContentRestHelper;

import java.io.IOException;

import junit.framework.Assert;

import org.apache.commons.httpclient.HttpException;
import org.json.JSONException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.User;

public class ApplinkAuthenticationTest extends AbstractJIRACreateTest
{
    @Before
    public void setUp() throws Exception
    {
        CreateJiraContentRestHelper.setDiscoveredred(rpc.getBaseUrl(), User.ADMIN);
        applinkHelper.removeAllApplink();
        applinkHelper.setupOAuthAndActivate();
        product.loginAndView(User.ADMIN, Page.TEST);
    }
    
    @Test
    public void createIssueWithOAuth()
    {
        CreateIssueDialog dialog = openCreateIssueDialog("#main-content>p", 0, 5);
        Assert.assertTrue(dialog.getJiraIssueMessage().isOAuthButtonAvailable());

        dialog.getJiraIssueMessage().authorizedOauthentication();
        String message = dialog.getJiraIssueMessage().getAuthIssueMessage();
        Assert.assertFalse(message.contains("Allow access"));

    }
    
    @Test
    public void createIssueWithWrongJiraServer() throws HttpException, IOException, JSONException
    {
        applinkHelper.removeAllApplink();
        String badJiraBaseServer = "http://localhost:11990/jira2"; //just a wrong jira server
        ApplinkHelper appLinkHelperUnreachable = new ApplinkHelper(User.ADMIN, product.getProductInstance().getBaseUrl(), badJiraBaseServer);
        appLinkHelperUnreachable.setupBasicAndActivate();
        
        CreateIssueDialog dialog = openCreateIssueDialog("#main-content>p", 0, 5);
        String message = dialog.getJiraIssueMessage().getJiraConnectivityMessage();
        Assert.assertEquals("The version of selected JIRA server is not supported. You may want to upgrade to the newer version or create issue in JIRA.", message);
    }
    @After
    public void restoreApplicationLink() throws HttpException, IOException, JSONException
    {
        applinkHelper.removeAllApplink();
        applinkHelper.setupBasicAndActivate();
    }
    

}
