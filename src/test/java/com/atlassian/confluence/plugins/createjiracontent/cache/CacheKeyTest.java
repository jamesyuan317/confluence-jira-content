package com.atlassian.confluence.plugins.createjiracontent.cache;

import junit.framework.TestCase;

public class CacheKeyTest extends TestCase
{
    private static final String APP_ID_1 = "7d715c83-b5ce-39d3-a175-976aac6540ad";

    private static final String APP_ID_2 = "39d02cfc-ce59-31a8-81ab-621294388590";

    public void testSameAppId() {
        CacheKey key1 = new CacheKey(APP_ID_1);
        CacheKey key2 = new CacheKey(APP_ID_1);
        assertTrue(key1.equals(key2));
        assertTrue(key1.hashCode()==key2.hashCode());
    }

    public void testDifferentAppIds() {
        CacheKey key1 = new CacheKey(APP_ID_1);
        CacheKey key2 = new CacheKey(APP_ID_2);
        assertFalse(key1.equals(key2));
        assertFalse(key1.hashCode()==key2.hashCode());
    }

}
