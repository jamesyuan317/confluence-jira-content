/*
 * Facade service for analytics event handling
 * */

Confluence.CreateJiraContent.Analytics = (function($) {
    // how many people are discovering the feature/using it
    var OPEN_CREATE_ISSUE_DIALOG = 'confluence.jira.content.dialog.open';

    // If more users create from table, we should invest time in improving the table experience
    // make it possible to create from sorted tables and select the table column
    var CREATE_ISSUE_FROM_TEXT = 'confluence.jira.content.create.text';   // server side
    var CREATE_ISSUE_FROM_TABLE = 'confluence.jira.content.create.table'; // server side

    // errors: can't insert JIM, page being edited, can't insert all issues,
    var CREATE_ISSUE_ERROR = 'confluence.jira.content.error';              // server side
    // Is the algorithm correct, is there some unexpected content that is making the insertion fail
    var CREATE_ISSUE_JIM_INSERT_FAILED = 'confluence.jira.content.jim.insert.fail';

    // Do people switch between the two often. Should create from table be the default when highlighting in a table
    var CREATE_ISSUE_SWITCH_TO_TABLE = 'confluence.jira.content.switch.to.table';
    var CREATE_ISSUE_SWITCH_TO_TEXT = 'confluence.jira.content.switch.to.text';

    // Is the description field necessary, is it used aside from overflow for the summary
    var CREATE_ISSUE_EDIT_DESCRIPTION = 'confluence.jira.content.edit.description';

    // Do people consistently create user stories that are too long, work out a better system of informing the user
    // of the maximum length
    var CREATE_ISSUE_SUMMARY_TRUNCATED = 'confluence.jira.content.summary.truncated';

    // How successful is the integration between Confluence and JIRA Agile, should we invest more time into this
    var CREATE_ISSUE_WITH_EPIC = 'confluence.jira.content.with.epic';

    // How often do we encounter a warning about required fields, is this a common use case. Do we need to improve it
    var CREATE_ISSUE_REQUIRED_FIELDS = 'confluence.jira.content.required.fields';

    // Users can't create an issue using Create JIRA issue dialog because there are required fields with no default value
    var CREATE_ISSUE_CREATE_DIRECTLY_IN_JIRA = 'confluence.jira.content.create.directly.in.jira';

    // If many people encounter a problem with sorted tables, will invest time in a better solution
    var CREATE_ISSUE_IN_SORTED_TABLE = 'confluence.jira.content.sorted.table';

    // TO DO: log event when required fields prevent user from creating an issue and user clicks create in JIRA link
    // If this happens frequently, need to invest more time in required fields

    var TABLE_MODE = "table";

    function triggerEvent(name, properties) {
        AJS.EventQueue = AJS.EventQueue || [];
        AJS.EventQueue.push({
            name: name,
            properties: properties
        });
    }

    function sendAnalyticsForDialogOpen() {
        triggerEvent(OPEN_CREATE_ISSUE_DIALOG);
    }

    function sendAnalyticsForModeChange(mode) {
        triggerEvent(((mode === TABLE_MODE) ? CREATE_ISSUE_SWITCH_TO_TABLE : CREATE_ISSUE_SWITCH_TO_TEXT));
    }

    function sendAnalyticsForCreatingIssue(mode, total) {
        if (mode === TABLE_MODE) {
            var properties = {
                total: total
            };
            triggerEvent(CREATE_ISSUE_FROM_TABLE, properties);
        } else {
            triggerEvent(CREATE_ISSUE_FROM_TEXT);
        }
    }

    function sendAnalyticsForDescriptionEdited() {
        triggerEvent(CREATE_ISSUE_EDIT_DESCRIPTION);
    }

    function sendAnalyticsForCreateIssueWithEpic() {
        triggerEvent(CREATE_ISSUE_WITH_EPIC);
    }

    function sendAnalyticsForTruncatedSummary(total) {
        var properties = {
            total: total
        };
        triggerEvent(CREATE_ISSUE_SUMMARY_TRUNCATED, properties);
    }

    function sendAnalyticsForJIMInsertFailed(type, reason) {
        var properties = {
            type: type,
            reason: reason
        };
        triggerEvent(CREATE_ISSUE_JIM_INSERT_FAILED, properties);
    }

    function sendAnalyticsForSortedTable() {
        triggerEvent(CREATE_ISSUE_IN_SORTED_TABLE);
    }

    function sendAnalyticsForCreateError(cause) {
        var parameters = {
            cause: cause
        };
        triggerEvent(CREATE_ISSUE_ERROR, parameters);
    }

    function sendAnalyticsForRequiredFields() {
        triggerEvent(CREATE_ISSUE_REQUIRED_FIELDS);
    }

    function bindAnalyticsForCreateDirectlyInJira() {
        $('#create-issue-in-jira-link').click(function(e) {
            triggerEvent(CREATE_ISSUE_CREATE_DIRECTLY_IN_JIRA);
        });
    }

    return {
        sendAnalyticsForDialogOpen: sendAnalyticsForDialogOpen,
        sendAnalyticsForModeChange: sendAnalyticsForModeChange,
        sendAnalyticsForCreatingIssue: sendAnalyticsForCreatingIssue,
        sendAnalyticsForDescriptionEdited: sendAnalyticsForDescriptionEdited,
        sendAnalyticsForCreatedIssueWithEpic: sendAnalyticsForCreateIssueWithEpic,
        sendAnalyticsForTruncatedSummary: sendAnalyticsForTruncatedSummary,
        sendAnalyticsForJIMInsertFailed: sendAnalyticsForJIMInsertFailed,
        sendAnalyticsForSortedTable: sendAnalyticsForSortedTable,
        bindAnalyticsForCreateDirectlyInJira: bindAnalyticsForCreateDirectlyInJira,
        sendAnalyticsForCreateError: sendAnalyticsForCreateError,
        sendAnalyticsForRequiredFields: sendAnalyticsForRequiredFields
    };
})(AJS.$);