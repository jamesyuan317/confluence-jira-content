package com.atlassian.confluence.plugins.createjiracontent;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import junit.framework.TestCase;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.confluence.plugins.createjiracontent.cache.CacheKey;
import com.atlassian.confluence.plugins.createjiracontent.rest.beans.CachableJiraServerBean;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.ConfluenceUserImpl;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.sal.api.net.Request.MethodType;
import com.atlassian.sal.api.net.ResponseException;

public class CachedJiraResourcesManagerTest extends TestCase
{
    private static final String PLUGIN_NAME = "com.atlassian.confluence.plugins.createjiracontent.CachedJiraResourcesManager";

    @Mock private CacheManager cacheManager;

    @Mock private ApplicationLinkService appLinkService;

    @Mock private I18NBeanFactory i18nBeanFactory;

    private CachedJiraResourcesManager jiraResourcesManager;

    private List<ApplicationLink> appLinks;

    protected void setUp() throws Exception
    {
        super.setUp();
        ConfluenceUser test = new ConfluenceUserImpl("test", "test", "test@example.com");
        AuthenticatedUserThreadLocal.set(test);

        MockitoAnnotations.initMocks(this);

        jiraResourcesManager = new CachedJiraResourcesManager(appLinkService, cacheManager, 770L);

        ApplicationLink link = createMockApplicationLink("7d715c83-b5ce-39d3-a175-976aac6540ad", "Default Supported JIRA Server", true, "6.1.1", 6155);
        appLinks = new ArrayList<ApplicationLink>();
        appLinks.add(link);
    }

    public void testGetJiraServers()
    {
        try
        {
            ApplicationLink link = createMockApplicationLink("39d02cfc-ce59-31a8-81ab-621294388590", "UnSupported JIRA Server", false, "4.4.5", 633);
            appLinks.add(link);

            when(appLinkService.getApplicationLinks(com.atlassian.applinks.api.application.jira.JiraApplicationType.class)).thenReturn(appLinks);
            Cache cache = createMockCache(appLinks.get(0).getId().toString(), appLinks.get(1).getId().toString());
            when(cacheManager.getCache(CachedJiraResourcesManager.class.getCanonicalName())).thenReturn(cache);

            List<CachableJiraServerBean> servers = jiraResourcesManager.getJiraServers();
            assertEquals(2, servers.size());
        }
        catch (Exception e)
        {
            fail("Could not create mock Application Link");
        }
    }

    public void testGetSupportedJiraServers()
    {
        try
        {
            ApplicationLink link = createMockApplicationLink("39d02cfc-ce59-31a8-81ab-621294388590", "Another Supported JIRA Server", false, "5.2.5", 847);
            appLinks.add(link);

            when(appLinkService.getApplicationLinks(com.atlassian.applinks.api.application.jira.JiraApplicationType.class)).thenReturn(appLinks);
            Cache cache = createMockCache(appLinks.get(0).getId().toString(), appLinks.get(1).getId().toString());
            when(cacheManager.getCache(PLUGIN_NAME)).thenReturn(cache);

            List<CachableJiraServerBean> servers = jiraResourcesManager.getSupportedJiraServers();
            assertEquals(2, servers.size());
        }
        catch (Exception e)
        {
            fail("Could not create mock Application Link");
        }
    }

    public void testGetSupportedJiraServersOneNotSupported()
    {
        try
        {
            ApplicationLink link = createMockApplicationLink("39d02cfc-ce59-31a8-81ab-621294388590", "UnSupported JIRA Server", false, "4.4.5", 663);
            appLinks.add(link);

            when(appLinkService.getApplicationLinks(com.atlassian.applinks.api.application.jira.JiraApplicationType.class)).thenReturn(appLinks);
            Cache cache = createMockCache(appLinks.get(0).getId().toString(), appLinks.get(1).getId().toString());
            when(cacheManager.getCache(PLUGIN_NAME)).thenReturn(cache);

            List<CachableJiraServerBean> servers = jiraResourcesManager.getSupportedJiraServers();
            assertEquals(1, servers.size());
        }
        catch (Exception e)
        {
            fail("Could not create mock Application Link");
        }
    }

    private ApplicationLink createMockApplicationLink(String id, String linkName, boolean primaryLink,
            String serverVersion, long buildNumber) throws CredentialsRequiredException, ResponseException, URISyntaxException
    {
        String url = "http://localhost:11990/jira";
        ApplicationLink appLink = mock(ApplicationLink.class);
        ApplicationLinkRequestFactory requestFactory = mock(ApplicationLinkRequestFactory.class);
        ApplicationLinkRequest request = mock(ApplicationLinkRequest.class);

        when(appLink.getId()).thenReturn(new ApplicationId(UUID.randomUUID().toString()));
        when(appLink.getRpcUrl()).thenReturn(new URI(url));
        when(appLink.getName()).thenReturn(linkName);
        when(appLink.isPrimary()).thenReturn(primaryLink);

        when(appLink.createAuthenticatedRequestFactory()).thenReturn(requestFactory);
        when(requestFactory.createRequest(any(MethodType.POST.getClass()) , anyString())).thenReturn(request);

        String response = "{\"baseUrl\": \"" + url + "\",\"version\": \"" + serverVersion + "\",\"buildNumber\": " + buildNumber + ",\"serverTitle\": \"" + linkName + "\"}";
        when(request.execute()).thenReturn(response);
        return appLink;
    }

    private Cache createMockCache(String...linkIds)
    {
        Cache cache = mock(Cache.class);
        for (String id: linkIds)
        {
            CacheKey key = new CacheKey(id);
            when(cache.get(key)).thenReturn(null);
        }
        return cache;
    }

}
