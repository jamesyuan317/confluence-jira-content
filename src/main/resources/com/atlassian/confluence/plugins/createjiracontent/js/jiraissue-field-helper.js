Confluence.CreateJiraContent.JiraIssue = (function($) {
    var JIRA_METADATA_LINK ="/rest/api/2/issue/createmeta";
    var JIRA_SCHEMA_EPIC_KEY = "com.pyxis.greenhopper.jira:gh-epic-link";
    var JIRA_GREENHOPPER_EPIC_PROPS_REST_URL = "/rest/greenhopper/1.0/api/epicproperties";
    var CREATE_JIRA_ISSUE_EPIC_CACHED_KEY_PREFIX = "CREATE_JIRA_ISSUE_";
    var JIRA_EPIC_ISSUE_TYPE_CACHED_KEY_PREFIX = "JIRA_EPIC_ISSUE_TYPE_";
    var CREATE_JIRA_ISSUE_FIELD_CACHED_KEY_PREFIX = "CREATE_JIRA_ISSUE_FIELD_";

    // XHR object, hold ajax get fields request to call abort when needed
    var xhrField;
    /**
     * Discovering FIELD metadata (server>project>issuetype>(fields))
     * @param paramObj: request param object with serverId, projectId, issueTypeId params 
     * @param onSuccessCallback: callback function receive list fields json object param
     * @param onErrorCallback: callback function receive error message param
     */
    var discoverIssueTypeField = function(paramObj, onSuccessCallback, onErrorCallback) {
        // If xhrProject is existed, this mean previous ajax hadn't completed yet, call abort() to cancel it.
        if (xhrField && xhrField.readyState !== 4 && xhrField.abort) {
            xhrField.abort();
        }
        if (!paramObj.serverId || !paramObj.projectId || !paramObj.issueTypeId) {
            AJS.logError(AJS.format('confluence-jira-content:discoverIssueTypeField - Error with parameters: serverId={0}, projectId={1}, issueTypeId={2}',
                    paramObj.serverId, paramObj.projectId, paramObj.issueTypeId));
            // call error when get field params are unexpected.
            onErrorCallback && onErrorCallback(AJS.I18n.getText("createjiracontent.dialog.create.issue.discover.fields.error.request.param"));
            return;
        }
        xhrField = AppLinks.makeRequest({
            appId : paramObj.serverId,
            type : "GET",
            url : JIRA_METADATA_LINK,
            data : {
                expand : "projects.issuetypes.fields",
                projectIds : paramObj.projectId,
                issuetypeIds: paramObj.issueTypeId
            },
            dataType : "json",
            success : function(issueFieldData) {
                if (!issueFieldData || !issueFieldData.projects || !issueFieldData.projects[0].issuetypes) {
                    AJS.logError("confluence-jira-content:discoverIssueTypeField - Data discovering error! Unexpected data return.");
                    // call error when received unexpected data.
                    onErrorCallback && onErrorCallback(AJS.I18n.getText("createjiracontent.dialog.create.issue.discover.fields.error.data.return"));
                    return;
                }
                var fields = issueFieldData.projects[0].issuetypes[0].fields;
                onSuccessCallback(fields);
            },
            error : function(xhr) {
                AJS.logError("confluence-jira-content:discoverIssueTypeField - Error with status=" + xhr.status);
                onErrorCallback && onErrorCallback(AJS.I18n.getText("createjiracontent.dialog.create.issue.discover.fields.error.request.response", xhr.status));
            }
        });
    };

    /**
     * 
     */
    var resolveEpicIssueType = function(serverId, onSuccess, onError) {
        if (!serverId) {
            AJS.logError(AJS.format("confluence-jira-content:resolveEpicIssueType - Error with parameters: serverId={0}", serverId));
            return;
        }
        var epicIssueTypeCacheKey = JIRA_EPIC_ISSUE_TYPE_CACHED_KEY_PREFIX + serverId;
        if (Confluence.CreateJiraContent.JiraIssue.Cache.containKey(epicIssueTypeCacheKey)) {
            onSuccess(Confluence.CreateJiraContent.JiraIssue.Cache.get(epicIssueTypeCacheKey));
            return;
        }
        AppLinks.makeRequest({
            appId : serverId,
            type : "GET",
            url : JIRA_GREENHOPPER_EPIC_PROPS_REST_URL,
            dataType : "json",
            success : function(epicProperties) {
                if (!epicProperties || !epicProperties.epicTypeId) {
                    AJS.logError("confluence-jira-content:resolveEpicIssueType - Data discovering error! Unexpected data return.");
                    return;
                }
                var epicIssueTypeId = epicProperties.epicTypeId;
                onSuccess(epicIssueTypeId);
                Confluence.CreateJiraContent.JiraIssue.Cache.put(epicIssueTypeCacheKey, epicIssueTypeId); // Cached storing
            },
            error : function(xhr) {
                onError(xhr.status);
                AJS.logError("confluence-jira-content:resolveEpicIssueType - Error with status=" + xhr.status);
            }
        });
    };

    /**
     * Get create issue fields depend on project and issue type
     * 
     * @param paramObj: request param object with serverId, projectId, issueTypeId params 
     * @param onSuccessCallback: callback function receive list fields json object param
     * @param onErrorCallback: callback function receive error message param 
     */
    var getCreateIssueFields = function(paramObj, onSuccessCallback, onErrorCallback) {
        var fieldDataKey = CREATE_JIRA_ISSUE_FIELD_CACHED_KEY_PREFIX + paramObj.serverId
                + paramObj.projectId + paramObj.issueTypeId;
        // get field data in cached
        if (Confluence.CreateJiraContent.JiraIssue.Cache.containKey(fieldDataKey)) {
            processCacheData(fieldDataKey, onSuccessCallback, onErrorCallback);
            return;
        }

        var onDiscoverFieldSuccessCallback = function(fields) {
            // cache field data
            Confluence.CreateJiraContent.JiraIssue.Cache.put(fieldDataKey, fields);
            onSuccessCallback(fields);
        };

        discoverIssueTypeField(paramObj, onDiscoverFieldSuccessCallback, onErrorCallback);
    };

    /**
     * Detect Epic customField
     * @param paramObj: request param object with serverId, projectId, issueTypeId params  
     * @param handlingEpicFieldCallback : callback with customeField data
     */
    var resolveEpicField =  function(paramObj, handlingEpicFieldCallback) {
        var epicFieldCacheKey = CREATE_JIRA_ISSUE_EPIC_CACHED_KEY_PREFIX + 
                                paramObj.serverId + paramObj.projectId + paramObj.issueTypeId;
        if (Confluence.CreateJiraContent.JiraIssue.Cache.containKey(epicFieldCacheKey)) {
            processCacheData(epicFieldCacheKey, handlingEpicFieldCallback);
            return;
        }

        var onGetFieldsSuccessCallback = function(fields) {
            $.each(fields, function(key, value) {
                if (value.schema["custom"] == JIRA_SCHEMA_EPIC_KEY) {
                    // Epic cached storing
                    Confluence.CreateJiraContent.JiraIssue.Cache.put(epicFieldCacheKey, key);
                    handlingEpicFieldCallback(key);
                    return false;
                }
            });
        };

        getCreateIssueFields(paramObj, onGetFieldsSuccessCallback);
    };

    /**
     * Identify create issue fields:
     * + get other required fields are not in the defaultRequiredFields
     * + identity description field status: hidden/visible/required 
     *  
     * @param paramObj: request param object with serverId, projectId, issueTypeId params 
     * @param defaultRequiredFields: default required fields array name
     * @param onSuccessCallback: callback function with description field status and list of other required fields name
     * @param onErrorCallback: callback function receive error message param 
     */
    var identifyCreateIssueFields = function(paramObj, defaultRequiredFields, onSuccessCallback, onErrorCallback) {

        var onGetFieldsSuccessCallback = function(fields) {
            var descriptionFieldStatus = Confluence.CreateJiraContent.JiraIssue.FieldStatus.HIDDEN;
            var otherRequiredFields = [];
            $.each(fields, function(key, value) {
                if(key === "description") {
                    descriptionFieldStatus = (value.required == true ? 
                                            Confluence.CreateJiraContent.JiraIssue.FieldStatus.REQUIRED:
                                            Confluence.CreateJiraContent.JiraIssue.FieldStatus.VISIBLE);
                } else if (value.required == true && !value.hasDefaultValue) {
                    if ($.inArray(key, defaultRequiredFields) == -1) { // check current field is existed in default required field
                        otherRequiredFields.push(AJS.escapeHtml(value.name));
                    }
                }
            });

            onSuccessCallback(descriptionFieldStatus, otherRequiredFields);
        };

        getCreateIssueFields(paramObj, onGetFieldsSuccessCallback, onErrorCallback);
    };

    var processCacheData = function(cacheKey, onSuccessCallback, onErrorCallback) {
        var cacheData = Confluence.CreateJiraContent.JiraIssue.Cache.get(cacheKey);
        // has some problem with cache when cache data is null, process error for it
        if(cacheData) {
            onSuccessCallback(cacheData);
        }
        else {
            onErrorCallback
                    && onErrorCallback(AJS.I18n
                            .getText("createjiracontent.dialog.create.issue.discover.fields.error.data.return"));
        }
    };

    return {
        resolveEpicField : resolveEpicField,
        resolveEpicIssueType: resolveEpicIssueType,
        identifyCreateIssueFields: identifyCreateIssueFields
    };
})(AJS.$);

Confluence.CreateJiraContent.JiraIssue.Cache = (function() {
    return {
        put : function(key, value) {
            sessionStorage.setItem(key, JSON.stringify(value));
        },
        get : function(key) {
            var value = sessionStorage.getItem(key);
            if (value) {
                try {
                    var obj = JSON.parse(value);
                } catch (e) {
                    return null;
                }
                return obj;
            }
            return null;
        },
        containKey : function(key) {
            return sessionStorage.getItem(key) != null;
        }
    };
})();

// enum for field status
Confluence.CreateJiraContent.JiraIssue.FieldStatus = {
        HIDDEN : -1, // not existed
        VISIBLE : 0, // existed but not required
        REQUIRED : 1 // required field
};