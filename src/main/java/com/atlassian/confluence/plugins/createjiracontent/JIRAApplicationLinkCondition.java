package com.atlassian.confluence.plugins.createjiracontent;

import java.util.Map;

import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.application.jira.JiraApplicationType;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

public class JIRAApplicationLinkCondition implements Condition
{
    private ApplicationLinkService applicationLinkService;

    public JIRAApplicationLinkCondition(ApplicationLinkService applicationLinkService)
    {
        this.applicationLinkService = applicationLinkService;
    }

    @Override
    public void init(Map<String, String> params) throws PluginParseException
    {
        // EMPTY params
    }

    @Override
    public boolean shouldDisplay(Map<String, Object> arg0)
    {
        return applicationLinkService.getApplicationLinks(JiraApplicationType.class).iterator().hasNext();
    }

}
