package it.com.atlassian.confluence.plugins.jiracreate;

import static org.junit.Assert.assertTrue;
import it.com.atlassian.confluence.plugins.jiracreate.pageobjects.CreateIssueDialog;
import it.com.atlassian.confluence.plugins.jiracreate.pageobjects.PageAfterCreate;
import it.com.atlassian.confluence.plugins.jiracreate.rest.CreateJiraContentRestHelper;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import junit.framework.Assert;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.it.rpc.ConfluenceRpc;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.pageobjects.elements.query.Poller;

public class CreateJiraIssueFromTableTest extends AbstractJIRACreateTest
{
    @Before
    public void setUp()
    {
        CreateJiraContentRestHelper.setDiscoveredred(rpc.getBaseUrl(), User.TEST);
    }

    private static final String[][] TABLE_DATA = new String[][] { 
                                { "heading 1", "heading 2", "heading 3" },
                                { "cell A1", "cell A2", "cell A3" }, 
                                { " ", "cell B2", "cell B3" }, 
                                { " ", " ", "cell C3" } };

    private Page createPageTableContent(String pageTitle, int colums, int rows)
    {
        StringBuilder content = new StringBuilder();
        content.append("|");
        for(int i = 0; i < colums; i ++)
        {
            content.append("|" + "heading " + i + "|");
        }
        content.append("\n");
        
        for (int i = 0 ; i < rows; i ++) 
        {
            for (int j = 0; j < colums; j ++)
            {
                content.append("|" + "Cell " + i + "-" + j);
                
            }
            content.append("|\n");
        }
        
        Page page = new Page(Space.TEST, pageTitle, content.toString());
        rpc.logIn(User.ADMIN);
        rpc.createPage(page);
        return page;
    }

    private Page createPageWithNormalTable()
    {
        String title = "page with table";
        StringBuilder content = new StringBuilder();
        content.append("|");
        for (int i = 0; i < TABLE_DATA[0].length; i++)
        {
            content.append("|" + TABLE_DATA[0][i] + "|");
        }
        content.append("\n");

        for (int i = 1; i < TABLE_DATA.length; i++)
        {
            for (int j = 0; j < TABLE_DATA[i].length; j++)
            {
                content.append("|" + TABLE_DATA[i][j]);
            }
            content.append("|\n");
        }

        Page page = new Page(Space.TEST, title, content.toString());

        rpc.logIn(User.ADMIN);
        rpc.createPage(page);

        return page;
    }

    private static final String[][] NO_HEADING_TABLE_DATA = new String[][] { 
        { "cell A1", "cell A2", "cell A3" }, 
        { " ", "cell B2", "cell B3" }, 
        { " ", " ", "cell C3" } };

    private Page createPageWithNoHeadingTable()
    {
        String title = "page with table";
        StringBuilder content = new StringBuilder();

        for (int i = 0; i < NO_HEADING_TABLE_DATA.length; i++)
        {
            for (int j = 0; j < NO_HEADING_TABLE_DATA[i].length; j++)
            {
                content.append("|" + NO_HEADING_TABLE_DATA[i][j]);
            }
            content.append("|\n");
        }

        Page page = new Page(Space.TEST, title, content.toString());

        rpc.logIn(User.ADMIN);
        rpc.createPage(page);

        return page;
    }

    private CreateIssueDialog openCreateIssueDialog(int colIndex)
    {
        return openCreateIssueDialog(".confluenceTable>tbody td:nth-child("+ colIndex + ") p", 0, 7);
    }

    @Test
    public void displayCreateIssueDialog()
    {
        product.loginAndView(User.TEST, createPageWithNormalTable());
        Poller.waitUntilTrue(openCreateIssueDialog(2).isCreateIssueDialogVisible());
    }

    @Test
    public void displayCorrectSuggestionWhenCreateIssueFromTable()
    {
        product.loginAndView(User.TEST, createPageWithNormalTable());
        String suggestedText = openCreateIssueDialog(2).getSuggestedText();
        assertTrue(suggestedText.contains("Looks like you are creating issues from a table"));
        assertTrue(suggestedText.contains("Create 2 issues from this table"));
    }

    @Test
    public void displayCorrectSuggestionWhenCreateIssueFromTableInCaseItContainsOtherTable()
    {
        product.loginAndView(User.TEST, createPageWithTableContainingOtherTable());
        String suggestedText = openCreateIssueDialog(2).getSuggestedText();
        assertTrue(suggestedText.contains("Looks like you are creating issues from a table"));
        assertTrue(suggestedText.contains("Create 2 issues from this table"));
    }

    @Test
    public void notDisplaySuggestionPanelWhenThereIsOneIssueToCreate()
    {
        product.loginAndView(User.TEST, createPageWithNormalTable());
        Assert.assertFalse(openCreateIssueDialog(1).isSuggestionPanelPresent());
    }

    @Test
    public void checkListJiraServerValues()
    {
        product.loginAndView(User.TEST, createPageWithNormalTable());
        CreateIssueDialog dialog = openCreateIssueDialog(2);

        Assert.assertFalse(dialog.isJiraServerSelectVisible());
    }

    @Test
    public void checkListJiraProjectValues()
    {
        product.loginAndView(User.TEST, createPageWithNormalTable());
        CreateIssueDialog dialog = openCreateIssueDialog(2);
        if (dialog.isJiraServerSelectVisible())
        {
            dialog.selectJiraServer("testjira");
        }

        List<String> listJiraProjects = Arrays.asList("Select project", "Alphanumeric Key Test", "Special Project 1", 
                                            "Special Project 2", "Test Project", "Test Project 1", "Test Project 2");
        Assert.assertEquals(listJiraProjects, dialog.getListJiraProjectValues());
    }

    @Test
    public void checkListIssueTypeValues()
    {
        product.loginAndView(User.TEST, createPageWithNormalTable());
        CreateIssueDialog dialog = openCreateIssueDialog(2);
        if (dialog.isJiraServerSelectVisible())
        {
            dialog.selectJiraServer("testjira");
        }
        dialog.selectProject("Test Project");
        Assert.assertEquals("New Feature", dialog.getSelectedJiraIssueTypeValue());
        
        List<String> listIssueTypes = Arrays.asList("Bug", "New Feature", "Task", "Improvement");
        Assert.assertEquals(listIssueTypes, dialog.getListJiraIssueTypeValues());
    }

    private Page createPageWithTableContainingOtherTable()
    {
        String title = "page with table";
        String content = "<table class=\"confluenceTable\">" +
                         "    <tbody>" +
                         "        <tr>" +
                         "            <th>heading 1</th>" +
                         "            <th>heading 2</th>" +
                         "        </tr>" +
                         "        <tr>" +
                         "            <td><p>cell A1</p></td>" +
                         "            <td><p>cell A2</p></td>" +
                         "        </tr>" +
                         "        <tr>" +
                         "            <td><p>cell B1</p></td>" +
                         "            <td>" +
                         "                <p><table>" +
                         "                    <tbody>" +
                         "                        <tr>" +
                         "                            <th>sub heading 1</th>" +
                         "                            <th>sub heading 2</th>" +
                         "                        </tr>" +
                         "                        <tr>" +
                         "                            <td><p>sub cell A1</p></td>" +
                         "                            <td>sub cell A2</td>" +
                         "                        </tr>" +
                         "                    </tbody>" +
                         "               </table></p>" +
                         "           </td>" +
                         "       </tr>" +
                         "    </tbody>" +
                         "</table>";
        Page page = new Page(Space.TEST, title, content);
        ConfluenceRpc newInstance = ConfluenceRpc.newInstance(product.getProductInstance().getBaseUrl());
        newInstance.logIn(User.ADMIN);
        newInstance.createPage(page);

        return page;
    }

    @Test
    public void createIssuesFromNoHeadingTable()
    {
        Page testPage = createPageWithNoHeadingTable();
        product.loginAndView(User.TEST, testPage);

        String projectName = "Test Project";
        String issueTypeName = "Task";

        // Test for column 3 with 3 issues
        int columnIndex = 2;
        CreateIssueDialog dialog = openCreateIssueDialog(3)
                                   .switchToTableMode()
                                   .selectProject(projectName)
                                   .selectIssueType(issueTypeName);

        dialog.clickSubmitButton();
        PageAfterCreate page = product.getPageBinder().bind(PageAfterCreate.class);
        Assert.assertTrue("Message panel should be presented", page.isMsgPanelPresent());
        String[] issueKeys = page.getCreatedIssueKeys();
        int length = issueKeys.length;
        Assert.assertEquals(3, length);
        for (int i = 0; i < length; i++)
        {
            JiraIssueBean issueBean = new JiraIssueBean(issueKeys[i], NO_HEADING_TABLE_DATA[i][columnIndex], null,
                    projectName, issueTypeName);
            validateJiraIssue(issueBean);
        }

        String expectedMsg = String.format("%d issues have been created. Your page has been updated to reflect the status of these issues.", TABLE_DATA.length -1);
        Assert.assertEquals(expectedMsg, page.getMsg());
    }

    @Test
    public void checkCreateIssuesFromTable()
    {
        Page testPage = createPageWithNormalTable();
        product.loginAndView(User.TEST, testPage);

        String projectName = "Test Project";
        String issueTypeName = "Task";

        // Test for column 3 with 3 issues
        int columnIndex = 2;
        CreateIssueDialog dialog = openCreateIssueDialog(".confluenceTable>tbody td:nth-child(3) p", 0, 7)
                .switchToTableMode()
                .selectProject(projectName)
                .selectIssueType(issueTypeName);
        dialog.clickSubmitButton();
        PageAfterCreate page = product.getPageBinder().bind(PageAfterCreate.class);
        Assert.assertTrue("Message panel should be presented", page.isMsgPanelPresent());
        String[] issueKeys = page.getCreatedIssueKeys();
        int length = issueKeys.length;
        Assert.assertEquals(TABLE_DATA.length - 1, length);
        for (int i = 0; i < length; i++)
        {
            JiraIssueBean issueBean = new JiraIssueBean(issueKeys[i], TABLE_DATA[i + 1][columnIndex], null,
                    projectName, issueTypeName);
            validateJiraIssue(issueBean);
        }

        String expectedMsg = String.format("%d issues have been created. Your page has been updated to reflect the status of these issues.", TABLE_DATA.length -1);
        Assert.assertEquals(expectedMsg, page.getMsg());
    }
    
    @Test
    public void previewIssueNoMoreRemaining()
    {
        Page testPage = createPageTableContent("previewIssueNoMoreRemaining", 3, 3);
        product.loginAndView(User.TEST, testPage);
        String projectName = "Test Project";
        String issueTypeName = "Improvement";
        
        CreateIssueDialog dialog = openCreateIssueDialog(".confluenceTable>tbody td:nth-child(3) p", 0, 7)
                .switchToTableMode()
                .selectProject(projectName)
                .selectIssueType(issueTypeName);
        String issueType = dialog.getSelectedJiraIssueTypeValue();
        String url = dialog.getUrlIssueTypeIcon();
        Assert.assertTrue(url.contains(issueType.toLowerCase()));
        String expectedMsg = "";
        Assert.assertEquals(expectedMsg, dialog.getRemainingIssuesMessage());
    }
    @Test
    public void previewIssueOneMoreRemaining()
    {
        Page testPage = createPageTableContent("previewIssueOneMoreRemaining", 3, 4);
        product.loginAndView(User.TEST, testPage);

        String projectName = "Test Project";
        String issueTypeName = "Task";
        
        CreateIssueDialog dialog = openCreateIssueDialog(".confluenceTable>tbody td:nth-child(3) p", 0, 7)
                .switchToTableMode()
                .selectProject(projectName)
                .selectIssueType(issueTypeName);
        String issueType = dialog.getSelectedJiraIssueTypeValue();
        String url = dialog.getUrlIssueTypeIcon();
        Assert.assertTrue(url.contains(issueType.toLowerCase()));
        String expectedMsg = "+ 1 more issue.";
        Assert.assertEquals(expectedMsg, dialog.getRemainingIssuesMessage());
       
    }
    
    @Test
    public void previewIssueTwoMoreRemaining()
    {
        Page testPage = createPageTableContent("previewIssueTwoMoreRemaining", 3, 5);
        product.loginAndView(User.TEST, testPage);

        String projectName = "Test Project";
        String issueTypeName = "Bug";
        
        CreateIssueDialog dialog = openCreateIssueDialog(".confluenceTable>tbody td:nth-child(3) p", 0, 7)
                .switchToTableMode()
                .selectProject(projectName)
                .selectIssueType(issueTypeName);
        String issueType = dialog.getSelectedJiraIssueTypeValue();
        String url = dialog.getUrlIssueTypeIcon();
        Assert.assertTrue(url.contains(issueType.toLowerCase()));
        String expectedMsg = String.format("+ %d more issues.", 2);
        Assert.assertEquals(expectedMsg, dialog.getRemainingIssuesMessage());
       
    }


    private Page createPageWithTableWithLongCellText(String title)
    {
        String content = this.getDumpXmlData("created_page_with_table.xml");
        Page page = new Page(Space.TEST, title, content);
        ConfluenceRpc newInstance = ConfluenceRpc.newInstance(product.getProductInstance().getBaseUrl());
        newInstance.logIn(User.ADMIN);
        newInstance.createPage(page);
        return page;
    }

    @Test
    public void createIssueWithTableWithLongCellText()
    {
        String expected = "Welcome to the Atlassian Developer Documentation!\n" +
                "                Using the Atlassian plugin software developer kit (SDK), developers can create plugins that extend the functionality of\n" +
                "                Atlassian applications such as JIRA, Confluence, and others. The SDK allows you to quickly connect to and leverage the plugin development platform.\n" +
                "                We offer a number of infrastructure tools to help you create, market and sell your plugins. For example, thousands of customers visit theAtlassian\n" +
                "                Marketplace and the Universal Plugin Manager every day, to find plugins they may want to install.";
        final Page testPage = createPageWithTableWithLongCellText("page with table with long cell text 123456789");
        product.loginAndView(User.TEST, testPage);

        final String projectName = "Test Project";
        final String issueTypeName = "Task";
        final CreateIssueDialog dialog = openCreateIssueDialog(".confluenceTable>tbody p", 0, 300)
                .switchToTableMode()
                .selectProject(projectName)
                .selectIssueType(issueTypeName);
        dialog.clickSubmitButton();
        PageAfterCreate page = product.getPageBinder().bind(PageAfterCreate.class);
        Assert.assertTrue("Can create issue", page.getCreatedIssueKeys().length == 3);
        for(String issueName : page.getCreatedIssueKeys()){
            Issue issue = testKitJIRA.issues().getIssue(issueName);
            Assert.assertNotNull(issue);
            Assert.assertTrue("The summary must be less or equal 255", issue.fields.summary.length() <= 255);
            if(issue.fields.description != null && !issue.fields.description.isEmpty()){
                Assert.assertEquals(issue.fields.description, expected);
               break;
            }
        }
    }
    
    private PageAfterCreate createPageHasTable(String title, int column, int rows, int childIndex) 
    {
        Page testPage = createPageTableContent(title, column, rows);
        product.loginAndView(User.TEST, testPage);
        String projectName = "Test Project";
        String issueTypeName = "Bug";

        CreateIssueDialog dialog = openCreateIssueDialog(".confluenceTable>tbody td:nth-child(" + childIndex + ") p", 0, 7)
                .switchToTableMode()
                .selectProject(projectName)
                .selectIssueType(issueTypeName);
        dialog.clickSubmitButton();
        return product.getPageBinder().bind(PageAfterCreate.class);
        
    }
    
    @Test
    public void checkCreateIssuesFromTableHasWithManyIssues()
    {
        final int numOfIssuesToCreate = 19;
        PageAfterCreate page = createPageHasTable("checkCreateIssuesFromTableHasWithManyIssues", 3, numOfIssuesToCreate, 3);
        Assert.assertTrue("Message panel should be presented", page.isMsgPanelPresentLongWait());
        String[] issueKeys = page.getCreatedIssueKeys();
        int length = issueKeys.length;
        Assert.assertEquals(numOfIssuesToCreate, length);

        String expectedMsg = String.format("%d issues have been created. Your page has been updated to reflect the status of these issues.", numOfIssuesToCreate);
        Assert.assertEquals(expectedMsg, page.getMsg());
    }

    @Test
    public void disableSubmitButtonWhenFormIncomplete()
    {
        Page testPage = createPageWithNormalTable();
        product.loginAndView(User.TEST, testPage);

        String projectName = "Test Project";
        String issueTypeName = "Task";

        CreateIssueDialog dialog = openCreateIssueDialog(".confluenceTable>tbody td:nth-child(3) p", 0, 7);
        Poller.waitUntilTrue("Submit button is disabled when opening dialog", dialog.isSubmitButtonDisabled());
        dialog.selectProject(projectName)
              .selectIssueType(issueTypeName);

        Poller.waitUntilFalse("Submit button is enabled when project, issue type and summary were filled", dialog.isSubmitButtonDisabled());
        dialog.setSummary(" ");

        Poller.waitUntilTrue("Submit button is disabled when summary empty", dialog.isSubmitButtonDisabled());
        dialog.switchToTableMode();

        Poller.waitUntilFalse("Submit button is enabled when project, issue type selected in table mode", dialog.isSubmitButtonDisabled());
    }

    @Test
    public void checkCreateIssuesAndAutomaticallyCloseMessagePanelAfter10Seconds() throws InterruptedException
    {
        final String title = "checkCreateIssuesAndAutomaticallyCloseMessagePanelAfter10Seconds";
        PageAfterCreate pageAfterCreate = createPageHasTable(title, 3, 3, 3);
        pageAfterCreate.waitUntilSuccessMsgPanelPresent();
        Thread.sleep(12 * 1000);
        Assert.assertTrue("Message panel should not be display", !pageAfterCreate.isSuccessMsgPanelDisplay());
    }

    @Test
    public void createIssueWithOldPageVersion() throws InterruptedException
    {
        String expected = "These can't be added to the page because you were viewing an outdated version of this page.";
        String content = this.getDumpXmlData("old-page-version.xml");
        String newContent = this.getDumpXmlData("new-page-version.xml");
        Page page = new Page(Space.TEST, "old-page-version", content);
        ConfluenceRpc newInstance = ConfluenceRpc.newInstance(product.getProductInstance().getBaseUrl());
        newInstance.logIn(User.ADMIN);
        newInstance.createPage(page);

        product.loginAndView(User.TEST, page);
        final String projectName = "Test Project";
        final String issueTypeName = "Task";
        final CreateIssueDialog dialog = openCreateIssueDialog(".confluenceTable>tbody p", 0, 20)
                .switchToTableMode()
                .selectProject(projectName)
                .selectIssueType(issueTypeName);

        // make new version of page
        page.setContent(newContent);
        newInstance.savePage(page);

        dialog.clickSubmitButton();

        PageAfterCreate pageAfterCreate = product.getPageBinder().bind(PageAfterCreate.class);
        Assert.assertTrue(pageAfterCreate.isWarningMsgPanelDisplay());
        Assert.assertTrue(pageAfterCreate.getMsg().contains(expected));
    }
    
    private CreateIssueDialog selectTextFirstCellOfThirdColumnAsSortedTable(String cellValue)
    {
        String projectName = "Test Project";
        String issueTypeName = "Task";
        
        product.loginAndView(User.TEST, createPageWithNormalTable());
        product.getTester().getDriver()
                .findElement(By.cssSelector(".confluenceTable .sortableHeader>th:nth-child(3)")).click(); // sort table
        
        CreateIssueDialog dialog = openCreateIssueDialog(".confluenceTable>tbody td:nth-child(3) p", 0, cellValue.length())
                .selectProject(projectName)
                .selectIssueType(issueTypeName);
        MatcherAssert.assertThat(dialog.getSortedWarningMessage(),
                        Matchers.containsString("You are creating issues on the sorted table. These ones could not be appended to the page."));
        return dialog;
    }
    
    @Test
    public void insertIssueTableModeFailedInPageWithSortedTable()
    {
        CreateIssueDialog dialog = selectTextFirstCellOfThirdColumnAsSortedTable("Cell A3");
        dialog.switchToTableMode().clickSubmitButton();
        PageAfterCreate page = product.getPageBinder().bind(PageAfterCreate.class);
        String expectedMsg = String.format("%d issues have been created, but there are problems in adding them to the page.\nYou can manually add them later using JIRA macro.", TABLE_DATA.length -1);
        Assert.assertEquals(expectedMsg, page.getMsg());
        
    }
    @Test
    public void insertIssueSingleModeSuccessInPageWithSortedTable()
    {
        CreateIssueDialog dialog = selectTextFirstCellOfThirdColumnAsSortedTable("Cell A3");
        dialog.clickSubmitButton();
        PageAfterCreate page = product.getPageBinder().bind(PageAfterCreate.class);
        String expectedMsg = String.format("%s has been created. Your page has been updated to reflect the status of the issue.", page.getCreatedIssueKey());
        Assert.assertEquals(expectedMsg, page.getMsg());
    }
    
    @Test
    public void insertIssueSingleModeFailedInPageWithSortedTable()
    {
        CreateIssueDialog dialog = selectTextFirstCellOfThirdColumnAsSortedTable("Cell"); //"Cell" ->Duplicate text with row 2 and 3 (Cell B3, Cell C3)
        dialog.clickSubmitButton();
        PageAfterCreate page = product.getPageBinder().bind(PageAfterCreate.class);
        String expectedMsg = String.format("%s has been created, but there is a problem in adding it to the page.\nYou can manually add it later using JIRA macro.", page.getCreatedIssueKey());
        Assert.assertEquals(expectedMsg, page.getMsg());
    }
    
    
    @Test
    public void displayRequiredMessageWhenHaveRequiredField() throws IOException, InterruptedException
    {
        Page testPage = createPageWithNormalTable();
        product.loginAndView(User.TEST, testPage);

        String projectName = "Special Project 1";
        String issueTypeName = "Epic";

        CreateIssueDialog dialog = openCreateIssueDialog(".confluenceTable>tbody td:nth-child(3) p", 0, 7)
                                    .selectProject(projectName)
                                    .selectIssueType(issueTypeName);

        // Check display required message
        String requiredMessage = "The project and issue type you selected has required fields: Epic Name";
        Poller.waitUntil(dialog.getFormMessage(), Matchers.containsString(requiredMessage), Poller.by(10 * 1000));
        // Still display required message when switch into create issue from table mode
        dialog.switchToTableMode();
        Poller.waitUntil(dialog.getFormMessage(), Matchers.containsString(requiredMessage));
    }

    @Test
    public void checkCreateIssueFromTableWhenDescriptionFieldIsRequired() throws InterruptedException
    {
        String longCellText = "Welcome to the Atlassian Developer Documentation!\n" +
                "                Using the Atlassian plugin software developer kit (SDK), developers can create plugins that extend the functionality of\n" +
                "                Atlassian applications such as JIRA, Confluence, and others. The SDK allows you to quickly connect to and leverage the plugin development platform.\n" +
                "                We offer a number of infrastructure tools to help you create, market and sell your plugins. For example, thousands of customers visit theAtlassian\n" +
                "                Marketplace and the Universal Plugin Manager every day, to find plugins they may want to install.";
        final Page testPage = createPageWithTableWithLongCellText("page with table with long cell text");
        product.loginAndView(User.TEST, testPage);

        final String projectName = "Special Project 1";
        final String issueTypeName = "Task";
        final CreateIssueDialog dialog = openCreateIssueDialog(".confluenceTable>tbody p", 0, 300)
                .switchToTableMode()
                .selectProject(projectName)
                .selectIssueType(issueTypeName);
        dialog.clickSubmitButton();
        PageAfterCreate page = product.getPageBinder().bind(PageAfterCreate.class);

        String[] createdIssues = page.getCreatedIssueKeys();
        // check created issues with 2 cases:  
        // 1. Row 1 - description has long cell text when cell text length is > 255
        Issue issue = testKitJIRA.issues().getIssue(createdIssues[0]);
        Assert.assertEquals(issue.fields.description, longCellText);

        // 2. Row 2 - description equals the summary because of the description is required need fill description field 
        issue = testKitJIRA.issues().getIssue(createdIssues[1]);
        Assert.assertEquals(issue.fields.description, issue.fields.summary);
    }

    @Test
    public void checkCreateIssueFromTableWhenDescriptionFieldIsHidden() throws InterruptedException
    {
        Page testPage = createPageWithNormalTable();
        product.loginAndView(User.TEST, testPage);

        String projectName = "Special Project 2";
        String issueTypeName = "Story";

        // Test for column 3 with 3 issues
        CreateIssueDialog dialog = openCreateIssueDialog(".confluenceTable>tbody td:nth-child(3) p", 0, 7)
                .switchToTableMode()
                .selectProject(projectName)
                .selectIssueType(issueTypeName);
        dialog.clickSubmitButton();
        // Can create issues successful without error 
        PageAfterCreate page = product.getPageBinder().bind(PageAfterCreate.class);
        Assert.assertTrue("Message panel should be presented", page.isMsgPanelPresent());
        Assert.assertEquals(3, page.getCreatedIssueKeys().length);
    }

    @Test
    public void checkKeepInputtedValueWhenSwitchCreateIssueType() 
    {
        Page testPage = createPageWithNormalTable();
        product.loginAndView(User.TEST, testPage);

        String projectName = "Special Project 1";
        String issueTypeName = "Story";

        String summary = "Summary";
        String description = "Description";

        CreateIssueDialog dialog = openCreateIssueDialog(".confluenceTable>tbody td:nth-child(3) p", 0, 7)
                .selectProject(projectName)
                .selectIssueType(issueTypeName)
                .setSummary(summary)
                .setDescription(description);
        // keep inputted value when switch to table and turn back to text
        dialog.switchToTableMode();
        dialog.switchToTextMode();
        Assert.assertEquals(summary, dialog.getSummaryValue());
        Assert.assertEquals(description, dialog.getDescriptionValue());

        // keep inputted value when show required message and turn back
        dialog.selectIssueType("Epic");
        dialog.selectIssueType(issueTypeName);
        Assert.assertEquals(summary, dialog.getSummaryValue());
        Assert.assertEquals(description, dialog.getDescriptionValue());
    }
}