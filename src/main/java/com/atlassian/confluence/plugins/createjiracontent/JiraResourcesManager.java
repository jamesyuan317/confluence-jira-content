package com.atlassian.confluence.plugins.createjiracontent;

import java.util.List;

import com.atlassian.confluence.plugins.createjiracontent.rest.beans.CachableJiraServerBean;

public interface JiraResourcesManager
{
    List<CachableJiraServerBean> getJiraServers();

    List<CachableJiraServerBean> getSupportedJiraServers();

}
