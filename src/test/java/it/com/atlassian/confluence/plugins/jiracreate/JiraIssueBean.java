package it.com.atlassian.confluence.plugins.jiracreate;

public class JiraIssueBean
{
    private String key;

    private String summary;

    private String description = "";

    private String projectName;

    private String issueTypeName;

    public JiraIssueBean(String key, String summary, String description, String projectName, String issueTypeName)
    {
        this.key = key;
        this.summary = summary;
        this.description = description;
        this.projectName = projectName;
        this.issueTypeName = issueTypeName;
    }

    public String getKey()
    {
        return key;
    }


    public String getSummary()
    {
        return summary;
    }

    public String getDescription()
    {
        return description;
    }

    public String getProjectName()
    {
        return projectName;
    }

    public String getIssueTypeName()
    {
        return issueTypeName;
    }
}
