package com.atlassian.confluence.plugins.createjiracontent.rest;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.xml.sax.SAXException;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.application.jira.JiraApplicationType;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.extra.jira.JiraIssuesManager;
import com.atlassian.confluence.extra.jira.api.services.JiraMacroFinderService;
import com.atlassian.confluence.extra.jira.api.services.JqlBuilder;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.macro.xhtml.MacroManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.createjiracontent.JiraResourcesManager;
import com.atlassian.confluence.plugins.createjiracontent.rest.beans.CachableJiraServerBean;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.sal.api.net.ResponseException;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Path("/")
public class CreateJiraIssueRestResource
{
    private final PageManager pageManager;
    private MacroManager macroManager;
    private JiraMacroFinderService jiraMacroFinderService;
    private JiraIssuesManager jiraIssuesManager;
    private ApplicationLinkService appLinkService;
    private JiraResourcesManager jiraResourcesManager;

    public CreateJiraIssueRestResource(PageManager pageManager, MacroManager macroManager,
            JiraMacroFinderService jiraMacroFinderService, ApplicationLinkService applicationLinkService,
            JiraIssuesManager jiraIssuesManager, JiraResourcesManager jiraResourcesManager)
    {
        this.pageManager = pageManager;
        this.macroManager = macroManager;
        this.jiraMacroFinderService = jiraMacroFinderService;
        this.appLinkService = applicationLinkService;
        this.jiraIssuesManager = jiraIssuesManager;
        this.jiraResourcesManager = jiraResourcesManager;
    }

    /**
     * Detect all EpicJiraIssue which created by current Server and Page
     * if have no any or more than 1 epic (one page, one server) return EMPTY
     * if have 1 epic return HtmlPlaceHolder of EpicJiraIssue
     * 
     * @param pageId
     * @param serverId
     * @return HtmlPlaceHolder
     * @throws SAXException
     * @throws MacroExecutionException
     * @throws XhtmlException
     * @throws CredentialsRequiredException
     * @throws ResponseException
     * @throws UnsupportedEncodingException
     */
    @GET
    @Produces(APPLICATION_JSON)
    @Path("find-epic-issue")
    public Response findJiraEpicIssue(@QueryParam("pageId") long pageId, @QueryParam("serverId") final String serverId, 
            @QueryParam("epicIssueTypeId") final String epicIssueTypeId) 
            throws SAXException, MacroExecutionException, XhtmlException, CredentialsRequiredException, ResponseException
    {
        if (StringUtils.isBlank(serverId))
        {
            throw new ResponseException("Server Id cannot be empty");
        }
        AbstractPage abstractPage = pageManager.getAbstractPage(pageId);
        Predicate<MacroDefinition> epicMacroDefinitionFilter = new Predicate<MacroDefinition>()
        {
            @Override
            public boolean apply(MacroDefinition macroDef)
            {
                String macroServerId = macroDef.getParameters().get("serverId");
                return StringUtils.equals(macroServerId, serverId);
            }
        };
        
        Set<MacroDefinition> macroDefinitions = jiraMacroFinderService.findJiraIssueMacros(abstractPage, epicMacroDefinitionFilter);

        ApplicationLink applicationLink = getApplicationLink(serverId);
        if (applicationLink == null)
        {
            throw new ResponseException("Cannot find the applicationLink for serverId=" + serverId);
        }
        List<JiraIssue> jiraEpics = filterEpicIssue(applicationLink, macroDefinitions, epicIssueTypeId);
        if (CollectionUtils.isEmpty(jiraEpics) || jiraEpics.size() != 1)
        {
            return Response.noContent().build();
        }
        
        Macro macro = macroManager.getMacroByName("jira");
        if (macro != null)
        {
            String epicKey = jiraEpics.get(0).getKey();
            
            Map<String, String> params = Maps.newHashMap();
            params.put("key", epicKey);
            params.put("showSummary", Boolean.TRUE.toString());
            params.put("serverId", serverId);
            
            String htmlPlaceHolder = macro.execute(params, null, new DefaultConversionContext(abstractPage.toPageContext()) );
            JsonObject epicResult = new JsonObject();
            epicResult.addProperty("epicKey", epicKey);
            epicResult.addProperty("epicHtmlPlaceHolder", htmlPlaceHolder);
            
            return Response.ok(epicResult.toString()).build();
        }
        return Response.noContent().build();

    }

    /** 
     * 
     * @return list of JIRA servers
     */
    @GET
    @Produces({ APPLICATION_JSON })
    @Path("get-jira-servers")
    public Response getJiraServers()
    {
        List<CachableJiraServerBean> jiraServers = jiraResourcesManager.getJiraServers();
        return (jiraServers == null) ? Response.ok(Collections.EMPTY_LIST).build() : Response.ok(jiraServers).build();
    }

    /**
     * Filter all key in Macrodefinition, and then request to jira to collect "which key is epic type?"
     * This function can have to ask jira server twice.
     * Normally it will make first request with all key and if have no error, return correct JiraIssue
     * if there is some invalid key (no permission or has been deleted in jira) in the system, this invalid key will be remove and make the second request
     * After second request, we should receive the correct JiraIssue
     * @param applicationLink
     * @param macroDefinitionsHaveServer
     * @return
     * @throws CredentialsRequiredException
     * @throws ResponseException
     */
    private List<JiraIssue> filterEpicIssue(ApplicationLink applicationLink,
            Set<MacroDefinition> macroDefinitionsHaveServer, String epicIssueTypeId)
            throws CredentialsRequiredException, ResponseException
    {
        Function<MacroDefinition, String> extractMacroDefinitionKeyFunction =  new Function<MacroDefinition,String>()
        {
            @Override
            public String apply(MacroDefinition definition)
            {
                return definition.getParameters().get("key");
            }
        };
        
        Iterable<String> issueKeysServer = Collections2.transform(macroDefinitionsHaveServer, extractMacroDefinitionKeyFunction);
        Iterables.removeAll(issueKeysServer, Arrays.asList(null, ""));
        
        List<String> keys = Lists.newArrayList(issueKeysServer);
        if (CollectionUtils.isEmpty(keys))
        {
            return Collections.emptyList();
        }
        
        //try to execute in the first time, this execute can get the error
        String jsonStringResult = executeEpicJqlWithKeys(applicationLink,epicIssueTypeId, keys);
        
        //with the second time, execute with CORRECT key, the error will be free.
        List<String> errorKeys = detectErrorKeys(jsonStringResult, keys);
        if (CollectionUtils.isNotEmpty(errorKeys))
        {
            keys.removeAll(errorKeys);
            if (CollectionUtils.isEmpty(keys))
            {
                return Collections.emptyList();
            }
            jsonStringResult = executeEpicJqlWithKeys(applicationLink, epicIssueTypeId, keys);
        }
        
        List<JiraIssue> issueWithServer = parseFromJsonIssue(jsonStringResult);
        return issueWithServer;
    }
    
    private String executeEpicJqlWithKeys(ApplicationLink applicationLink, String epicIssueTypeId, List<String> issueKeys) throws ResponseException, CredentialsRequiredException
    {
        String[] keys = issueKeys.toArray(new String[issueKeys.size()]);
        String jqlQuery = new JqlBuilder()
                .issueTypes(epicIssueTypeId)
                .issueKeys(keys)
                .buildAndEncode();
        return jiraIssuesManager.executeJqlQuery(jqlQuery, applicationLink);
    }
    
    /**
     * Try to detect the error message in result and filter all invalid keys
     * @param jsonString
     * @param issueKeys that contain valid/invalid key
     * @return list of error key
     */
    private List<String> detectErrorKeys(String jsonString, List<String> issueKeys)
    {
        JsonParser jsonParser = new JsonParser();
        JsonObject jsonObject = jsonParser.parse(jsonString).getAsJsonObject();
        if (!jsonObject.has("errorMessages"))
        {
            return Collections.emptyList();
        }
        List<String> errorKeys = Lists.newArrayList();
        JsonArray errorJson = jsonObject.get("errorMessages").getAsJsonArray();
        for (int i = 0; i < errorJson.size(); i++)
        {
            String errorMessage = errorJson.get(i).getAsString();
            for (String key : issueKeys)
            {
                if (errorMessage.contains(key))
                {
                    errorKeys.add(key);
                }
            }
        }
        return errorKeys;
    }
    
    private List<JiraIssue> parseFromJsonIssue(String jsonString)
    {
        JsonParser jsonParser = new JsonParser();
        JsonObject jsonObject = jsonParser.parse(jsonString).getAsJsonObject();
        List<JiraIssue> jiraIssueBeans = Lists.newArrayList();
        JsonArray issuesJson = jsonObject.get("issues").getAsJsonArray();
        
        for (int i = 0; i < issuesJson.size(); i++) {
            JsonObject json = (JsonObject) issuesJson.get(i);
            jiraIssueBeans.add(new JiraIssue(json.get("key").getAsString()));
        }
        return jiraIssueBeans;
    }

    /**
     * Resolve applicationLink by serverId, if cannot find, use primary applink
     * @param serverId
     * @return {@link ApplicationLink}
     */
    private ApplicationLink getApplicationLink(String serverId)
    {
        for (ApplicationLink applicationLink : appLinkService.getApplicationLinks(JiraApplicationType.class))
        {
            if(StringUtils.equals(serverId, applicationLink.getId().toString()))
            {
                return applicationLink;
            }
        }
        return null;
    }
    
}

/**
 * Bean keep the value of JiraIssue
 *
 */
class JiraIssue
{
    private String key;
    
    public JiraIssue()
    {
    }
    
    public JiraIssue(String key)
    {
        this.key = key;
    }
    
    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

}
