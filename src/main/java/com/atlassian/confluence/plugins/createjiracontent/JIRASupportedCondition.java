package com.atlassian.confluence.plugins.createjiracontent;

import java.util.Map;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

public class JIRASupportedCondition implements Condition
{
    private JiraResourcesManager jiraResourcesManager;

    public JIRASupportedCondition(JiraResourcesManager jiraResourcesManager)
    {
        this.jiraResourcesManager = jiraResourcesManager;
    }

    public void init(Map<String, String> params) throws PluginParseException
    {
    }

    public boolean shouldDisplay(Map<String, Object> params)
    {
        return !jiraResourcesManager.getSupportedJiraServers().isEmpty();
    }

}
