package it.com.atlassian.confluence.plugins.jiracreate.pageobjects;

import java.util.concurrent.TimeUnit;

import junit.framework.Assert;

import com.atlassian.confluence.pageobjects.component.ConfluenceAbstractPageComponent;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

public class CreateIssuePanelButton extends ConfluenceAbstractPageComponent
{
    @ElementBy(cssSelector = "#inline-dialog-selection-action-panel button[data-key='com.atlassian.confluence.plugins.confluence-jira-content:create-JIRA-issue-summary']")
    private PageElement createIssuePanelButtonSummary;

    public boolean isCreateIssuePanelButtonSummaryPresent()
    {
        return createIssuePanelButtonSummary.timed().isPresent().by(20, TimeUnit.SECONDS);
    }

    public Object openDialog(Class<? extends ConfluenceAbstractPageComponent> dialogClass)
    {
        Assert.assertTrue("Panel and Summary button when select text should be displayed! But not.", isCreateIssuePanelButtonSummaryPresent());
        createIssuePanelButtonSummary.javascript().mouse().click();
        return (pageBinder.bind(dialogClass));
    }
}