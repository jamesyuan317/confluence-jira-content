package com.atlassian.confluence.plugins.createjiracontent.context;

import com.atlassian.confluence.plugins.createjiracontent.IsIssuesCreatedParametersPresentCondition;
import com.atlassian.confluence.web.context.HttpContext;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Provide velocity context for issues created message panel, although we can do it all
 * in velocity if we pass all necessary data through URL but this class give a chance
 * to change to server side option if needed as well as to have chance to verify parameters
 */
public class IssuesCreatedPanelContextProvider implements ContextProvider {

    private final HttpContext httpContext;

    public IssuesCreatedPanelContextProvider(HttpContext httpContext) {
        this.httpContext = httpContext;
    }

    /**
     * @inheritDoc
     */
    @Override
    public void init(Map<String, String> params) throws PluginParseException {
        //Do nothing
    }

    /**
     * @inheritDoc
     */
    @Override
    public Map<String, Object> getContextMap(Map<String, Object> context) {
        context.put(IsIssuesCreatedParametersPresentCondition.NUM_OF_ISSUES_REQ_PARAM, getNumOfIssuesCreated());
        context.put(IsIssuesCreatedParametersPresentCondition.ISSUES_URL_REQ_PARAM, getIssuesURL());
        context.put(IsIssuesCreatedParametersPresentCondition.ISSUE_NAME_REQ_PARAM, getIssueId());
        context.put(IsIssuesCreatedParametersPresentCondition.ADDED_TO_PAGE_REQ_PARAM, isAddedToPage());
        context.put(IsIssuesCreatedParametersPresentCondition.ERROR_MESSAGES_REQ_PARAM, getErrorMessages());
        context.put(IsIssuesCreatedParametersPresentCondition.STATUS_TEXT_REQ_PARAM, getStatusText());
        return context;
    }

    private int getNumOfIssuesCreated() {
        return Integer.valueOf(httpContext.getRequest().getParameter(IsIssuesCreatedParametersPresentCondition.NUM_OF_ISSUES_REQ_PARAM));
    }

    private String getIssuesURL() {
        return httpContext.getRequest().getParameter(IsIssuesCreatedParametersPresentCondition.ISSUES_URL_REQ_PARAM);
    }

    private String getIssueId() {
        return httpContext.getRequest().getParameter(IsIssuesCreatedParametersPresentCondition.ISSUE_NAME_REQ_PARAM);
    }

    private boolean isAddedToPage() {
        return Boolean.valueOf(httpContext.getRequest().getParameter(IsIssuesCreatedParametersPresentCondition.ADDED_TO_PAGE_REQ_PARAM));
    }

    private List<String> getErrorMessages() {
        String[] values = httpContext.getRequest().getParameterValues(IsIssuesCreatedParametersPresentCondition.ERROR_MESSAGES_REQ_PARAM);
        if (values == null) {
            return Collections.emptyList();
        }
        return Arrays.asList(values);
    }

    private String getStatusText()
    {
        return httpContext.getRequest().getParameter(IsIssuesCreatedParametersPresentCondition.STATUS_TEXT_REQ_PARAM);
    }
}
