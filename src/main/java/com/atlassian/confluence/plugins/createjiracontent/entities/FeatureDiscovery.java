package com.atlassian.confluence.plugins.createjiracontent.entities;

import net.java.ao.Entity;
import net.java.ao.Preload;

/**
 * TODO: copied implementation from https://stash.atlassian.com/projects/CONF/repos/confluence-jira-metadata/pull-requests/28/overview
 * remove it when have generic feature discovery service:  https://jira.atlassian.com/browse/CONFDEV-21132
 */
@Preload
public interface FeatureDiscovery extends Entity
{
    public String getUserKey();

    public void setUserKey(String userKey);

    public Boolean getDiscovered();

    public void setDiscovered(Boolean discovered);
}