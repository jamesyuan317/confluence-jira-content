package com.atlassian.confluence.plugins.createjiracontent.cache;

import java.io.Serializable;

public class CacheKey implements Serializable
{
    private final String appId;

    public CacheKey(String appId)
    {
        this.appId = appId;
    }

    public String getAppId()
    {
        return appId;
    }

    public String toString()
    {
        return "AppId: " + this.appId;
    }

    public int hashCode()
    {
        return (this.appId != null ? this.appId.hashCode() : 1);
    }

    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }

        if ((obj == null) || !(obj instanceof CacheKey))
        {
            return false;
        }

        CacheKey other = (CacheKey) obj;
        return (this.appId != null) && this.appId.equals(other.appId);
    }

}
