Confluence.CreateJiraContent.Form = (function($) {
    var TABLE_MODE = "table";
    var TEXT_MODE = "text";
    var MAX_ISSUE_PREVIEW = 3;
    var $createIssueForm;

    // in case creating issue from table, this is the containing table
    var $tableElement;

    // list issues in current column
    var columnIssues;

    // current column index from table
    var currentTableColIndex;

    // creating-issue dialog
    var dialog;

    // show type mode of dialog, can be text, table or list
    var showType;

    // keep the text selection at the time the form is shown
    var textSelectionObject;
    var selectionObject = function(selection) {
        if (selection !== undefined) {
            textSelectionObject = selection;
        } else {
            return textSelectionObject;
        }
    };

    // extract issues from the column
    var extractIssuesFromColumn = function(columnIndex) {
        var serverSelected = $createIssueForm.find("#jira-servers option:selected").val();
        var projectSelected = $createIssueForm.find("#jira-projects option:selected").val();
        var listIssuesInColumn = [];
        $tableElement.find("> tbody > tr").each(function(i, row) {
            $(row).find("td:eq(" + columnIndex + ")").each(function(j, cell) {
                var cellContent =$.trim($(cell).text());
                if (cellContent.length) {
                    var truncCellContent = Confluence.CreateJiraContent.FormTextHelper.removeLineBreaksAndTruncate(cellContent);
                    var isCutCellContent = Confluence.CreateJiraContent.FormTextHelper.isCutLongText(cellContent);
                    var description = (isCutCellContent == true) ? cellContent : "";
                    listIssuesInColumn.push({
                        summary : truncCellContent,
                        description : description
                    });
                }
            });
        });
        return listIssuesInColumn;
    };

    var fillCreateIssueFormFromText = function() {
        var $suggestedLinkPlaceholder = $createIssueForm.find("#text-suggestion");
        if (columnIssues != null && columnIssues.length > 1) {
            var suggestedLink = Confluence.CreateJiraContent.issueFromTextSuggestedLink({
                "numberOfIssues" : columnIssues.length
            });
            $suggestedLinkPlaceholder.html(suggestedLink);
        }
        else {
            $suggestedLinkPlaceholder.hide();
        }
    };

    var fillPreviewIssuesFromTable = function() {
        if (showType === TABLE_MODE) {
            var $issueTablePlaceholder = $createIssueForm.find("#create-from-table");
            $issueTablePlaceholder.empty();
            if ($createIssueForm.find("#jira-projects option:selected").val() != -1) {
                columnIssues = extractIssuesFromColumn(currentTableColIndex);
                var numberOfRemainingIssues = 0;
                var slicedColumnIssues = $(columnIssues);

                if (slicedColumnIssues.length > MAX_ISSUE_PREVIEW) {
                    slicedColumnIssues = slicedColumnIssues.slice(0, MAX_ISSUE_PREVIEW);
                    numberOfRemainingIssues = columnIssues.length - MAX_ISSUE_PREVIEW;
                }

                var previewIssues = Confluence.CreateJiraContent.createPreviewIssuesFromTable({
                    "issues" : slicedColumnIssues,
                    "numberOfRemainingIssues" : numberOfRemainingIssues
                });

                $issueTablePlaceholder.html(previewIssues);
            }
            var issueTypeSelected = $createIssueForm.find("#jira-issue-types option:selected").val();
            if (issueTypeSelected != -1) {
                Confluence.CreateJiraContent.FormHelper.changeIssuesTypeIconForPreviewPanel($createIssueForm.find(
                        "#jira-issue-types option:selected").val());
            }
            // refresh dialog to resize panel after remove content in dialog
            Confluence.CreateJiraContent.displayDialog.refresh();
        }
    };

    var fillCreateIssueFormFromTable = function() {
        fillPreviewIssuesFromTable(currentTableColIndex);
        var $suggestedLinkPlaceholder = $createIssueForm.find("#text-suggestion");
        var suggestedLink = Confluence.CreateJiraContent.issueFromTableSuggestedLink();
        $suggestedLinkPlaceholder.html(suggestedLink);
    };

    var switchShowType = function(type) {
        Confluence.CreateJiraContent.FormHelper.removeDisplayMessages();
        setShowType(type);
        if (type == TEXT_MODE) {
            $('#create-issues-dialog-header').text(AJS.I18n.getText("createjiracontent.dialog.form.header.title"));
            fillCreateIssueFormFromText();
            $createIssueForm.find("#create-from-table").hide();
            $createIssueForm.find("#create-from-text").show();
        } else if (showType === TABLE_MODE) {
            $('#create-issues-dialog-header').text(AJS.I18n.getText("createjiracontent.dialog.form.header.title.plural"));
            fillCreateIssueFormFromTable();
            $createIssueForm.find("#create-from-text").hide();
            $createIssueForm.find("#create-from-table").show();
        }
        AJS.trigger('confluence-jira-content.form-updated');
    };

    var bindSuggestionLinkEvent = function() {
        $createIssueForm.on('click', '#text-suggested-link, #table-suggested-link', function(e) {
            var $this = $(this);
            var isTextMode = ($this.attr('id') === 'text-suggested-link');
            var newMode = (isTextMode) ? TABLE_MODE : TEXT_MODE;
            switchShowType(newMode);
            e.preventDefault();
            return false;
        });
    };

    var getIssueFromTextForm = function() {
        var issues = [];
        var $summary = $createIssueForm.find("#issue-summary");
        var $description = $createIssueForm.find("#issue-description");

        var issue = {
            summary : $.trim($summary.val()),
            description : $.trim($description.val())
        };
        issues.push(issue);
        return issues;
    };

    var createIssueFromText = function() {
        if (Confluence.CreateJiraContent.FormHelper.validateCreateIssueForm()) {
            var issues = getIssueFromTextForm();
            Confluence.CreateJiraContent.FormHelper.createIssues(issues, formObject);
        }
    };

    // get list row index from column has value 
    var getIssueRowIndexesFromColumn = function(columnIndex) {
        var rowIssueIndexes = [];
        $tableElement.find("td:nth-child(" + (columnIndex + 1) + ")").each(function(index, cell) {
            var cellContent = $.trim($(cell).text());
            if (cellContent.length > 0) {
                rowIssueIndexes.push(index);
            }
        });
        return rowIssueIndexes;
    };

    var createIssueFromTable = function() {
        if (Confluence.CreateJiraContent.FormHelper.validateCreateIssueForm()) {
            var issueRowIndexes = getIssueRowIndexesFromColumn(currentTableColIndex);
            Confluence.CreateJiraContent.FormHelper.createIssues(columnIssues, formObject, currentTableColIndex, issueRowIndexes);
        }
    };

    //bind submit action for create issue form based on show type mode
    var bindFormSubmit = function() {
        $createIssueForm.submit(function() {
            if (showType === TEXT_MODE) {
                createIssueFromText();
            } 
            else {
                createIssueFromTable();
            }
            return false;
        });
    };

    var getDefaultShowType = function() {
        return TEXT_MODE;
    };

    var setShowType = function(type) {
        showType = type;
    }

    /**
     * Verify if selected text in table is sorted table or not.
     */
    var isInSortedTable = function() {
        return $tableElement.find("> thead > tr > th.tablesorter-headerSortUp, > thead > tr > th.tablesorter-headerSortDown").length > 0;
    };

    var handlingMessageInSortedTable = function() {
        if (isInSortedTable()) {
            var sortedTableMessage = atlassian.message.warning({
                'content' : AJS.I18n.getText("createjiracontent.messages.issues.not.added.reason.sortedtable")
            });
            $createIssueForm.children("#prepare-issue-messages").html(sortedTableMessage);
            Confluence.CreateJiraContent.Analytics.sendAnalyticsForSortedTable();
        }
    };

    var isHighlightInTable = function() {
        $tableElement = $(selectionObject().containingElement).closest("table");
        if ($tableElement.length > 0) {
            return true;
        }
        return false;
    };

    var setDialogObject = function(createIssueDialog) {
        dialog = createIssueDialog;
    };

    var bindCancelLink = function($createIssueForm) {
        var $cancelLink = $createIssueForm.find(".create-issue-cancel");
        $cancelLink.on("click", function(e) {
            // this variable is used as a flag to let Confluence.CreateJiraContent.FormHelper.bindHideEventToDialog() function know if
            // the dialog is closed by clicking on Cancel Button
            // when Cancel is clicked, it will assign as TRUE, we don't need to reset this value since this dom will be reset each time it show
            Confluence.CreateJiraContent.displayDialog.isCancelButtonClicked = true;
            Confluence.CreateJiraContent.displayDialog.hide();
            e.preventDefault();
        });
    };

    var prepareDataForCreatingFromTable = function() {
        currentTableColIndex = $(selectionObject().containingElement).closest("td").index();

        //when select Multiple Cell or Double Click on cell in Chrome (startContainer contains the correct element) 
        //=>try to get first element if support (Firefox, chrome)
        if (currentTableColIndex == -1 && selectionObject().range.startContainer) {
            var containingElement = $(selectionObject().range.startContainer);
            currentTableColIndex = containingElement.closest("td").index();
        }
        //if cannot detect the cell selected (multiple cell in IE<=8)
        if (currentTableColIndex == -1) {
            currentTableColIndex = 0;
        }

        columnIssues = extractIssuesFromColumn(currentTableColIndex);
    };

    var createFormContent = function() {
        var truncSummaryText = Confluence.CreateJiraContent.FormTextHelper.trunc(selectionObject().text);
        var isCutLongText = Confluence.CreateJiraContent.FormTextHelper.isCutLongText(truncSummaryText);
        var $formContent = Confluence.CreateJiraContent.createIssueDialog({
            "summary" : truncSummaryText,
            "isCutLongText" : isCutLongText,
            "comment" : selectionObject().text
        });
        return $formContent;
    };

    var isInTextMode = function() {
        return showType === TEXT_MODE;
    };

    var formObject = {
        addFormContent : function($form, selObject) {
            // store the object that represents the current selection
            selectionObject(selObject);

            var $formContent = createFormContent();
            $form.html($formContent);

            $createIssueForm = $form.find("#jira-content-create-issue-form");

            setShowType(getDefaultShowType()); //showType is text as default
            columnIssues = null; //reset columnIssues in case a page has text & table
            if (isHighlightInTable()) {
                prepareDataForCreatingFromTable();
            }

            // make sure we don't show text-suggestion when we highlight in a table which has one row and highlight in sentence.
            if (columnIssues == null || columnIssues.length == 1) {
                $form.find('#text-suggestion').remove();
            } else {
                bindSuggestionLinkEvent();
            }

            switchShowType(showType);

            Confluence.CreateJiraContent.FormHelper.init($createIssueForm);

            bindFormSubmit();
            bindCancelLink($createIssueForm);
            handlingMessageInSortedTable();
        },
        setDialogObject: setDialogObject,
        selectionObject: selectionObject,
        fillPreviewIssuesFromTable: fillPreviewIssuesFromTable,
        isInSortedTable: isInSortedTable,
        isInTextMode: isInTextMode
    };

    return formObject;
})(AJS.$);

Confluence.CreateJiraContent.FormTextHelper = (function() {
    var maxLength = 255;
    var hellip = "\u2026";
    return {
        trunc : function(txt) {
            return txt.length <= maxLength ? txt : txt.substr(0, maxLength - 1) + hellip;
        },
        isCutLongText : function(txt) {
            if (txt.length >= maxLength) {
                return true;
            }
            return false;
        },
        removeLineBreaksAndTruncate: function(txt){
            var removedBreakLineText = txt.replace(/\n|\r|\r\n/g, " ");
            return this.trunc(removedBreakLineText);
        }
        
    };
})();
