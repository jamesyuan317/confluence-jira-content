Confluence.CreateJiraContent.InlineDialog = (function($) {
    var DIALOG_WIDTH = 360;
    // What is the max height the dialog can have (error message, create from table link, etc)
    // Need to know max height to determine if there is enough space to display the dialog above highlighted text
    var DIALOG_MAX_HEIGHT = 500;
    var FEATURE_DISCOVERY_DIALOG_WIDTH = 370;

    var $inlineDialogTarget = $("<div>").attr("id", "jira-content-dialog-target");

    var prepareCreateIssueDialogTarget = function(selectionObject) {
        var targetRect = selectionObject.area;
        Confluence.DocThemeUtils.appendAbsolutePositionedElement($inlineDialogTarget);
        $inlineDialogTarget.css({
            top: targetRect.average.top,
            height: targetRect.average.height,
            left: targetRect.average.left,
            width: targetRect.average.width
        });
    };

    var defaultDialogOptions = {
        hideDelay: null,
        maxHeight: DIALOG_MAX_HEIGHT
    }

    var openCreateIssueDialog = function(selectionObject) {
        prepareCreateIssueDialogTarget(selectionObject);

        var createIssueDialog = Confluence.ScrollingInlineDialog($inlineDialogTarget, "create-issue-dialog", function(content, trigger, showPopup) {
            Confluence.CreateJiraContent.Form.addFormContent(content, selectionObject);
            Confluence.CreateJiraContent.Form.setDialogObject(createIssueDialog);
            showPopup();
            return false;
        },
        $.extend({}, defaultDialogOptions, {
            // Add this event to prevent auto-hide inline dialog when select2 is focus
            // return true to hide and false to prevent hiding.
            preHideCallback: function(){
                // Select2 auto create a mask with ID is #select2-drop-mask when it's expending
                // it cause inline dialog lost focus then hide.
                // also don't hide the dialog if issue creation process is under way
                return ($('#formSpinner.aui-icon.aui-icon-wait').length == 0) && (!$('#select2-drop-mask').is(':visible'));
            },
            width : DIALOG_WIDTH,
            hideCallback: function() {
                $inlineDialogTarget.remove();
                Confluence.CreateJiraContent.FormHelper.bindHideEventToDialog();
            }

        })
        );
        return createIssueDialog;
    };

    var openFeatureDiscoveryDialog = function(selectionObject, showCreateIssueDialogFn) {
        prepareCreateIssueDialogTarget(selectionObject);

        var featureDiscoveryDialog = Confluence.ScrollingInlineDialog($inlineDialogTarget, "create-issue-feature-discovery-dialog", function(content, trigger, showPopup) {
            Confluence.CreateJiraContent.FeatureDiscoveryDialog.addFeatureDiscoveryContent(content, selectionObject, showCreateIssueDialogFn);
            Confluence.CreateJiraContent.FeatureDiscoveryDialog.setFeatureDialogObject(featureDiscoveryDialog);
            showPopup();
            return false;
        },
        $.extend({}, defaultDialogOptions, {
            width : FEATURE_DISCOVERY_DIALOG_WIDTH,
            hideCallback: function() {
                if (Confluence.CreateJiraContent.FeatureDiscoveryDialog.shouldShowFeatureDiscovery()) {
                    $inlineDialogTarget.remove();
                }
            }
        })
        );
        return featureDiscoveryDialog;
    };

    return {
        openCreateIssueDialog : openCreateIssueDialog,
        openFeatureDiscoveryDialog : openFeatureDiscoveryDialog
    };
})(AJS.$);
