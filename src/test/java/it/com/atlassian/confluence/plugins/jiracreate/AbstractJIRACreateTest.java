package it.com.atlassian.confluence.plugins.jiracreate;

import com.atlassian.webdriver.testing.rule.WebDriverScreenshotRule;

import it.com.atlassian.confluence.plugins.jiracreate.pageobjects.CreateIssueDialog;
import it.com.atlassian.confluence.plugins.jiracreate.pageobjects.CreateIssuePanelButton;
import it.com.atlassian.confluence.plugins.jiracreate.pageobjects.FeatureDiscoveryDialog;
import junit.framework.Assert;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.BeforeClass;

import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.pageobjects.component.ConfluenceAbstractPageComponent;
import com.atlassian.confluence.webdriver.AbstractWebDriverTest;
import com.atlassian.jira.testkit.client.Backdoor;
import com.atlassian.jira.testkit.client.IssuesControl;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.testkit.client.util.TestKitLocalEnvironmentData;

import org.junit.Rule;

import java.io.IOException;
import java.io.InputStream;

public class AbstractJIRACreateTest extends AbstractWebDriverTest
{
    protected final String jiraBaseUrl = System.getProperty("baseurl.jira1", "http://localhost:11990/jira");

    protected final String jiraDisplayUrl = jiraBaseUrl.replace("localhost", "127.0.0.1");

    protected static Backdoor testKitJIRA;

    protected ApplinkHelper applinkHelper;

    @Rule
    public WebDriverScreenshotRule webDriverScreenshotRule = new WebDriverScreenshotRule();

    @BeforeClass
    public static void prepareGlobal()
    {
        testKitJIRA = new Backdoor(new TestKitLocalEnvironmentData());
    }

    @Override
    public void start() throws Exception
    {
        super.start();
        String confluenceBaseUrl = product.getProductInstance().getBaseUrl();
        applinkHelper = new ApplinkHelper(User.ADMIN, confluenceBaseUrl, jiraBaseUrl);
        applinkHelper.removeAllApplink();
        applinkHelper.setupBasicAndActivate();
        clearLocalStorage();
    }
    
    @After
    public void tearDown()
    {
        clearLocalStorage();
    }
    
    protected CreateIssueDialog openCreateIssueDialog(String selector, int selectStart, int selectEnd)
    {   
        return (CreateIssueDialog) selectTextAndOpenDialog(selector, selectStart, selectEnd, CreateIssueDialog.class);
    }
    
    protected FeatureDiscoveryDialog openFeatureDiscoveryDialog(String selector, int selectStart, int selectEnd)
    {   
        return (FeatureDiscoveryDialog) selectTextAndOpenDialog(selector, selectStart, selectEnd, FeatureDiscoveryDialog.class);
    }

    protected Page createPageWithText()
    {
        String title = "page with text";
        String content = "This is test page, then use it for testing.";
        Page page = new Page(Space.TEST, title, content);

        rpc.logIn(User.ADMIN);
        rpc.createPage(page);

        return page;
    }

    protected IssuesControl getIssues()
    {
        return testKitJIRA.issues();
    }

    protected void validateJiraIssue(JiraIssueBean jiraIssueBean)
    {
        Issue issue = getIssues().getIssue(jiraIssueBean.getKey());
        Assert.assertEquals(issue.fields.summary, jiraIssueBean.getSummary());
        Assert.assertEquals(issue.fields.description, jiraIssueBean.getDescription());
        Assert.assertEquals(issue.fields.project.name, jiraIssueBean.getProjectName());
        Assert.assertEquals(issue.fields.issuetype.name, jiraIssueBean.getIssueTypeName());
    }

    protected Object selectTextAndOpenDialog(String selector, int selectStart, int selectEnd, Class<? extends ConfluenceAbstractPageComponent> dialogClass)
    {
        SelectionTextHelper.selectTextNode(product.getTester().getDriver(), selector, selectStart, selectEnd);
        CreateIssuePanelButton createIssuePanelButton = product.getPageBinder().bind(CreateIssuePanelButton.class);
        return createIssuePanelButton.openDialog(dialogClass);
    }

    protected String getDumpXmlData(String dumpFileName)
    {
       String xmlData = null;
        if (xmlData == null)
        {
            InputStream xmlStream = this.getClass().getClassLoader().getResourceAsStream("xml/" + dumpFileName);
            try
            {
                xmlData = IOUtils.toString(xmlStream, "UTF-8");
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
        }

        return xmlData;
    }
    
    protected void clearLocalStorage()
    {
        product.getTester().getDriver().executeScript("window.localStorage.clear();");
    }
    
}
