package com.atlassian.confluence.plugins.createjiracontent.rest;

import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.confluence.plugins.createjiracontent.services.FeatureDiscoveryService;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;

/**
 * TODO: copied implementation from https://stash.atlassian.com/projects/CONF/repos/confluence-jira-metadata/pull-requests/28/overview
 * remove it when have generic feature discovery service:  https://jira.atlassian.com/browse/CONFDEV-21132
 */
@Path("metadata")
@Produces({ MediaType.APPLICATION_JSON })
public class CreateIssueMetadataResource
{
    private final FeatureDiscoveryService featureDiscoveryService;

    public CreateIssueMetadataResource(final FeatureDiscoveryService featureDiscoveryService)
    {
        this.featureDiscoveryService = featureDiscoveryService;
    }

    @Path("/discovered")
    @PUT
    public Response setDiscovered()
    {
        ConfluenceUser user = AuthenticatedUserThreadLocal.get();
        this.featureDiscoveryService.setUserDiscovered(user, true);
        return Response.ok().build();
    }
}
