package it.com.atlassian.confluence.plugins.jiracreate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.atlassian.confluence.it.RestHelper;
import com.atlassian.confluence.it.User;
import com.sun.jersey.api.client.WebResource;

public class ApplinkHelper
{
    private static final String APPLINK_WS =  "/rest/applinks/1.0/applicationlink";
    private static final String CREATE_APPLINK_WS = "/rest/applinks/1.0/applicationlinkForm/createAppLink";
    private static final String HTTP_QUERY_PARAM_ACCEPT_TYPE = "application/json, text/javascript, */*";
    private static final String OUTGOING_APPLINK_ACTIVATE = "/plugins/servlet/applinks/auth/conf/oauth/outbound/atlassian/";
    private static final String INCOMING_APPLINK_ACTIVATE = "/plugins/servlet/applinks/auth/conf/oauth/add-consumer-by-url/";
    
    private User user;
    private String jiraBaseUrl;
    private String confluenceBaseUrl;
    
    public ApplinkHelper(User user, String confluenceBaseUrl, String jiraBaseUrl)
    {
        this.user = user;
        this.jiraBaseUrl = jiraBaseUrl;
        this.confluenceBaseUrl = confluenceBaseUrl;
    }

    public void removeAllApplink() throws JSONException
    {
        removeAllApplink(confluenceBaseUrl);
        removeAllApplink(jiraBaseUrl);
    }
    
    public void setupBasicAndActivate() throws HttpException, IOException, JSONException 
    {
        this.enableApplinkBasicMode(setupAppLinkConfluence());
    }
    
    public void setupOAuthAndActivate() throws HttpException, IOException, JSONException 
    {
        this.enableOauthWithApplinkOutgoing(setupAppLinkConfluence(), confluenceBaseUrl);
        this.enableOauthWithApplinkIncoming(setupAppLinkJira(), jiraBaseUrl);
    }
    
    public void removeAllApplink(String baseUrl) throws JSONException
    {
        //get all the applink(s)
        WebResource webResource = RestHelper.newResource(baseUrl + APPLINK_WS, user);
        WebResource.Builder builder = webResource.accept(HTTP_QUERY_PARAM_ACCEPT_TYPE);
        
        String result = builder.get(String.class);
        
        final JSONObject jsonObj = new JSONObject(result);
        JSONArray jsonArray = jsonObj.getJSONArray("applicationLinks");
        List<String> ids = new ArrayList<String>();
        for (int i = 0; i < jsonArray.length(); i++)
        {
            final String id = jsonArray.getJSONObject(i).getString("id");
            ids.add(id);
        }

        // delete all applink
        for (String id : ids)
        {
            webResource.path(id).accept(HTTP_QUERY_PARAM_ACCEPT_TYPE).delete(String.class);
        }
    }

    
    public String setupAppLinkConfluence() throws HttpException, IOException, JSONException
    {
        String jsonContent = buildCreateApplinkJson(jiraBaseUrl);
        HttpClient httpClient = new HttpClient();
        return createAppLink(httpClient, confluenceBaseUrl, jsonContent);
    }
    public String setupAppLinkJira() throws HttpException, IOException, JSONException
    {
        String jsonContent = buildCreateApplinkJson(confluenceBaseUrl);
        HttpClient httpClient = new HttpClient();
        return createAppLink(httpClient, jiraBaseUrl, jsonContent);
    }
    
    private String createAppLink(HttpClient client, String baseUrl, String jsonContent) throws HttpException, IOException, JSONException
    {
        String authArgs = getAuthQueryString();
        
        PostMethod m = new PostMethod(baseUrl + CREATE_APPLINK_WS + authArgs);
        m.setRequestHeader("Accept", "application/json, text/javascript, */*");
        StringRequestEntity reqEntity = new StringRequestEntity(jsonContent,"application/json", "UTF-8");
        m.setRequestEntity(reqEntity);
        
        int status = client.executeMethod(m);
        
        Assert.assertEquals(200, status);
        JSONObject jsonObj = new JSONObject(m.getResponseBodyAsString());
        String id = jsonObj.getJSONObject("applicationLink").getString("id");
        return id;
    }
    
    private String buildCreateApplinkJson(String baseUrl) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        JSONObject applicationLink = new JSONObject();
        applicationLink.put("displayUrl", baseUrl);
        applicationLink.put("isPrimary", false);
        applicationLink.put("name", "testjira");
        applicationLink.put("rpcUrl", baseUrl);
        applicationLink.put("typeId", "jira");
        jsonObject.put("applicationLink" , applicationLink);
        
        JSONObject configFormValues = new JSONObject();
        configFormValues.put("shareUserbase", false);
        configFormValues.put("trustEachOther", false);
        
        jsonObject.put("configFormValues" , configFormValues);
        jsonObject.put("createTwoWayLink" , false);
        jsonObject.put("customRpcURL" , false);
        jsonObject.put("password", "");
        jsonObject.put("rpcUrl" , "");
        jsonObject.put("username" , "");
        return jsonObject.toString();
    }
    
    protected void enableOauthWithApplinkOutgoing(String id, String baseUrl) throws HttpException, IOException
    {
        HttpClient client = new HttpClient();
        
        PostMethod setTrustMethod = new PostMethod(baseUrl + OUTGOING_APPLINK_ACTIVATE + id + getAuthQueryString());
        setTrustMethod.addParameter("outgoing-enabled", "true");
        setTrustMethod.addRequestHeader("X-Atlassian-Token", "no-check");
        client.executeMethod(setTrustMethod);
    }
    protected void enableOauthWithApplinkIncoming(String id, String baseUrl) throws HttpException, IOException
    {
        HttpClient client = new HttpClient();
        PostMethod setTrustMethod = new PostMethod(baseUrl + INCOMING_APPLINK_ACTIVATE+ id +"/INBOUND"+ getAuthQueryString());
        setTrustMethod.addParameter("oauth-incoming-enabled", "true");
        setTrustMethod.addRequestHeader("X-Atlassian-Token", "no-check");
        client.executeMethod(setTrustMethod);
    }
    
    private void enableApplinkBasicMode(String idAppLink) throws IOException
    {
        final HttpClient client = new HttpClient();
        doWebSudo(client);
        final PutMethod method = new PutMethod(confluenceBaseUrl + "/plugins/servlet/applinks/auth/conf/basic/" + idAppLink + getBasicQueryString());
        method.addRequestHeader("X-Atlassian-Token", "no-check");
        final int status = client.executeMethod(method);
        Assert.assertEquals(HttpStatus.SC_MOVED_TEMPORARILY, status);
    }
    private String getBasicQueryString()
    {
        final String adminUserName = User.ADMIN.getUsername();
        final String adminPassword = User.ADMIN.getPassword();
        return "?username=" + adminUserName + "&password1=" + adminPassword + "&password2=" + adminPassword;
    }
    
    private String getAuthQueryString()
    {
        return "?os_username=" + user.getUsername() + "&os_password=" + user.getPassword();
    }
    
    private void doWebSudo(HttpClient client) throws IOException, HttpException
    {
        final PostMethod l = new PostMethod(confluenceBaseUrl + "/confluence/doauthenticate.action" + getAuthQueryString());
        l.addParameter("password", User.ADMIN.getPassword());
        final int status = client.executeMethod(l);
        Assert.assertEquals(HttpStatus.SC_MOVED_TEMPORARILY, status);
    }
    
    public void setupAppLink() throws IOException, JSONException
    {
        String authArgs = getAuthQueryString();
        final HttpClient client = new HttpClient();
        if(!checkExistAppLink(client, authArgs))
        {
            final String idAppLink = createAppLink(client, confluenceBaseUrl, buildCreateApplinkJson(jiraBaseUrl));
            doWebSudo(client);
            enableApplinkBasicMode(idAppLink);
        }
    }

    private boolean checkExistAppLink(HttpClient client, String authArgs) throws JSONException, HttpException, IOException
    {
        final JSONArray jsonArray = getListAppLink(client, authArgs);
        for(int i = 0; i< jsonArray.length(); i++)
        {
            final String url = jsonArray.getJSONObject(i).getString("rpcUrl");
            Assert.assertNotNull(url);
            if(url.equals(jiraBaseUrl))
            {
                return true;
            }
        }
        return false;
    }

    private JSONArray getListAppLink(HttpClient client, String authArgs) throws HttpException, IOException, JSONException
    {
        final GetMethod m = new GetMethod(confluenceBaseUrl + "/rest/applinks/1.0/applicationlink" + authArgs);
        m.setRequestHeader("Accept", "application/json, text/javascript, */*");

        final int status = client.executeMethod(m);
        Assert.assertEquals(HttpStatus.SC_OK, status);

        final JSONObject jsonObj = new JSONObject(m.getResponseBodyAsString());
        return jsonObj.getJSONArray("applicationLinks");
    }

}
