package it.com.atlassian.confluence.plugins.jiracreate.pageobjects;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedQuery;

public class EpicLinkIssue
{
    @ElementBy(id = "jira-epic-content")
    private PageElement jiraEpicContent;

    @ElementBy(cssSelector = "#jira-epic-content #epic-link .checkbox")
    private PageElement epicLinkInput;

    @ElementBy(cssSelector = "#jira-epic-content #epic-link .jira-issue")
    private PageElement epicJiraIssue;

    public boolean isEpicLinkAvailable()
    {
        return epicLinkInput.isVisible();
    }

    public void selectEpicLink()
    {
        epicLinkInput.click();
    }

    public String getEpicContent()
    {
        return jiraEpicContent.getText();
    }

    public TimedQuery<String> getEpicIssueTitle()
    {
        return epicJiraIssue.timed().getAttribute("original-title");
    }

    public TimedQuery<String> getEpicIssueText()
    {
        return epicJiraIssue.timed().getText();
    }

    public void clickOnEpicIssue()
    {
        epicJiraIssue.click();
    }

}
