package it.com.atlassian.confluence.plugins.jiracreate;

import com.atlassian.pageobjects.elements.query.Poller;
import it.com.atlassian.confluence.plugins.jiracreate.pageobjects.CreateIssueDialog;
import it.com.atlassian.confluence.plugins.jiracreate.pageobjects.CreateIssuePanelButton;

import java.io.IOException;

import it.com.atlassian.confluence.plugins.jiracreate.rest.CreateJiraContentRestHelper;
import junit.framework.Assert;

import org.json.JSONException;
import org.junit.After;
import org.junit.Test;

import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.SpacePermission;
import com.atlassian.confluence.it.User;

public class CreateJiraIssuePanelTest extends AbstractJIRACreateTest
{
    
    @Test
    public void createIssueButtonNotShownWithoutApplink() throws JSONException
    {
        applinkHelper.removeAllApplink();
        
        CreateIssuePanelButton createIssuePanelButton = selectTextAndOpenPanelButton();
        Assert.assertFalse(createIssuePanelButton.isCreateIssuePanelButtonSummaryPresent());
    }
    
    @Test
    public void createIssueButtonShownWithApplink() throws IOException, JSONException
    {
        applinkHelper.removeAllApplink();
        applinkHelper.setupBasicAndActivate();
        
        CreateIssuePanelButton createIssuePanelButton = selectTextAndOpenPanelButton();
        Assert.assertTrue(createIssuePanelButton.isCreateIssuePanelButtonSummaryPresent());
    }
    
    private CreateIssuePanelButton selectTextAndOpenPanelButton() 
    {
        product.loginAndView(User.ADMIN, Page.TEST);
        SelectionTextHelper.selectTextNode(product.getTester().getDriver(), "#main-content>p", 0, 5);
        return product.getPageBinder().bind(CreateIssuePanelButton.class);
    }
    
    @After
    public void restoreApplicationLink() throws IOException, JSONException
    {
        applinkHelper.removeAllApplink();
        applinkHelper.setupBasicAndActivate();
    }

    @Test
    public void createIssueButtonNotShowForAnonymousUser()
    {
        rpc.grantAnonymousUsePermission();
        rpc.grantAnonymousPermission(SpacePermission.VIEW, Space.TEST);

        product.viewPage(Page.TEST.getIdAsString());
        SelectionTextHelper.selectTextNode(product.getTester().getDriver(), "#main-content>p", 0, 5);
        CreateIssuePanelButton createIssuePanelButton = product.getPageBinder().bind(CreateIssuePanelButton.class);
        Assert.assertFalse(createIssuePanelButton.isCreateIssuePanelButtonSummaryPresent());
    }

    @Test
    public void createIssueDialogTargetElementBehavesAsExpected()
    {
        CreateJiraContentRestHelper.setDiscoveredred(rpc.getBaseUrl(), User.TEST);
        product.loginAndView(User.TEST, createPageWithText());
        CreateIssueDialog createIssueDialog = openCreateIssueDialog("#main-content>p", 0, 5);
        Poller.waitUntilTrue(createIssueDialog.isDialogTargetPresent());
        createIssueDialog.clickCancelButton();
        Poller.waitUntilFalse(createIssueDialog.isDialogTargetPresent());
        createIssueDialog = openCreateIssueDialog("#main-content>p", 0, 5);
        Poller.waitUntilTrue(createIssueDialog.isDialogTargetPresent());
    }
}
