package com.atlassian.confluence.plugins.createjiracontent.services;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.confluence.plugins.createjiracontent.entities.FeatureDiscovery;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.sal.api.user.UserKey;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * TODO: copied implementation from https://stash.atlassian.com/projects/CONF/repos/confluence-jira-metadata/pull-requests/28/overview
 * remove it when have generic feature discovery service:  https://jira.atlassian.com/browse/CONFDEV-21132
 */
@RunWith(MockitoJUnitRunner.class)
public class DefaultFeatureDiscoveryServiceTest
{
    @Mock
    private ActiveObjects ao;
    @Mock
    private ConfluenceUser user;
    @Mock
    private FeatureDiscovery discovery;

    private DefaultFeatureDiscoveryService service;

    @Before
    public void setup()
    {
        service = new DefaultFeatureDiscoveryService(ao);
        when(user.getKey()).thenReturn(new UserKey("foo"));
    }

    @Test
    public void testUnknownUser()
    {
        assertFalse(service.hasUserDiscovered(user));
    }

    @Test
    public void testUndiscoveredUser()
    {
        when(ao.find(eq(FeatureDiscovery.class), anyString(), anyString())).thenReturn(new FeatureDiscovery[] { discovery });
        assertFalse(service.hasUserDiscovered(user));
        verify(discovery).getDiscovered();
    }

    @Test
    public void testDiscoveredUser()
    {
        when(ao.find(eq(FeatureDiscovery.class), anyString(), anyString())).thenReturn(new FeatureDiscovery[] { discovery });
        when(discovery.getDiscovered()).thenReturn(true);
        assertTrue(service.hasUserDiscovered(user));
    }

    @Test
    public void testSetDiscovered()
    {
        when(ao.find(eq(FeatureDiscovery.class), anyString(), anyString())).thenReturn(new FeatureDiscovery[] { discovery });
        service.setUserDiscovered(user, true);
        verify(discovery).setDiscovered(true);
    }
}