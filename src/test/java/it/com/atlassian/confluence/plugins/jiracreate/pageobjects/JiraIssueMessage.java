package it.com.atlassian.confluence.plugins.jiracreate.pageobjects;

import java.util.Iterator;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.atlassian.confluence.pageobjects.component.ConfluenceAbstractPageComponent;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.google.common.base.Predicate;

public class JiraIssueMessage extends ConfluenceAbstractPageComponent
{
    @ElementBy(id = "jiraserver-issue-messages")
    private PageElement authIssueMessages;
    
    @ElementBy(cssSelector = "#jiraserver-issue-messages .oauth-init")
    private PageElement authIssueMessagesApproveButton;
    
    @ElementBy(cssSelector="#jiraserver-issue-messages .aui-message")
    private PageElement jiraConnectivityMessage;
    
    @ElementBy(cssSelector="#jiraserver-issue-messages .oauth-init")
    private PageElement loginAndApprove;
    
    
    //oauth/authorized page in JIRA
    @ElementBy(id = "login-form-username")
    private PageElement jiraUsername;
    
    @ElementBy(id = "login-form-password")
    private PageElement jiraPassword;
    
    @ElementBy(id = "login-form-submit")
    private PageElement jiraSubmit;
    
    @ElementBy(id = "approve")
    private PageElement jiraApprove;
    
    public String getAuthIssueMessage()
    {
        return authIssueMessages.getText();
    }
    
    public boolean isOAuthButtonAvailable()
    {
        return authIssueMessagesApproveButton.isVisible();
    }
    public String getJiraConnectivityMessage()
    {
        return jiraConnectivityMessage.getText();
    }
    
    public void authorizedOauthentication() 
    {
        final String parentWindowHandle = driver.getWindowHandle();
        loginAndApprove.click();
        
        /**
         * 1. Waiting for the JIRA login page
         * 2. Do the login -> redirect to "authorized" page
         * 3. Approve it.
         */
        WebDriverWait wait = new WebDriverWait(driver, 100);
        
        wait.until(new Predicate<WebDriver>()
        {
            @Override
            public boolean apply(WebDriver wdriver)
            {
                Iterator<String> windowIterator = wdriver.getWindowHandles().iterator();
                while (windowIterator.hasNext())
                {
                    String windowHandle = windowIterator.next();
                    if (StringUtils.isNotEmpty(windowHandle) && !windowHandle.equals(parentWindowHandle))
                    {
                        driver.switchTo().window(windowHandle);
                        if (jiraUsername.isVisible())
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        });
        jiraUsername.type("admin");
        jiraPassword.type("admin");
        jiraSubmit.click();
        jiraApprove.click();
        
        //back to main window
        driver.switchTo().window(parentWindowHandle);
    }
}
