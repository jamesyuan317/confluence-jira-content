AJS.toInit(function($) {
    var PLUGIN_KEY = "com.atlassian.confluence.plugins.confluence-jira-content:create-JIRA-issue-summary";
    
    var $msgPanel = $("div.jira-issues-created");
    if ($msgPanel.length > 0) {
        //Trying to remove get parameters
        if (window.history && window.history.replaceState) {
            var currentPageHref = window.location.href;
            var originalPageHref = currentPageHref.substr(0, currentPageHref.indexOf("JIRAIssuesCreated") - 1);
            window.history.replaceState({}, document.title, originalPageHref);
        }
        var $contentMessage = $msgPanel.find("#jira-content-message-panel-error-warning");
        var $viewMoreLink = $msgPanel.find("#jira-content-message-panel-view-more-link");
        $viewMoreLink.click(function(e) {
            e.preventDefault();
            $viewMoreLink.hide();
            $contentMessage.show();
        });
        //Hide the success message panel after 10 seconds. Otherwise let user manually dismiss it.
        if ($msgPanel.hasClass("success")) {
            setTimeout(function() { $msgPanel.hide(); }, 10000);
        }
    }

    var jiraIssueDialog;
    var featureDiscoveryDialog;

    var showCreateIssueDialog = function(selectionObject) {
        if(jiraIssueDialog) {
            jiraIssueDialog.remove();
        }
        jiraIssueDialog = Confluence.CreateJiraContent.InlineDialog.openCreateIssueDialog(selectionObject);
        jiraIssueDialog.show();
        // Expose the dialog so we can call dialog.refresh() on it whenever content changes
        Confluence.CreateJiraContent.displayDialog = jiraIssueDialog;
        Confluence.CreateJiraContent.Analytics.sendAnalyticsForDialogOpen();
    };

    var showFeatureDiscoveryDialog = function(selectionObject) {
        if(featureDiscoveryDialog) {
            featureDiscoveryDialog.remove();
        }
        featureDiscoveryDialog = Confluence.CreateJiraContent.InlineDialog.openFeatureDiscoveryDialog(selectionObject, showCreateIssueDialog);
        featureDiscoveryDialog.show();
    };

    var callback = function(selectionObject) {
        if (Confluence.CreateJiraContent.FeatureDiscoveryDialog.shouldShowFeatureDiscovery()) {
            showFeatureDiscoveryDialog(selectionObject);
        } else {
            showCreateIssueDialog(selectionObject); 
        }
    };

    // check if plugin Confluence-highlight-action is enabled
    Confluence && Confluence.HighlightAction && Confluence.HighlightAction.registerButtonHandler(PLUGIN_KEY, {
        onClick: callback,
        shouldDisplay: Confluence.HighlightAction.WORKING_AREA.MAINCONTENT_ONLY // this plugin should be only works on Main Content
    });
});