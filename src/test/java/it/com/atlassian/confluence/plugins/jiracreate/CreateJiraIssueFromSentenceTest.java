package it.com.atlassian.confluence.plugins.jiracreate;

import it.com.atlassian.confluence.plugins.jiracreate.pageobjects.CreateIssueDialog;
import it.com.atlassian.confluence.plugins.jiracreate.pageobjects.EpicLinkIssue;
import it.com.atlassian.confluence.plugins.jiracreate.pageobjects.PageAfterCreate;
import it.com.atlassian.confluence.plugins.jiracreate.rest.CreateJiraContentRestHelper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import junit.framework.Assert;

import org.apache.commons.lang.StringUtils;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.it.rpc.ConfluenceRpc;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.pageobjects.elements.query.Poller;

public class CreateJiraIssueFromSentenceTest extends AbstractJIRACreateTest
{
    @Before
    public void setUp()
    {
        CreateJiraContentRestHelper.setDiscoveredred(rpc.getBaseUrl(), User.TEST);
    }

    private CreateIssueDialog openCreateIssueDialog()
    {
        return openCreateIssueDialog("#main-content>p", 0, 17);
    }

    @Test
    public void displayCreateIssueDialogWithSelectedText()
    {
        product.loginAndView(User.TEST, createPageWithText());
        CreateIssueDialog dialog = openCreateIssueDialog();
        Poller.waitUntilTrue(dialog.isCreateIssueDialogVisible());
        Assert.assertEquals("This is test page", dialog.getSummaryValue());
    }

    @Test
    public void disableIssueTypeSelectBoxIfProjectIsNotSelected()
    {
        product.loginAndView(User.TEST, createPageWithText());
        CreateIssueDialog dialog = openCreateIssueDialog();
        Assert.assertTrue(dialog.isJiraIssueTypeSelectBoxDisabled());
    }

    private Page createPageWithoutLogin()
    {
        String title = "Create New Page for SelectedValue";
        String content = "This is test page, then use it for testing.";
        Page page = new Page(Space.TEST, title, content);

        rpc.createPage(page);

        return page;
    }

    @Test
    public void checkCreateIssueAndInsertJiraIssueMacro()
    {
        product.loginAndView(User.TEST, createPageWithText());

        String projectName = "Test Project";
        String issueTypeName = "Task";
        String summary = "Test summary";
        String description = "Test description";

        openCreateIssueDialog()
                .selectProject(projectName)
                .selectIssueType(issueTypeName)
                .setSummary(summary)
                .setDescription(description)
                .clickSubmitButton();

        PageAfterCreate page = product.getPageBinder().bind(PageAfterCreate.class);
        Assert.assertTrue("Message panel should be presented", page.isMsgPanelPresent());
        String issueKey = page.getCreatedIssueKey();
        JiraIssueBean issueBean = new JiraIssueBean(issueKey, summary, description, projectName, issueTypeName);
        validateJiraIssue(issueBean);
        String expectedMsg = String.format("%s has been created. Your page has been updated to reflect the status of the issue.", issueKey);
        Assert.assertEquals(expectedMsg, page.getMsg());
    }
    
    @Test
    public void checkCreateIssueAndCanNotInsertJiraIssueMacro()
    {
        product.loginAndView(User.TEST, createPageWithJiraIssueMacro());

        String projectName = "Test Project";
        String issueTypeName = "Task";
        String summary = "Test summary";
        String description = "Test description";

        openCreateIssueDialog("#main-content>p", 0, 6)
                .selectProject(projectName)
                .selectIssueType(issueTypeName)
                .setSummary(summary)
                .setDescription(description)
                .clickSubmitButton();

        PageAfterCreate page = product.getPageBinder().bind(PageAfterCreate.class);
        Assert.assertTrue("Message panel should be present", page.isMsgPanelPresent());
        String issueKey = page.getCreatedIssueKey();
        JiraIssueBean issueBean = new JiraIssueBean(issueKey, summary, description, projectName, issueTypeName);
        validateJiraIssue(issueBean);
        String expectedMsg = String.format("%s has been created, but there is a problem in adding it to the page.\nYou can manually add it later using JIRA macro.", issueKey);
        Assert.assertEquals(expectedMsg, page.getMsg());
    }

    @Test
    public void checkProjectIconIsDisplayed() throws UnsupportedEncodingException
    {
        product.loginAndView(User.TEST, createPageWithText());

        String projectName = "Test Project";
        String projectIconUrl = "/secure/projectavatar?pid=10011&avatarId=10011&size=small";
        projectIconUrl = URLEncoder.encode(projectIconUrl, "UTF-8");
        // Check project icon is visible in the project option list
        CreateIssueDialog dialog = openCreateIssueDialog()
                                .showProjectOptionList();
        Assert.assertTrue(dialog.isIconVisibleInOptionList(projectIconUrl));
        // Check project icon is visible in the dialog when project was selected
        dialog.chooseProject(projectName);
        Assert.assertTrue(dialog.isProjectIconVisible(projectIconUrl));
    }

    @Test
    public void checkIssueTypeIconIsDisplayed()
    {
        product.loginAndView(User.TEST, createPageWithText());
        String projectName = "Test Project";
        CreateIssueDialog dialog = openCreateIssueDialog()
                                .selectProject(projectName)
                                .showIssueTypeOptionList();
        String issueTypeIconUrl = "/images/icons/issuetypes/task.png";
        // Check issue type icon is visible in the issue type option list
        Assert.assertTrue(dialog.isIconVisibleInOptionList(issueTypeIconUrl));
        // Check project icon is visible in the dialog when project was selected
        String issueTypeName = "Task";
        dialog.chooseIssueType(issueTypeName);
        Assert.assertTrue(dialog.isIssueTypeIconVisible(issueTypeIconUrl));
    }

    private Page createPageWithJiraIssueMacro() {
        String title = "Page with Jira issue macro";
        String content = "<p><ac:macro ac:name=\"cheese\"/></p>";

        Page page = new Page(Space.TEST, title, content);
        ConfluenceRpc newInstance = ConfluenceRpc.newInstance(product.getProductInstance().getBaseUrl());
        newInstance.logIn(User.ADMIN);
        newInstance.createPage(page);
        return page;
    }

    private Page createPageWithLongText()
    {
        String title = "page with long text";
        String content = "Welcome to the Atlassian Developer Documentation!" +
                "Using the Atlassian plugin software developer kit (SDK), developers can create plugins that extend the functionality of " +
                "Atlassian applications such as JIRA, Confluence, and others. The SDK allows you to quickly connect to and leverage the plugin development platform." +
                "We offer a number of infrastructure tools to help you create, market and sell your plugins. For example, thousands of customers visit theAtlassian " +
                "Marketplace and the Universal Plugin Manager every day, to find plugins they may want to install.";
        Page page = new Page(Space.TEST, title, content);

        rpc.logIn(User.ADMIN);
        rpc.createPage(page);

        return page;
    }

    @Test
    public void displayCreateIssueDialogWithSelectedLongText()
    {
        String expected = "Welcome to the Atlassian Developer Documentation!Using the Atlassian plugin software developer kit (SDK), developers can create plugins that extend the functionality of Atlassian applications such as JIRA, Confluence, and others. The SDK allows you to quickly connect to and leverage the plugin devel";
        product.loginAndView(User.TEST, createPageWithLongText());
        CreateIssueDialog dialog = openCreateIssueDialog("#main-content>p", 0, 300);
        dialog.selectProject("Test Project").selectIssueType("Task");
        dialog.clickSubmitButton();
        PageAfterCreate page = product.getPageBinder().bind(PageAfterCreate.class);
        String issueName = page.getCreatedIssueKey();
        Issue issue = testKitJIRA.issues().getIssue(issueName);
        Assert.assertTrue("The summary must be less or equal than 255", issue.fields.summary.length() <= 255);
        Assert.assertTrue("The description must not be empty", !issue.fields.description.isEmpty());
        Assert.assertEquals(issue.fields.description, expected);
    }
    
    private Page createPagewithEpic()
    {
        String content = this.getDumpXmlData("epic-page.xml");
        
        Page page = new Page(Space.TEST, "Page with Epic", content);
        ConfluenceRpc newInstance = ConfluenceRpc.newInstance(product.getProductInstance().getBaseUrl());
        newInstance.logIn(User.ADMIN);
        newInstance.createPage(page);
        return page;
    }

    private Page createPagewithLongArticle()
    {
        String content = this.getDumpXmlData("longArticle.xml");

        Page page = new Page(Space.TEST, "Page with Long Article", content);
        ConfluenceRpc newInstance = ConfluenceRpc.newInstance(product.getProductInstance().getBaseUrl());
        newInstance.logIn(User.ADMIN);
        newInstance.createPage(page);
        return page;
    }
    
    @Test
    public void createIssueWithoutEpiclinkDetect()
    {
        product.loginAndView(User.TEST, Page.TEST);
        EpicLinkIssue epicLinkIssue = openCreateIssueDialog("#main-content>p", 0, 6)
                .selectProject("Test Project")
                .selectIssueType("Bug")
                .getEpicLinkIssue();
        MatcherAssert.assertThat(epicLinkIssue.getEpicContent(), Matchers.containsString(StringUtils.EMPTY));
    }
    
    @Test
    public void createIssueWithEpicLink() throws IOException
    {
        final String epicIssueKey = "TST-2";
        product.loginAndView(User.TEST, createPagewithEpic());
        CreateIssueDialog createIssueDialog = openCreateIssueDialog();

        //verify information on Create-Issue-Dialog
        EpicLinkIssue epicLinkIssue = createIssueDialog
                .selectProject("Test Project")
                .selectIssueType("Bug")
                .getEpicLinkIssue();
        Assert.assertTrue(epicLinkIssue.isEpicLinkAvailable());
        MatcherAssert.assertThat(epicLinkIssue.getEpicContent(), Matchers.containsString(epicIssueKey));

        //Verify page after create issue
        createIssueDialog.clickSubmitButton();
        PageAfterCreate page = product.getPageBinder().bind(PageAfterCreate.class);
        Assert.assertTrue(page.isMsgPanelPresent());
        String issueKey = page.getCreatedIssueKey();
        MatcherAssert.assertThat(page.getMsg(), Matchers.containsString(issueKey));

        //Verify issue on JIRA
        Issue issue = testKitJIRA.issues().getIssue(issueKey);
        Assert.assertEquals(epicIssueKey, issue.fields.get("customfield_10016"));
    }

    @Test
    public void createIssueWithStoreValue()
    {
        product.loginAndView(User.TEST, createPageWithoutLogin());

        String projectName = "Test Project";
        String issueTypeName = "Task";

        // Create first issue to store SelectedValue
        openCreateIssueDialog("#main-content>p", 0, 7)
            .selectProject(projectName)
            .selectIssueType(issueTypeName)
            .clickSubmitButton();

        // Wait until the Ajax call completely (reload)
        PageAfterCreate page = product.getPageBinder().bind(PageAfterCreate.class);
        Assert.assertTrue(page.isMsgPanelPresent());

        // Create another page
        product.viewPage(String.valueOf(createPageWithText().getId()));

        // Create second issue to check if SelectedValue is loaded correctly and works
        CreateIssueDialog dialogWithSelectedValue = openCreateIssueDialog();

        // Project and IssueType are should not visible
        Assert.assertTrue(dialogWithSelectedValue.isSelectedPanelVisible());
        Assert.assertFalse(dialogWithSelectedValue.isProjectFieldVisible());
        Assert.assertFalse(dialogWithSelectedValue.isIssueTypeFieldVisible());

        // Project and IssueType value should be bind correctly
        dialogWithSelectedValue.clickEditLink();
        Assert.assertEquals(projectName, dialogWithSelectedValue.getSelectedJiraProjectValue());
        Assert.assertEquals(issueTypeName, dialogWithSelectedValue.getSelectedJiraIssueTypeValue());

        // Create new Issue from SelectedValue
        String summaryBySelectedValue = "Summary: Create ticket by SelectedValue";
        String descriptionBySelectedValue = "Description: Create ticket by SelectedValue";
        dialogWithSelectedValue.setSummary(summaryBySelectedValue);
        dialogWithSelectedValue.setDescription(descriptionBySelectedValue);
        dialogWithSelectedValue.clickSubmitButton();

        // Verify if that Issue create successful
        PageAfterCreate pageBySelectedValue = product.getPageBinder().bind(PageAfterCreate.class);
        Assert.assertTrue("Message panel should be presented", pageBySelectedValue.isMsgPanelPresent());
        String issueKeyBySelectedValue = pageBySelectedValue.getCreatedIssueKey();
        JiraIssueBean issueBeanBySelectedValue = new JiraIssueBean(issueKeyBySelectedValue, summaryBySelectedValue, descriptionBySelectedValue, projectName, issueTypeName);
        validateJiraIssue(issueBeanBySelectedValue);
    }

    @Test
    public void createIssueWithInvalidStoreValue()
    {
        product.loginAndView(User.TEST, createPageWithoutLogin());

        // Fake the storedvalue
        product.getTester().getDriver().executeScript("window.localStorage.setItem('confluence.test1.jira-content.savedFormValues'," +
                "\"{'serverId':'029221ac-3cf1-324c-8c61-553f08975f1e','project':{'id':'10011','text':'Test Project'},'issueType':{'id':'2','text':'New Feature'}}\" )");

        CreateIssueDialog dialog = openCreateIssueDialog();

        // If storedValue is invalid, Dialog should show as normally.
        // Project and IssueType must visible and Summary must be hidden
        Assert.assertTrue(dialog.isProjectFieldVisible());
        Assert.assertTrue(dialog.isJiraIssueTypeSelectBoxDisabled());
        Assert.assertFalse(dialog.isSelectedPanelRendered());
    }

    @Test
    /**
     * We storevalue right after dialog is hidden.
     * When Cancel is click, we dont need to store value.
     */
    public void StoreValueWhenClickCancel()
    {
        product.loginAndView(User.TEST, createPageWithoutLogin());

        String projectName = "Test Project";
        String issueTypeName = "Task";

        // Try to select another project, issue type then click cancel
        CreateIssueDialog createIssueDialog = openCreateIssueDialog();

        // We have to use script to select project because selectProject() works incorrect with storevalue
        createIssueDialog.selectProject(projectName)
            .selectIssueType(issueTypeName)
            .clickCancelButton();

        // make sure that it doesn't save new values.
        createIssueDialog = openCreateIssueDialog("#main-content>p", 0, 5);
        Assert.assertEquals("Select project", createIssueDialog.getSelectedJiraProjectValue());
        Assert.assertTrue(createIssueDialog.isJiraIssueTypeSelectBoxDisabled());
    }

    @Test
    /**
     * We storevalue right after dialog is hidden.
     * When click outside of dialog, dialog will auto hide and trigger our code to save the current value.
     */
    public void StoreValueWhenClickOutsideDialog()
    {
        product.loginAndView(User.TEST, createPageWithoutLogin());

        String projectName = "Test Project";
        String issueTypeName = "Task";

        CreateIssueDialog createIssueDialog = openCreateIssueDialog();

        // We have to use script to select project because selectProject() works incorrect with storevalue
        createIssueDialog.selectProject(projectName)
                .selectIssueType(issueTypeName);

        // click on body to hide the dialog
        product.getTester().getDriver().executeScript("$('body').trigger('click');");

        // re-selecting text should dismiss the create jira dialog and open the highlight actions
        createIssueDialog = openCreateIssueDialog("#main-content>p", 5, 10);
        Poller.waitUntilTrue(createIssueDialog.isCreateIssueDialogVisible());

        createIssueDialog.clickEditLink();
        Assert.assertEquals(projectName, createIssueDialog.getSelectedJiraProjectValue());
        Assert.assertEquals(issueTypeName, createIssueDialog.getSelectedJiraIssueTypeValue());
    }

    @Test
    public void dontShowSuggestionPanelWhenCreateIssueFromSentence(){
        product.loginAndView(User.TEST, createPageWithoutLogin());
        CreateIssueDialog createIssueDialog = openCreateIssueDialog();

        // make sure that suggestion panel doesn't show when create single jira issue
        Assert.assertFalse(createIssueDialog.isSuggestionPanelPresent());
    }

    @Test
    public void epicLinkTooltip() throws InterruptedException
    {
        final String expected = "Here is epic for confluence test";
        product.loginAndView(User.TEST, createPagewithEpic());
        CreateIssueDialog createIssueDialog = openCreateIssueDialog();
        EpicLinkIssue epicLinkIssue = createIssueDialog
                .selectProject("Test Project")
                .selectIssueType("Bug")
                .getEpicLinkIssue();
        Assert.assertTrue(epicLinkIssue.isEpicLinkAvailable());
        epicLinkIssue.clickOnEpicIssue();
        Thread.sleep(1000);
        Poller.waitUntil(epicLinkIssue.getEpicIssueTitle(), Matchers.containsString(expected), Poller.by(1000 * 15));
    }

    @Test
    public void inlineDialogBelowHighLightText()
    {
        product.loginAndView(User.TEST, createPagewithLongArticle());

        CreateIssueDialog dialog = openCreateIssueDialog("#main-content>div>p", 0, 10);
        Assert.assertFalse(dialog.isCreateIssueDialogShowAboveHightlightText());
    }

    @Test
    public void inlineDialogAboveHighLightText()
    {
        product.loginAndView(User.TEST, createPagewithLongArticle());

        product.getTester().getDriver().executeScript("window.scrollBy(0, 5000)");
        CreateIssueDialog dialogUp = openCreateIssueDialog("#main-content>div p:last", 0, 10);
        Assert.assertTrue(dialogUp.isCreateIssueDialogShowAboveHightlightText());
    }

    @Test
    public void createIssueWithOldPageVersion() throws InterruptedException
    {
        String expected = "These can't be added to the page because you were viewing an outdated version of this page.";
        String content = this.getDumpXmlData("old-page-version.xml");
        String newContent = this.getDumpXmlData("new-page-version.xml");
        Page page = new Page(Space.TEST, "old-page-version", content);
        ConfluenceRpc newInstance = ConfluenceRpc.newInstance(product.getProductInstance().getBaseUrl());
        newInstance.logIn(User.ADMIN);
        newInstance.createPage(page);

        product.loginAndView(User.TEST, page);
        CreateIssueDialog createIssueDialog = openCreateIssueDialog();
        createIssueDialog.selectProject("Test Project").selectIssueType("Bug");

        // make new version of page
        page.setContent(newContent);
        newInstance.savePage(page);

        createIssueDialog.clickSubmitButton();

        PageAfterCreate pageAfterCreate = product.getPageBinder().bind(PageAfterCreate.class);
        Assert.assertTrue(pageAfterCreate.isWarningMsgPanelDisplay());
        Assert.assertTrue(pageAfterCreate.getMsg().contains(expected));
    }
    
    @Test
    public void checkCreateIssueWhenDescriptionFieldIsRequired()
    {
        product.loginAndView(User.TEST, createPageWithText());

        String projectName = "Special Project 1";
        String issueTypeName = "Task";
        String summary = "Test summary";
        String description = "Test description";

        CreateIssueDialog dialog = openCreateIssueDialog()
                .selectProject(projectName)
                .selectIssueType(issueTypeName)
                .setSummary(summary);

        // description field is required, can not create issue when description field is empty
        Poller.waitUntilTrue("Submit button is disabled when description is required", dialog.isSubmitButtonDisabled());

        // placeholder for description field has changed
        Assert.assertEquals("Enter description (required)", dialog.getDescriptionPlaceholder());

        // input description and can create issue
        dialog.setDescription(description);
        Poller.waitUntilFalse("Submit button is enabled when description was inputted", dialog.isSubmitButtonDisabled());
    }

    @Test
    public void checkCreateIssueWhenDescriptionFieldIsHidden()
    {
        product.loginAndView(User.TEST, createPageWithText());

        String projectName = "Special Project 2";
        String issueTypeName = "Story";
        String summary = "Test summary";

        CreateIssueDialog dialog = openCreateIssueDialog()
                                    .selectProject(projectName)
                                    .selectIssueType(issueTypeName)
                                    .setSummary(summary);

        // description input is hidden
        Assert.assertTrue("Description input is invisible", !dialog.isDescriptionFieldVisible());
        
        // can create issue without error
        dialog.clickSubmitButton();

        // Verify if that Issue create successful
        PageAfterCreate pageBySelectedValue = product.getPageBinder().bind(PageAfterCreate.class);
        Assert.assertTrue("Message panel should be presented", pageBySelectedValue.isMsgPanelPresent());
        
        String issueKeyBySelectedValue = pageBySelectedValue.getCreatedIssueKey();
        JiraIssueBean issueBeanBySelectedValue = new JiraIssueBean(issueKeyBySelectedValue, summary, null, projectName, issueTypeName);
        validateJiraIssue(issueBeanBySelectedValue);
    }
}
