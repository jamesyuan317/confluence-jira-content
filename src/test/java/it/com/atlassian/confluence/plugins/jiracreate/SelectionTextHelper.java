package it.com.atlassian.confluence.plugins.jiracreate;

import com.atlassian.webdriver.AtlassianWebDriver;

import java.util.concurrent.TimeUnit;

public class SelectionTextHelper
{
    public static void selectTextNode(AtlassianWebDriver driver, String jquerySelector, int start, int to)
    {
        driver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);
        String waitForAjaxToComplete =  "var callback = arguments[arguments.length - 1]; window.setInterval(function() { !AJS.$.active && callback(); }, 100);";
        driver.executeAsyncScript(waitForAjaxToComplete);

        String scriptTemplate = "var textNode = $('%s').get(0).firstChild;"
                + "var range = document.createRange();"
                + "range.setStart(textNode, %s);" 
                + "range.setEnd(textNode, %s);"
                + "window.getSelection().addRange(range);"
                + "AJS.$('.wiki-content').first().trigger('mouseup');";

        driver.executeScript(String.format(scriptTemplate, jquerySelector, start, to));
    }
}
