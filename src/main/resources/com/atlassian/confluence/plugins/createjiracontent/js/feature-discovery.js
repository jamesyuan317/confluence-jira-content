Confluence.CreateJiraContent.FeatureDiscoveryDialog = (function($) {
    // feature discovery dialog
    var featureDiscoveryDialog;
    var isFirstTime = AJS.Meta.get("create-issue-metadata-show-discovery");

    var setFeatureDialogObject = function(dialog) {
        featureDiscoveryDialog = dialog;
    };

    var bindFeatureDiscoveryEvents = function($featureDiscoveryDialog, selectionObject, showCreateIssueDialogFn) {
        var $closeLink = $featureDiscoveryDialog.find("#feature-discovery-close");
        $closeLink.on("click", function(e) {
            // disable Feature Discovery dialog when click close button
            disableFeatureDiscovery();
            featureDiscoveryDialog.hide();
        });

        var $showCreateIssue = $featureDiscoveryDialog.find("#show-create-issue");
        $showCreateIssue.on("click", function(e) {
            disableFeatureDiscovery();
            featureDiscoveryDialog.hide();
            showCreateIssueDialogFn(selectionObject);
        });
    };

    var addFeatureDiscoveryContent = function($featureDiscoveryDialog, selectionObject, showCreateIssueDialogFn) {
        $featureDiscoveryDialog.html(Confluence.CreateJiraContent.Templates.FeatureDiscovery.createFeatureDiscoveryContent());
        bindFeatureDiscoveryEvents($featureDiscoveryDialog, selectionObject, showCreateIssueDialogFn);
    };

    var shouldShowFeatureDiscovery = function() {
        return isFirstTime;
    };

    // Indicates that feature discovery popup should no longer appear for this user.
    var disableFeatureDiscovery = function() {
        isFirstTime = false;
        $.ajax({
            url : AJS.Confluence.getBaseUrl() + "/rest/createjiracontent/1.0/metadata/discovered",
            type : "PUT"
        });
    };

    return {
        shouldShowFeatureDiscovery : shouldShowFeatureDiscovery,
        disableFeatureDiscovery : disableFeatureDiscovery,
        addFeatureDiscoveryContent : addFeatureDiscoveryContent,
        setFeatureDialogObject : setFeatureDialogObject
    };
})(AJS.$);