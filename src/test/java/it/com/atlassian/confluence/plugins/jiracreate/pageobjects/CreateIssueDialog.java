package it.com.atlassian.confluence.plugins.jiracreate.pageobjects;

import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.By;

import com.atlassian.confluence.pageobjects.component.ConfluenceAbstractPageComponent;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedQuery;

public class CreateIssueDialog extends ConfluenceAbstractPageComponent
{
    private static final String JIRA_SERVER_PICKER_ID =  "jira-servers";
    private static final String JIRA_PROJECT_PICKER_ID =  "jira-projects";
    private static final String JIRA_ISSUE_TYPE_PICKER_ID =  "jira-issue-types";
    private static final String SUMMARY_COLUMN_PICKER_ID =  "summary-columns";
    
    @ElementBy(id = "inline-dialog-create-issue-dialog")
    private PageElement createIssueDialog;

    @ElementBy(id = "text-suggestion")
    private PageElement suggestionPanel;

    @ElementBy(id = "text-suggested-link")
    private PageElement suggestedLink;

    @ElementBy(id = "table-suggested-link")
    private PageElement tableSuggestedLink;

    @ElementBy(id = "issue-summary")
    private PageElement summaryField;

    @ElementBy(id = "issue-description")
    private PageElement descriptionField;

    @ElementBy(cssSelector = "#inline-dialog-create-issue-dialog .create-issue-submit")
    private PageElement submitButton;

    @ElementBy(cssSelector = "#inline-dialog-create-issue-dialog .create-issue-cancel")
    private PageElement cancelButton;

    @ElementBy(id = JIRA_SERVER_PICKER_ID)
    private PageElement serverSelect;

    @ElementBy(id = JIRA_PROJECT_PICKER_ID)
    private PageElement projectSelect;

    @ElementBy(id = JIRA_ISSUE_TYPE_PICKER_ID)
    private PageElement issueTypeSelect;

    @ElementBy(cssSelector = "#inline-dialog-create-issue-dialog .summary-stored-value")
    private PageElement selectedPanel;

    @ElementBy(cssSelector = "#inline-dialog-create-issue-dialog .summary-stored-value a")
    private PageElement editLink;

    @ElementBy(id = "issue-remaining")
    private PageElement remainingIssues;

    @ElementBy(cssSelector = ".issue-container img")
    protected PageElement issueTypeImage;

    @ElementBy(id = "issue-content")
    private PageElement issueContent;

    @ElementBy(id = "arrow-create-issue-dialog")
    private PageElement createIssueDialogDownArrow;

    @ElementBy(cssSelector = "#prepare-issue-messages .aui-message.warning")
    private PageElement sortedTableWarningMessage;

    @ElementBy(id = "create-issue-form-messages")
    private PageElement formMessages;

    @ElementBy(cssSelector = ".create-issue-cancel")
    private PageElement closeDialogButton;

    @ElementBy(id = "jira-content-dialog-target")
    private PageElement dialogTarget;

    public TimedQuery<Boolean> isCreateIssueDialogVisible()
    {
        return createIssueDialog.timed().isVisible();
    }

    public String getSummaryValue()
    {
        return summaryField.getValue();
    }

    public String getDescriptionValue()
    {
        return descriptionField.getValue();
    }

    public String getSuggestedText()
    {
        return suggestionPanel.getText();
    }

    public boolean hasNoSuggestion()
    {
        return suggestionPanel.getText().trim().isEmpty();
    }

    public CreateIssueDialog switchToTableMode()
    {
        suggestedLink.click();
        return this;
    }

    public CreateIssueDialog switchToTextMode()
    {
        tableSuggestedLink.click();
        return this;
    }

    public CreateIssueDialog selectJiraServer(String value)
    {
        Picker jiraServersPicker = getPicker(JIRA_SERVER_PICKER_ID);
        jiraServersPicker.openDropdown().chooseOption(value);
        return this;
    }

    public String getSelectedJiraIssueTypeValue()
    {
        Picker jiraIssueTypePicker = getPicker(JIRA_ISSUE_TYPE_PICKER_ID);
        return jiraIssueTypePicker.getSelectedValue();
    }

    public String getSelectedJiraProjectValue()
    {
        Picker projectPicker = getPicker(JIRA_PROJECT_PICKER_ID);
        return projectPicker.getSelectedValue();
    }

    public List<String> getListSummaryColumnValues()
    {
        Picker summaryColumnPicker = getPicker(SUMMARY_COLUMN_PICKER_ID);
        return summaryColumnPicker.openDropdown().getAllValues();
    }
    
    public List<String> getListJiraServerValues()
    {
        Picker jiraServerPicker = getPicker(JIRA_SERVER_PICKER_ID);
        return jiraServerPicker.openDropdown().getAllValues();
    }

    public boolean isJiraServerSelectVisible()
    {
        return serverSelect.isVisible();
    }

    public boolean isProjectFieldVisible()
    {
        return projectSelect.isVisible();
    }

    public boolean isIssueTypeFieldVisible()
    {
        return issueTypeSelect.isVisible();
    }

    public boolean isSelectedPanelVisible()
    {
        return selectedPanel.isVisible();
    }

    public boolean isSelectedPanelRendered()
    {
        return selectedPanel.isPresent();
    }

    public boolean isCreateIssueDialogShowAboveHightlightText()
    {
        // When Inline-Dialog show on above of highlight text, it will add have one more css class, it is "aui-bottom-arrow"
        return createIssueDialogDownArrow.hasClass("aui-bottom-arrow");
    }

    private void waitUntilJiraProjectValuesIsLoaded(Picker jiraProjectPicker)
    {
        Poller.waitUntil(jiraProjectPicker.getSelectedOption().timed().getText(), Matchers.containsString("Select project"),
                         Poller.by(20000));
    }

    public List<String> getListJiraProjectValues()
    {
        Picker jiraProjectPicker = getPicker(JIRA_PROJECT_PICKER_ID);
        waitUntilJiraProjectValuesIsLoaded(jiraProjectPicker);
        return jiraProjectPicker.openDropdown().getAllValues();
    }

    public List<String> getListJiraIssueTypeValues()
    {
        Picker jiraIssueTypePicker = getPicker(JIRA_ISSUE_TYPE_PICKER_ID);
        return jiraIssueTypePicker.openDropdown().getAllValues();
    }

    public boolean isJiraIssueTypeSelectBoxDisabled()
    {
        Picker jiraIssueTypePicker = getPicker(JIRA_ISSUE_TYPE_PICKER_ID);
        return jiraIssueTypePicker.isDisabled();
    }

    public Picker getPicker(String pickerId)
    {
        Picker picker = pageBinder.bind(Picker.class);
        picker.bindingElements(pickerId);
        return picker;
    }

    public CreateIssueDialog selectProject(String projectName)
    {
        if (isJiraServerSelectVisible())
        {
            selectJiraServer("testjira");
        }
        showProjectOptionList();
        chooseProject(projectName);
        return this;
    }

    public CreateIssueDialog showProjectOptionList()
    {
        Picker projectPicker = getPicker(JIRA_PROJECT_PICKER_ID);
        waitUntilJiraProjectValuesIsLoaded(projectPicker);
        projectPicker.openDropdown();
        return this;
    }

    public CreateIssueDialog chooseProject(String projectName)
    {
        Picker projectPicker = getPicker(JIRA_PROJECT_PICKER_ID);
        projectPicker.chooseOption(projectName);
        Poller.waitUntil(projectPicker.getSelectedOption().timed().getText(), Matchers.containsString(projectName),
                Poller.by(20000));
        return this;
    }

    public CreateIssueDialog selectIssueType(String issueTypeName)
    {
        showIssueTypeOptionList();
        chooseIssueType(issueTypeName);
        return this;
    }

    public CreateIssueDialog showIssueTypeOptionList()
    {
        Picker issueTypePicker = getPicker(JIRA_ISSUE_TYPE_PICKER_ID);
        issueTypePicker.waitUntilEnable().openDropdown();
        return this;
    }

    public CreateIssueDialog chooseIssueType(String issueTypeName)
    {
        Picker issueTypePicker = getPicker(JIRA_ISSUE_TYPE_PICKER_ID);
        issueTypePicker.chooseOption(issueTypeName);
        return this;
    }

    public TimedQuery<Boolean> isSubmitButtonDisabled()
    {
        return submitButton.timed().hasAttribute("disabled", "true");
    }

    public CreateIssueDialog clickSubmitButton()
    {
        // need wait submit button is enabled for checking required fields
        Poller.waitUntilTrue(submitButton.timed().isEnabled());
        submitButton.click();
        return this;
    }

    public CreateIssueDialog clickCancelButton()
    {
        cancelButton.click();
        return this;
    }

    public CreateIssueDialog clickEditLink()
    {
        editLink.click();
        return this;
    }

    public CreateIssueDialog setSummary(String summary)
    {
        summaryField.clear().type(summary);
        return this;
    }

    public CreateIssueDialog setDescription(String description)
    {
        descriptionField.clear().type(description);
        return this;
    }

    public JiraIssueMessage getJiraIssueMessage() 
    {
        return pageBinder.bind(JiraIssueMessage.class);
    }

    public EpicLinkIssue getEpicLinkIssue()
    {
        return pageBinder.bind(EpicLinkIssue.class);
    }

    public boolean isIconVisibleInOptionList(String iconUrl)
    {
        String iconsSelector = ".select2-results .select2-result-label img";

        List<PageElement> icons = pageElementFinder.findAll(By.cssSelector(iconsSelector));
        for (PageElement pageElement : icons)
        {
            String imgSrc = pageElement.getAttribute("src");
            if (imgSrc.contains(iconUrl))
            {
                return true;
            }
        }
        return false;
    }

    public boolean isProjectIconVisible(String iconUrl)
    {
        return isIconVisibleInCreateIssueDialog("select-project-container", iconUrl);
    }

    public boolean isIssueTypeIconVisible(String iconUrl)
    {
        return isIconVisibleInCreateIssueDialog("select-issuetype-container", iconUrl);
    }

    private boolean isIconVisibleInCreateIssueDialog(String selectClass, String iconUrl)
    {
        String iconSelector = "." + selectClass + " .select2-chosen img";

        PageElement icon = pageElementFinder.find(By.cssSelector(iconSelector));
        return icon.getAttribute("src").contains(iconUrl);
    }

    public boolean isSuggestionPanelVisible()
    {
        return suggestionPanel.isVisible();
    }

    public boolean isSuggestionPanelPresent()
    {
        return suggestionPanel.isPresent();
    }

    public String getRemainingIssuesMessage() 
    {
       if(remainingIssues.isPresent()) 
       {
           return remainingIssues.getText();
       }
        return "";
    }

    public String getUrlIssueTypeIcon()
    {
        return issueTypeImage.getAttribute("src");
    }

    public String getDescriptionText()
    {
        return this.descriptionField.getText();
    }

    public String getDescriptionPlaceholder()
    {
        return this.descriptionField.getAttribute("placeholder");
    }

    public PageElement getIssueContent()
    {
        return this.issueContent;
    }
    
    public String getSortedWarningMessage()
    {
        return sortedTableWarningMessage.getText();
    }

    public boolean isFormMessageVisible()
    {
        return formMessages.isVisible();
    }

    public TimedQuery<String> getFormMessage()
    {
        return formMessages.timed().getText();
    }

    public boolean isDescriptionFieldVisible()
    {
        return descriptionField.isVisible();
    }

    public void close() {
        closeDialogButton.click();
    }

    public TimedQuery<Boolean> isDialogTargetPresent()
    {
        return dialogTarget.timed().isPresent();
    }
}
