package it.com.atlassian.confluence.plugins.jiracreate.rest;

import com.atlassian.confluence.it.RestHelper;
import com.atlassian.confluence.it.User;
import com.sun.jersey.api.client.WebResource;

public class CreateJiraContentRestHelper
{
    public static final String CREATE_ISSUE_METADATA_REST_PATH = "/rest/createjiracontent/1.0/metadata";

    public static void setDiscoveredred(String baseUrl, User user)
    {
        String url = baseUrl + CREATE_ISSUE_METADATA_REST_PATH + "/discovered";
        WebResource webResource = RestHelper.newResource(url, user);
        RestHelper.doPut(webResource, "");
    }
}
