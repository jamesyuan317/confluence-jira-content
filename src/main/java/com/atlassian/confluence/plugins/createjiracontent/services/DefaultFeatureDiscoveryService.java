package com.atlassian.confluence.plugins.createjiracontent.services;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.confluence.plugins.createjiracontent.entities.FeatureDiscovery;
import com.atlassian.confluence.user.ConfluenceUser;

/**
 * TODO: copied implementation from https://stash.atlassian.com/projects/CONF/repos/confluence-jira-metadata/pull-requests/28/overview
 * remove it when have generic feature discovery service:  https://jira.atlassian.com/browse/CONFDEV-21132
 */
public class DefaultFeatureDiscoveryService implements FeatureDiscoveryService
{
    private final ActiveObjects ao;

    public DefaultFeatureDiscoveryService(final ActiveObjects ao)
    {
        this.ao = ao;
    }

    @Override
    public boolean hasUserDiscovered(ConfluenceUser user)
    {
        FeatureDiscovery discovery = this.findForUser(user);
        return discovery != null ? discovery.getDiscovered() : false;
    }

    @Override
    public void setUserDiscovered(ConfluenceUser user, boolean discovered)
    {
        FeatureDiscovery entity = this.findForUser(user);
        if (entity == null)
        {
            entity = ao.create(FeatureDiscovery.class);
            entity.setUserKey(user.getKey().toString());
        }
        entity.setDiscovered(discovered);
        entity.save();
    }

    private FeatureDiscovery findForUser(ConfluenceUser user)
    {
        FeatureDiscovery[] featureDiscovery = ao.find(FeatureDiscovery.class, "USER_KEY = ?", user.getKey().toString());
        if (featureDiscovery != null && featureDiscovery.length > 0)
        {
            return featureDiscovery[0];
        }

        return null;
    }
}
