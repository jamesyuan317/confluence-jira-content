package com.atlassian.confluence.plugins.createjiracontent.rest.beans;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CachableJiraServerBean
{
    @XmlElement
    protected final String id;

    @XmlElement 
    protected final String name;

    @XmlElement
    protected final boolean selected;

    @XmlElement
    protected final String authUrl;

    @XmlElement
    protected final String url;

    @XmlElement
    protected final Long buildNumber;

    @XmlElement
    protected boolean supportedVersion;

    public CachableJiraServerBean(String id, String url, String name, boolean selected, String authUrl, Long buildNumber)
    {
        super();
        this.id = id;
        this.url = url;
        this.name = name;
        this.selected = selected;
        this.authUrl = authUrl;
        this.buildNumber = buildNumber;
    }

    public String getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public boolean isSelected()
    {
        return selected;
    }

    public String getAuthUrl()
    {
        return authUrl;
    }

    public String getUrl()
    {
        return url;
    }

    public Long getBuildNumber()
    {
        return buildNumber;
    }

    public void setSupportedVersion(boolean supportedVersion)
    {
        this.supportedVersion = supportedVersion;
    }

    public boolean isSupportedVersion()
    {
        return supportedVersion;
    }

}
