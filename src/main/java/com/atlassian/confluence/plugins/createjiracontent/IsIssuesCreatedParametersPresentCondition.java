package com.atlassian.confluence.plugins.createjiracontent;

import com.atlassian.confluence.plugin.descriptor.web.WebInterfaceContext;
import com.atlassian.confluence.plugin.descriptor.web.conditions.BaseConfluenceCondition;
import com.atlassian.confluence.web.context.HttpContext;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * Defines a condition for Web UI modules being shown if some parameters are present in
 * the HTTP request.
 * This condition is used to display a banner in the VIEW content (page, blog post) context,
 * which notify user about JIRA issues have been created.
 */
public class IsIssuesCreatedParametersPresentCondition extends BaseConfluenceCondition
{
    public static final String JIRA_ISSUES_CREATED_REQ_PARAM = "JIRAIssuesCreated";

    public static final String NUM_OF_ISSUES_REQ_PARAM = "numOfIssues";

    public static final String ISSUE_NAME_REQ_PARAM = "issueId";

    public static final String ISSUES_URL_REQ_PARAM = "issuesURL";

    public static final String ADDED_TO_PAGE_REQ_PARAM = "addedToPage";

    public static final String ERROR_MESSAGES_REQ_PARAM = "errorMessages";

    public static final String STATUS_TEXT_REQ_PARAM = "statusText";

    private final HttpContext httpContext;

    public IsIssuesCreatedParametersPresentCondition(HttpContext httpContext)
    {
        this.httpContext = httpContext;
    }

    /**
     * @inheritDoc
     */
    @Override
    protected boolean shouldDisplay(WebInterfaceContext context)
    {
        HttpServletRequest request = httpContext.getRequest();
        return  StringUtils.isNotBlank(request.getParameter(JIRA_ISSUES_CREATED_REQ_PARAM))
                && StringUtils.isNotBlank(request.getParameter(NUM_OF_ISSUES_REQ_PARAM))
                && StringUtils.isNotBlank(request.getParameter(ISSUES_URL_REQ_PARAM))
                && StringUtils.isNotBlank(request.getParameter(ADDED_TO_PAGE_REQ_PARAM));
    }
}