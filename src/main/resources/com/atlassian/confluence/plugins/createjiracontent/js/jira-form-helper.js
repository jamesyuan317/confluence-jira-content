Confluence.CreateJiraContent.FormHelper = (function($) {
    var DEFAULT_OPTION_VALUE = "-1", DEFAULT_LOADING_VALUE = "loading", 
        DEFAULT_OPTION_TEXT = AJS.I18n.getText("createjiracontent.dialog.form.select.default.text"),
        DEFAULT_OPTION_PROJECT_TEXT = AJS.I18n.getText("createjiracontent.dialog.form.select.default.project.text"),
        DEFAULT_OPTION_ISSUETYPE_TEXT = AJS.I18n.getText("createjiracontent.dialog.form.select.default.issuetype.text"),
        LOADING_OPTION_TEXT = AJS.I18n.getText("createjiracontent.dialog.form.select.loading.text"),
        JIRA_REST_URL = Confluence.getContextPath() + "/rest/jiraanywhere/1.0",
        BOTTOM_ARROW_CLASS = 'aui-bottom-arrow-color';
    var epicIssueTypeId = null;
    var hasRequiredFields = false;
    
    var DEFAULT_REQUIRED_FIELDS = ["assignee", "project", "issuetype", "summary", "reporter"];
    // Status of description field
    var currentDescriptionFieldStatus;

    var CREATE_ISSUES_BATCH_SIZE = 10;
    var TIMEOUT_AJAX_CALL = 60*1000; // a minute

    // store the form controls to avoid finding it many times
    var createIssueFormControls;
    var $createIssueForm;
    var defaultIssueTypeNames;

    // XHR object, hold this object to call abort function when needed
    var xhrProject;

    // Declare an instance of Confluence.storageManager
    var storageManager = Confluence.storageManager("jira-content");
    var storedDialogValues;
    // store applink data to avoid call get data again
    var projectsCache = {};
    var schemesCache = {};
    var issueTypesCache = {};
    var jiraServers = {};
    var projectDataCache = {};
    var epicCache = {};
    var epicSummaryText;
    // find and store frequently used controls
    var init = function($form) {
        createIssueFormControls = {
            serverSelect : $form.find("#jira-servers"),
            projectSelect : $form.find("#jira-projects"),
            issueTypeSelect : $form.find("#jira-issue-types"),
            projectSpinner : $form.find("#projectSpinner")
        };
        $createIssueForm = $form;
        // Get default issue type names from i18n for set default display of issue type select box
        defaultIssueTypeNames = AJS.I18n.getText("createjiracontent.dialog.form.issuetype.default.text").toLowerCase();
        defaultIssueTypeNames = defaultIssueTypeNames.split(",");
        //disable issueTypeSelect
        clearAndDisableSelectBox(createIssueFormControls.issueTypeSelect);
        // show description field as first time 
        currentDescriptionFieldStatus = Confluence.CreateJiraContent.JiraIssue.FieldStatus.VISIBLE;
    };

    // Declare name of Storege Key as a constant
    var FORM_VALUE_STORAGE_KEY = "savedFormValues";

    /**
     * By default, press ESC button will reset form in IE, it causes some problem with select2 
     * (value change to default but text holder is still display the old value)
     * this function will prevent form reset in IE. But still keep features: close selectbox, close dialog by using ESC
     */
    var preventEscKeyResetFormInIE = function() {
        if($.browser.msie) {
            $createIssueForm.find("input, textarea").keydown(function(e) {
                if (e.keyCode == 27) { //ESC key
                    return false;
                }
            });
        }
    };

    /*** Select box ****/
    var clearAndAddDefaultOptionForSelectBox = function($selectBox, customizeOptionText) {
        var optionContent = Confluence.CreateJiraContent.createOptionForSelectBox({
            "value" : DEFAULT_OPTION_VALUE,
            "text" : customizeOptionText ? customizeOptionText : DEFAULT_OPTION_TEXT
        });
        $selectBox.html(optionContent);
        $selectBox.auiSelect2("val", DEFAULT_OPTION_VALUE);
    };

    var showLoadingAndDisableProjectSelectBox = function() {
        var $projectSelect = createIssueFormControls.projectSelect;
        $projectSelect.auiSelect2("enable", false);
        var optionContent = Confluence.CreateJiraContent.createOptionForSelectBox({
            "value" : DEFAULT_OPTION_VALUE,
            "text" : LOADING_OPTION_TEXT
        });
        $projectSelect.html(optionContent);
        $projectSelect.auiSelect2("val", LOADING_OPTION_TEXT);
    };

    var enableProjectSelectBox = function() {
        createIssueFormControls.projectSelect.auiSelect2("enable", true);
    };

    var displayErrorLoadingForSelectBox = function($selectBox) {
        clearAndAddDefaultOptionForSelectBox($selectBox);
    };

    /*** Get & load data ***/
    var fillAndDisplayServers = function(servers) {
        var $serverSelect = createIssueFormControls.serverSelect;
        var selectHTML = [];
        $.each(servers, function(index, server) {
            jiraServers[server.id] = server;
            var optionContent = Confluence.CreateJiraContent.createOptionForSelectBox({
                "value" : server.id,
                "text" : server.name
            });
            selectHTML.push(optionContent);
        });
        $serverSelect.append(selectHTML.join(''));
        if (servers.length == 1) {
            $serverSelect.hide();
        } else {
            createSelect2WithTooltip(createIssueFormControls.serverSelect,
                {
                    "containerCssClass": "select-container",
                    "dropdownCssClass": "server-dropdown",
                    "width": "180px",
                    "minimumResultsForSearch": -1
                }
           );
        }
        // If storedDialogValues is defined, bind it into Select
        if (storedDialogValues) {
            setSummaryVisible(setSelectFieldValue($serverSelect, storedDialogValues.serverId));
        } else {
            setSummaryVisible(false);
        }
    };

    var loadServers = function() {
        return $.ajax({
            dataType : "json",
            url : AJS.Confluence.getBaseUrl() + '/rest/createjiracontent/1.0/get-jira-servers'
        }).done(function(servers) {
            if(servers.length) {
                fillAndDisplayServers(servers);
            } else {
                showJiraServerWarning(AJS.I18n.getText("createjiracontent.dialog.form.get.jiraserver.error.empty"));
            }
        });
    };

    // store applink data 
    var storeData = function(serverId, data) {
        projectsCache[serverId] = data.projects;

        // use key data for project
        var projectData = {};
        var projects = data.projects;
        $.each(projects, function(index, project) {
            projectData[project.id] = project;
        });
        projectDataCache[serverId] = projectData;

        // use key data for scheme 
        var schemeData = {};
        var schemes = data.schemes;
        $.each(schemes, function(index, scheme) {
            schemeData[scheme.id] = scheme;
        });
        schemesCache[serverId] = schemeData;

        // use key data for issue type
        var issueTypeData = {};
        var issueTypes = data.types;
        $.each(issueTypes, function(index, issueType) {
            issueTypeData[issueType.id] = issueType;
        });
        issueTypesCache[serverId] = issueTypeData;
    };

    var fillProjectSelectBox = function(projects) {
        var $projectSelect = createIssueFormControls.projectSelect;
        clearAndAddDefaultOptionForSelectBox($projectSelect, DEFAULT_OPTION_PROJECT_TEXT);
        var selectHTML = [];
        $.each(projects, function(index, project) {
            var optionContent = Confluence.CreateJiraContent.createOptionForSelectBox({
                "value" : project.id,
                "text" : project.name
            });
            selectHTML.push(optionContent);
        });
        $projectSelect.append(selectHTML.join(''));
    };

    var loadProjects = function() {
        var serverId = createIssueFormControls.serverSelect.val();
        var serverSupported = verifySupportedServer(jiraServers[serverId]);
        if (serverSupported) {
            var bindStoreValue = function() {
                $serverSelect = createIssueFormControls.serverSelect;
                // Sometime two separate servers have the same projects id, we have to make sure they match both server and issueType before show Summary
                if (storedDialogValues && storedDialogValues.serverId == serverId) {
                    $projectSelect = createIssueFormControls.projectSelect;
                    // If storedDialogValues is defined, bind it into Select
                    var success = setSelectFieldValue($projectSelect, storedDialogValues.projectId);
                    // In-case storedDialogValues is defined, but its value is not found in project list, so that value is not valid
                    setSummaryVisible(success);
                    // If project binding successful, IssueType should be loaded
                    if (success) {
                        loadIssueTypes();
                        checkCreateIssueFields();
                    }
                } else {
                    // Hide and reset IssueType Field
                    setSummaryVisible(false);
                    clearAndDisableSelectBox(createIssueFormControls.issueTypeSelect);
                }
            };

            // If xhrProject is exited, this mean previous ajax hadn't completed yet, call abort() to cancel it.
            if (xhrProject && xhrProject.readyState !== 4 && xhrProject.abort) {
                xhrProject.abort();
            }

            if (projectsCache[serverId]) {
                jiraServerStatus(200);
                fillProjectSelectBox(projectsCache[serverId]);
                bindStoreValue();
                detectEpicIssueOnPage(serverId);
                AJS.trigger('confluence-jira-content.form-updated');
                return $.Deferred().resolve();
            } else {
                showLoadingAndDisableProjectSelectBox();
                xhrProject = AppLinks.makeRequest({
                    appId : serverId,
                    type : "GET",
                    url : "/rest/api/1.0/admin/issuetypeschemes.json",
                    dataType : 'json',
                    success : function(data) {
                        storeData(serverId, data);
                        fillProjectSelectBox(projectsCache[serverId]);
                        enableProjectSelectBox();
                        jiraServerStatus(200, data);
                        bindStoreValue();
                        detectEpicIssueOnPage(serverId);
                    },
                    error : function(xhr) {
                        jiraServerStatus(xhr.status);
                        displayErrorLoadingForSelectBox(createIssueFormControls.projectSelect);
                        enableProjectSelectBox();
                    }
                }).done(function() {
                    AJS.trigger('confluence-jira-content.form-updated');
                });

                return xhrProject;
            }
        }
    };

    var getProjectSchemeId = function(serverId) {
        var projectSelectIndex = createIssueFormControls.projectSelect.prop("selectedIndex");
        if (projectSelectIndex !== 0) {
            return projectsCache[serverId][projectSelectIndex - 1].scheme;
        }
        return null;
    };
    
    // Check issue type name equals with default issue type name  
    var isMatchIssueTypeDefaultName = function(issueTypeName) {
        for ( var i = 0; i < defaultIssueTypeNames.length; i++) {
            if (defaultIssueTypeNames[i] == issueTypeName.toLowerCase()) {
                return true;
            }
        }
        return false;
    };

    var clearAndDisableSelectBox = function($selectBox) {
        $selectBox.auiSelect2("enable", false);
        clearAndAddDefaultOptionForSelectBox($selectBox, DEFAULT_OPTION_ISSUETYPE_TEXT);
        // make selectBox not be focused
        $selectBox.prev().find("a").removeAttr("tabindex");
    };

    var enableSelectBox = function($selectBox) {
        $selectBox.auiSelect2("enable", true);
        $selectBox.prev().find("a").attr("tabindex", "0");
    };

    var loadIssueTypes = function() {
        var projectId = createIssueFormControls.projectSelect.val();
        var $issueTypeSelect = createIssueFormControls.issueTypeSelect;
        if (projectId != DEFAULT_OPTION_VALUE) {
            enableSelectBox($issueTypeSelect);
            fillIssueTypeSelectBox($issueTypeSelect);

            // If storedDialogValues is defined, bind it into Select
            if (storedDialogValues) {
                setSummaryVisible(setSelectFieldValue($issueTypeSelect, storedDialogValues.issueTypeId));
            } else {
                setSummaryVisible(false);
            }
        } else {
            clearAndDisableSelectBox($issueTypeSelect);
        }
        handlingEpicTypeIssue($issueTypeSelect.val());
    };

    var fillIssueTypeSelectBox = function() {
        var addDefaultOption = true;
        var $issueTypeSelect = createIssueFormControls.issueTypeSelect;

        var serverId = createIssueFormControls.serverSelect.val();

        var projectSchemeId = getProjectSchemeId(serverId);
        if (projectSchemeId) {
            var schemeData = schemesCache[serverId];
            var scheme = schemeData[projectSchemeId];

            var schemeIssueTypes = scheme.types;
            var issueTypeData = issueTypesCache[serverId];
            var selectedValue = null;
            var selectHtml = [];
            $(schemeIssueTypes).each(function(index, schemeIssueType) {
                var issueType = issueTypeData[schemeIssueType];
                if (issueType) {
                    if(!selectedValue) {
                        // store issueType id for make default value of issuetype selectbox
                        if(isMatchIssueTypeDefaultName(issueType.name)) {
                            selectedValue = issueType.id;
                        }
                    }
                    var optionContent = Confluence.CreateJiraContent.createOptionForSelectBox({
                        "value" : issueType.id,
                        "text" : issueType.name
                    });
                    selectHtml.push(optionContent);
                    addDefaultOption = false;
                }
            });
            $issueTypeSelect.html(selectHtml.join(''));
            $issueTypeSelect.auiSelect2("val", selectedValue);
        }
        // show default select when data is empty
        if (addDefaultOption) {
            clearAndAddDefaultOptionForSelectBox($issueTypeSelect, DEFAULT_OPTION_ISSUETYPE_TEXT);
        }
    };

    //****** Create issue field functions *******/
    
    var showFormWaiting = function(waitingMessage) {
        enableSubmitButton(false);
        $createIssueForm.find("#form-message-spinner").addClass("aui-icon aui-icon-wait");
        $createIssueForm.find("#form-message").text(waitingMessage);
    };

    var removeFormWaiting = function() {
        $createIssueForm.find("#form-message-spinner").removeClass("aui-icon aui-icon-wait");
        $createIssueForm.find("#form-message").empty();
    };
    
    var showRequiredFieldMessage = function(otherRequiredFields) {
        var $formMessages = $createIssueForm.find("#create-issue-form-messages");
        var message = Confluence.CreateJiraContent.renderRequiredFieldWarningMessage({
            requiredFields: otherRequiredFields.join(", "),
            projectLinkUrl: getCurrentJiraCreateIssueUrl()
        });
        AJS.messages.warning($formMessages, {
            body : message,
            closeable : false
        });
        Confluence.CreateJiraContent.Analytics.sendAnalyticsForRequiredFields();
        Confluence.CreateJiraContent.Analytics.bindAnalyticsForCreateDirectlyInJira();
        $formMessages.removeClass("hidden");
        Confluence.CreateJiraContent.displayDialog.refresh();
    };

    var removeRequiredFieldMessage = function() {
        $createIssueForm.find("#create-issue-form-messages").empty().addClass("hidden");
        $createIssueForm.find("#issue-content, #jira-epic-content").show();
        Confluence.CreateJiraContent.displayDialog.refresh();
    };

    var setFieldPlaceholder = function($field, placeholder){
        $field.attr("placeholder", placeholder);

        if ($field.hasClass("placeholded")) {
            $field.val(placeholder); // IE8 does not support "placeholder" attribute
        }
    }

    var handleDescriptionField = function() {
        var $descriptionField = $createIssueForm.find("#issue-description");
        if (currentDescriptionFieldStatus === Confluence.CreateJiraContent.JiraIssue.FieldStatus.HIDDEN) {
            $descriptionField.parent().hide();
        } else {
            if (currentDescriptionFieldStatus === Confluence.CreateJiraContent.JiraIssue.FieldStatus.REQUIRED) {
                setFieldPlaceholder($descriptionField, AJS.I18n.getText("createjiracontent.dialog.form.text.required.description.placeholder"));
            } else {
                setFieldPlaceholder($descriptionField, AJS.I18n.getText("createjiracontent.dialog.form.text.description.placeholder"));
            }
            $descriptionField.parent().show();
        }
        Confluence.CreateJiraContent.displayDialog.refresh();
    };

    var checkCreateIssueFields = function() {
        removeRequiredFieldMessage();
        if(createIssueFormControls.projectSelect.val() !== DEFAULT_OPTION_VALUE) {
            var onIdentifyFieldsSuccessCallback = function(descriptionFieldStatus, otherRequiredFields) {
                currentDescriptionFieldStatus = descriptionFieldStatus;
                if (otherRequiredFields.length) {
                    hasRequiredFields = true;
                    showRequiredFieldMessage(otherRequiredFields);
                } else {
                    hasRequiredFields = false;
                }
                removeFormWaiting();
                AJS.trigger("confluence-jira-content.form-updated");
            };
            var onIdentifyFieldsErrorCallback = function(errorMessage) {
                removeFormWaiting();
            };

            showFormWaiting(AJS.I18n.getText("createjiracontent.dialog.create.issue.discover.fields.message.checking"));

            Confluence.CreateJiraContent.JiraIssue.identifyCreateIssueFields(createGetFieldsParamObj(),
                    DEFAULT_REQUIRED_FIELDS, onIdentifyFieldsSuccessCallback, onIdentifyFieldsErrorCallback);
        }
    };

    //****** Create issue field functions *******/

    // Check whether selected JIRA Server is supported version
    var verifySupportedServer = function(server) {
        var hiddenItems = $createIssueForm.children().not(".dialog-header, #jiraserver-issue-messages");
        if (!server.supportedVersion) {
            hiddenItems.hide();
            showJiraServerWarning(AJS.format(AJS.I18n.getText('createjiracontent.messages.server.notsupported'), server.url));
        } else {
            hiddenItems.show();
        }

        if ($createIssueForm.find('#text-suggestion').length) {
            $('#inline-dialog-create-issue-dialog > #arrow-create-issue-dialog').addClass(BOTTOM_ARROW_CLASS);
        }

        if (Confluence.CreateJiraContent.displayDialog) {
            Confluence.CreateJiraContent.displayDialog.refresh();
        }
        return server.supportedVersion;
    }
    var bindElementEvents = function() {
        createIssueFormControls.serverSelect.change(function(e) {
            $("#jiraserver-issue-messages").empty(); //clear the error message if have

            var serverSupported = verifySupportedServer(jiraServers[e.val]);
            if (serverSupported) {
                // In case user select other project which not match with our value, storedDialogValues is set to False, Then they re-select a valid one we have to reset storedDialogValues
                storedDialogValues = loadFormValues();
                loadProjects();
                // We only clear and disable Issue Type if storedDialogValues is not defined. If not it will clear all storevalue binding
                if (!storedDialogValues) {
                    clearAndDisableSelectBox(createIssueFormControls.issueTypeSelect);
                    changeIssuesTypeIconForPreviewPanel($createIssueForm.find("#jira-issue-types option:selected").val());
                }
                createIssueFormControls.projectSelect.change(); //trigger the event for projectSelect change.
            }
        });

        createIssueFormControls.projectSelect.change(function(e) {
            // Check if selected Project is match with
            var currentProjectValue = createIssueFormControls.projectSelect.val();
            if (storedDialogValues && currentProjectValue != storedDialogValues.projectId) {
                storedDialogValues = false;
            }
            loadIssueTypes();
            Confluence.CreateJiraContent.Form.fillPreviewIssuesFromTable();
            changeIssuesTypeIconForPreviewPanel($createIssueForm.find("#jira-issue-types option:selected").val());
            AJS.trigger('confluence-jira-content.form-updated');
            checkCreateIssueFields();
        });
        
        createIssueFormControls.issueTypeSelect.change(function(e) {
            changeIssuesTypeIconForPreviewPanel(e.val);
            handlingEpicTypeIssue(e.val);
            AJS.trigger('confluence-jira-content.form-updated');
            checkCreateIssueFields();
        });

        $createIssueForm.on('keyup', '#issue-summary', function(e) {
            AJS.trigger('confluence-jira-content.form-updated');
        });
        
        $createIssueForm.on('keyup', '#issue-description', function(e) {
            AJS.trigger('confluence-jira-content.form-updated');
        });

        if (!Confluence.CreateJiraContent.displayDialog) {
            AJS.bind('confluence-jira-content.form-updated', function() {
                enableSubmitButton(validateCreateIssueForm());
                handleDescriptionField();
            });

            AJS.bind('confluence-jira-content.form-switched', function() {
                handleDescriptionField();
            });
        }

        // Bind Edit Link Event
        $("#jira-content-create-issue-form .dialog-content").on("click", ".summary-stored-value a", function() {
            setSummaryVisible(false);
            storedDialogValues = false;

            return false;
        });
        preventEscKeyResetFormInIE();
    };

    var enableSubmitButton = function(enable) {
        if (enable) {
            $('.create-issue-submit').removeAttr('disabled');
        } else {
            $('.create-issue-submit').attr('disabled', 'disabled');
        }
    };

    /*** Validate ***/

    var validateCreateIssueForm = function() {
        var valid = isFieldValueValid($createIssueForm.find("#jira-projects"), DEFAULT_OPTION_VALUE) &&
            isFieldValueValid($createIssueForm.find("#jira-issue-types"), DEFAULT_OPTION_VALUE);
        valid = valid && !hasRequiredFields;

        if (Confluence.CreateJiraContent.Form.isInTextMode() && valid) {
            var $summaryField = $createIssueForm.find("#issue-summary");
            valid = valid && !$summaryField.hasClass("placeholded") && isFieldValueValid($summaryField, "");
            
            var $descriptionField = $createIssueForm.find("#issue-description");
            if (currentDescriptionFieldStatus === Confluence.CreateJiraContent.JiraIssue.FieldStatus.REQUIRED) {
                valid = valid && !$descriptionField.hasClass("placeholded") && isFieldValueValid($descriptionField, "");
            }
        }
        
        return valid;
    };

    var isFieldValueValid = function($field, invalidValue) {
        if ($.trim($field.val()) === invalidValue) {
            return false;
        }
        return true;
    };
    
    var changeIssuesTypeIconForPreviewPanel = function(issueTypeId) {
        if(issueTypeId != -1) {
            var currentServer = getCurrentServer();
            var issueIconUrl = currentServer.url + issueTypesCache[currentServer.id][issueTypeId].url;
            var images = $createIssueForm.find(".issue-container img").attr("src", issueIconUrl);
        }
    };
    
    /*** Create Jira Issue ***/

    var removeDisplayMessages = function() {
        $("#create-issue-messages", $createIssueForm).empty();
    };

    var disableForm = function() {
        $("#form-spinner", $createIssueForm).addClass("aui-icon aui-icon-wait");
        $(".create-issue-cancel", $createIssueForm).hide();
        $("input,select,textarea,submit", $createIssueForm).disable();
    };

    var enableForm = function() {
        $("#form-spinner", $createIssueForm).removeClass("aui-icon aui-icon-wait");
        $(".create-issue-cancel", $createIssueForm).show();
        $("input,select,textarea,submit", $createIssueForm).enable();
    };

    var getSelectedProjectId = function() {
        return createIssueFormControls.projectSelect.val();
    };

    var addAllFieldsForIssues = function(issues) {

        var projectId = getSelectedProjectId();
        var issueTypeId = createIssueFormControls.issueTypeSelect.val();
        var numOfTruncatedIssues = 0;
        var numOfEditedDescriptions = 0;

        $.each(issues, function(index, issue) {
            if (Confluence.CreateJiraContent.FormTextHelper.isCutLongText(issue.summary)) {
                numOfTruncatedIssues++;
            } else {
                // count number of edited descriptions, but don't count case where truncated summary is place in description
                if (issue.description != "") {
                    numOfEditedDescriptions++;
                }
            }
            issue.projectId = projectId;
            issue.issueTypeId = issueTypeId;
            // try fill issue description when description field is required 
            // by using issue summary field for empty issue description 
            if(currentDescriptionFieldStatus === Confluence.CreateJiraContent.JiraIssue.FieldStatus.REQUIRED && issue.description.length == 0) {
                issue.description = issue.summary;
            }
            // try remove issue description when description field is hidden
            if(currentDescriptionFieldStatus === Confluence.CreateJiraContent.JiraIssue.FieldStatus.HIDDEN) {
                delete issue.description;
            }
        });
        if (numOfTruncatedIssues > 0) {
            Confluence.CreateJiraContent.Analytics.sendAnalyticsForTruncatedSummary(numOfTruncatedIssues);
        }
        if (numOfEditedDescriptions > 0) {
            Confluence.CreateJiraContent.Analytics.sendAnalyticsForDescriptionEdited();
        }
    };

    var buildErrorMessagesWhenCreateFromTable = function(errorIssues, errorIssueRowIndexs) {
        // get error in each issue when create
        var errorMessages = [];
        $.each(errorIssues, function(index, errorIssue) {
            var errorMessage = AJS.I18n.getText("createjiracontent.dialog.create.issues.result.error", (errorIssueRowIndexs[index] + 1),
                    errorIssue.error);
            errorMessages.push(errorMessage);
        });
        return errorMessages;
    };

    var clearCacheInCurrentServer = function() {
        delete projectsCache[createIssueFormControls.serverSelect.val()]; // clear project cached base on serverId
    };
    var jiraServerStatus =  function(statusCode, data) {
        var hiddenItems = $createIssueForm.children().not(".dialog-header, #jiraserver-issue-messages");
        
        //application link is exsited, but do not have any configuration futher
        if (statusCode == 200 && data && data.isEmpty) {
            clearCacheInCurrentServer();
            hiddenItems.addClass("hidden");
            showJiraServerWarning(AJS.format(AJS.I18n.getText('createjiracontent.dialog.form.jiraserver.project.empty'), AJS.escapeHtml(getCurrentServer().name)));
        } else {
            switch (statusCode) {
                case 401: // Unauthorized
                    hiddenItems.addClass("hidden");
                    // OAuthentication handling
                    var selectedServer = getCurrentServer();
                    if (selectedServer && selectedServer.authUrl) {
                        createOauthForm(function() {
                            clearCacheInCurrentServer();
                            hiddenItems.removeClass("hidden");
                            $("#jiraserver-issue-messages").empty();
                            loadProjects();
                        });
                    }
                    break;
                case 404: // not found
                case 504: // proxy timeout
                    hiddenItems.addClass("hidden");
                    showJiraServerWarning(AJS.I18n.getText('createjiracontent.dialog.form.jiraserver.connect.error'));
                    break;
                default: //normal
                    hiddenItems.removeClass("hidden");
            }
        }
        Confluence.CreateJiraContent.displayDialog.refresh();
    };

    var createGetFieldsParamObj = function () {
        return {
            serverId : createIssueFormControls.serverSelect.val(),
            projectId : createIssueFormControls.projectSelect.val(),
            issueTypeId : createIssueFormControls.issueTypeSelect.val()
        };
    };

    var createIssues = function(issues, formObject, tableColumnIndex, issueRowIndexs) {
        var serverId = createIssueFormControls.serverSelect.val();
        var projectId = createIssueFormControls.projectSelect.val();
        var issueType = createIssueFormControls.issueTypeSelect.val();
        
        var $epicCheckbox = $("#epic-link > .checkbox");
        if ($epicCheckbox.is(":checked") && $epicCheckbox.is(":visible")) {
            Confluence.CreateJiraContent.Analytics.sendAnalyticsForCreatedIssueWithEpic();
            Confluence.CreateJiraContent.JiraIssue.resolveEpicField(createGetFieldsParamObj(), function(epicField) {
                //insert epic link in each issue
                var epicValue = $epicCheckbox.val();
                for ( var i = 0; i < issues.length; i++) {
                    var issue = issues[i];
                    if (!issue.fields) {
                        issue.fields = {};
                    }
                    issue.fields[epicField] = epicValue;
                }
                executeCreateJiraIssue(issues, formObject, tableColumnIndex, issueRowIndexs);
            });
        } else {
            executeCreateJiraIssue(issues, formObject, tableColumnIndex, issueRowIndexs);
        }
        
    };

    var executeCreateJiraIssue = function(issues, formObject, tableColumnIndex, issueRowIndexs) {
        addAllFieldsForIssues(issues);
        removeDisplayMessages();
        disableForm();
        var serverId = createIssueFormControls.serverSelect.val();
        var url = JIRA_REST_URL + "/jira-issue/create-jira-issues/" + serverId;
        var createdIssues = [];
        createIssuesBatchJob(url, issues, createdIssues, formObject, tableColumnIndex, issueRowIndexs);
    };

    var createIssuesBatchJob = function(url, issuesToCreate, createdIssues, formObject, tableColumnIndex, issueRowIndexs) {
        var numberOfSliceIssues = (issuesToCreate.length < CREATE_ISSUES_BATCH_SIZE ) ? issuesToCreate.length : CREATE_ISSUES_BATCH_SIZE;

        $.ajax({
            timeout: TIMEOUT_AJAX_CALL,
            type : 'POST',
            contentType : 'application/json',
            url : url,
            data : JSON.stringify(issuesToCreate.slice(0, numberOfSliceIssues)),
            success : function(issues) {
                createdIssues = createdIssues.concat(issues);
                // no more issues will be created in the next time.
                if (issuesToCreate.length <= CREATE_ISSUES_BATCH_SIZE) {
                    if (tableColumnIndex != undefined) {
                        updatePageAfterCreatedIssues(createdIssues, formObject, tableColumnIndex, issueRowIndexs);
                    } else {
                        var firstIssue = createdIssues[0];
                        if (!firstIssue.error) {
                            insertIssueInPage(firstIssue, formObject);
                        } else {
                            Confluence.CreateJiraContent.Analytics.sendAnalyticsForCreateError('rest');
                            refreshPageAfterInsertIssues([], false, [firstIssue.error]);
                        }
                    }
                } else {
                    // call a next issues will be created.
                    createIssuesBatchJob(url, issuesToCreate.slice(numberOfSliceIssues, issuesToCreate.length), createdIssues, formObject, tableColumnIndex, issueRowIndexs);
                }
            },
            error : function(xhr) {
                var errorMessages = [];
                var responseErrorIssues = [];
                if (xhr.status == 400) {
                    try {
                        responseErrorIssues = JSON.parse(xhr.responseText);
                        if (responseErrorIssues instanceof Array) {
                            //in case we have multiple error issue, just display error in first issue.
                            $.each(responseErrorIssues[0].errors, function(field, errorMsg) {
                                errorMessages.push(errorMsg);
                            });
                        } else {
                            errorMessages.push(responseErrorIssues);
                        }
                    } catch (e) {
                        responseErrorIssues = [];
                        errorMessages.push("Parse error:" + e.message);
                    }
                } else {
                    errorMessages.push(AJS.I18n.getText('createjiracontent.dialog.create.issue.error.server'));
                }
                Confluence.CreateJiraContent.Analytics.sendAnalyticsForCreateError('xhr');
                refreshPageAfterInsertIssues(responseErrorIssues, false, errorMessages);
            }
        });
    };

    /**
     * insert created issue single mode
     * @param issue
     * @param: formObject
     */
    var insertIssueInPage = function(issue, formObject) {
        if (formObject.isInSortedTable() && formObject.selectionObject().searchText.numMatches != 1) {
            refreshPageAfterInsertIssues([ issue ], false, []);
            return;
        }
        var insertionBean = Confluence.HighlightAction.createInsertionBean(
            [{xmlInsertion: toIssueMacroXml(issue)}], formObject.selectionObject().searchText);

        Confluence.CreateJiraContent.Analytics.sendAnalyticsForCreatingIssue('text');

        Confluence.HighlightAction.insertContentAtSelectionEnd(insertionBean)
            .done(function(data) {
                if (!data) {
                    Confluence.CreateJiraContent.Analytics.sendAnalyticsForJIMInsertFailed('text', 'algorithm');
                }
                refreshPageAfterInsertIssues([issue], data, []);
            })
            .fail(function(response) {
                Confluence.CreateJiraContent.Analytics.sendAnalyticsForJIMInsertFailed('text', 'server');
                refreshPageAfterInsertIssues([issue], false, [], response.statusText);
            });
    };

    var toIssueMacroXml = function(issue) {
        var serverId = createIssueFormControls.serverSelect.val();
        var serverName = jiraServers[serverId].name;
        var jiraMacro = Confluence.CreateJiraContent.issueMacroXml({
            "showSummary" : false,
            "serverName" : serverName,
            "serverId" : serverId,
            "issueKey" : issue.key
        });
        return jiraMacro;
    };

    /**
     * insert created issue from creating issue in table mode
     * @param paramObj: {createdIssueRowIndexes, createdIssues, errorMessages, formObject, tableColumnIndex}
     */
    var insertCreatedIssuesInPage = function(paramObj) {
        if (paramObj.formObject.isInSortedTable()) {
            refreshPageAfterInsertIssues(paramObj.createdIssues, false, paramObj.errorMessages);
            return;
        }
        
        var cellXmlInsertions = [];
        // Prepare jira macro xml content for insert into table and created
        // issues messages
        $.each(paramObj.createdIssues, function(index, createdIssue) {
            var cellXmlInsertion = {};
            cellXmlInsertion.rowIndex = paramObj.createdIssueRowIndexes[index];
            cellXmlInsertion.xmlInsertion = toIssueMacroXml(createdIssue);
            cellXmlInsertions.push(cellXmlInsertion);
        });

        var tableInsertionBean = Confluence.HighlightAction.createTableInsertionBean(cellXmlInsertions, paramObj.tableColumnIndex,
            paramObj.formObject.selectionObject().searchText);

        Confluence.CreateJiraContent.Analytics.sendAnalyticsForCreatingIssue('table', paramObj.createdIssues.length);

        Confluence.HighlightAction.insertContentsInTableColumnCells(tableInsertionBean)
            .done(function(data) {
                if (!data) {
                    Confluence.CreateJiraContent.Analytics.sendAnalyticsForJIMInsertFailed('table', 'algorithm');
                }
                refreshPageAfterInsertIssues(paramObj.createdIssues, data, paramObj.errorMessages);
            }).fail(function(response) {
                Confluence.CreateJiraContent.Analytics.sendAnalyticsForJIMInsertFailed('table', 'server');
                refreshPageAfterInsertIssues(paramObj.createdIssues, false, paramObj.errorMessages, response.statusText);
            });
    };

    var refreshPageAfterInsertIssues = function(createdIssues, addedToPage, errorMessages, statusText) {
        var issuesURL = getCurrentServer().url;
        if (createdIssues.length == 1) {
            issuesURL += "/browse/" + createdIssues[0].key;
        } else {
            var jql = "key in (";
            jql += $.map(createdIssues, function(issue) {
                return issue.key;
            }).join(",");
            jql += ")";
            issuesURL += "/issues/?jql=" + encodeURIComponent(jql);
        }
        var currentPath = window.location.href.split("#")[0];
        var oldJIRAIssueCreatedIdx = currentPath.indexOf("JIRAIssuesCreated");
        if (oldJIRAIssueCreatedIdx > 0) {
            currentPath = currentPath.substring(0, oldJIRAIssueCreatedIdx - 1);
        }
        var reloadURL = currentPath
            + (currentPath.indexOf("?") > 0 ? "&" : "?")
            + "JIRAIssuesCreated=true"
            + "&numOfIssues=" + createdIssues.length
            + "&issuesURL=" + encodeURIComponent(issuesURL)
            + "&addedToPage=" + addedToPage;
        if (createdIssues.length > 0) {
            reloadURL += "&issueId=" + encodeURIComponent(createdIssues[0].key);
        }
        if(statusText != undefined) {
            reloadURL += "&statusText=" + statusText;
        }
        $.each(errorMessages, function(index, message) {
            reloadURL += "&errorMessages=" + message;
        });
        $("#form-spinner").removeClass("aui-icon aui-icon-wait");
        Confluence.CreateJiraContent.displayDialog.hide();
        window.location.replace(reloadURL);
    };

    var updatePageAfterCreatedIssues = function(issues, formObject, tableColumnIndex, issueRowIndexes) {
        var errorIssues = [], errorIssueRowIndexes = [];
        var createdIssues = [], createdIssueRowIndexes = [];
        // find created issues and issues can not create
        $.each(issues, function(index, issue) {
            if (!issue.error) {
                createdIssues.push(issue);
                createdIssueRowIndexes.push(issueRowIndexes[index]);
            } else {
                errorIssues.push(issue);
                errorIssueRowIndexes.push(issueRowIndexes[index]);
            }
        });
        var errorMessages = buildErrorMessagesWhenCreateFromTable(errorIssues, errorIssueRowIndexes);
        if (createdIssues.length) {
            insertCreatedIssuesInPage({
                createdIssues: createdIssues,
                createdIssueRowIndexes: createdIssueRowIndexes,
                tableColumnIndex: tableColumnIndex,
                formObject: formObject,
                errorMessages: errorMessages});
        } else if (errorIssues.length) {
            refreshPageAfterInsertIssues([], false, errorMessages);
        }
    };

    var tooltipFormat = function(result) {
        return Confluence.CreateJiraContent.selectOption({"optionValue": result.text});
    };

    var issueTypeSelectFormat = function(result) {
        var issueTypeImageUrl;
        if (result.id >= 0) {
            // get issue type data from cache
            var currentServer = getCurrentServer();
            var issueTypeData = issueTypesCache[currentServer.id];
            var issueType = issueTypeData[result.id];

            // get issue type icon url
            issueTypeImageUrl = issueType.url;
            // check issue type icon can use external image link - begin with http 
            if (issueTypeImageUrl.indexOf("http") != 0) {
                issueTypeImageUrl = currentServer.url + issueTypeImageUrl;
            }
        }

        return Confluence.CreateJiraContent.selectOptionWithImage({
            "optionValue" : result.text,
            "imageUrl" : issueTypeImageUrl
        });
    };

    var projectSelectFormat = function(result) {
        var projectAvatarUrl;
        if (result.id >= 0) {
            var projectId = result.id;
            var currentServer = getCurrentServer();
            var projectData = projectDataCache[currentServer.id];
            // get project avatarId from cache data
            var projectAvatarId = projectData[projectId].img;
            // build project avatar url
            projectAvatarUrl = "/secure/projectavatar?pid=" + projectId + 
                                "&avatarId=" + projectAvatarId + "&size=small";
            // use applink proxy get project avatar
            projectAvatarUrl = Confluence.getContextPath() + "/plugins/servlet/applinks/proxy" + 
                            "?path=" + encodeURIComponent(projectAvatarUrl) + "&appId=" + currentServer.id;
        }

        return Confluence.CreateJiraContent.selectOptionProject({
            "optionValue": result.text,
            "imageUrl": projectAvatarUrl
        });
    };

    function createSelect2WithTooltip($element, config) {
        var defaultConfig = {
            escapeMarkup: function(markup) {
                // Keep this for now. 
                // Although automatic escaping of HTML in Confluence Select2 library is now disabled and this code currently does not do anything, 
                // it will be needed if the select2 library is updated and re-enabled the autoescaping in the future.
                return markup;
            },
            formatResult: tooltipFormat,
            formatSelection: tooltipFormat
        };
        $element.auiSelect2($.extend(defaultConfig, config));
    }

    var getCurrentServer = function() {
        var serverId = createIssueFormControls.serverSelect.val();
        var selectedServer = jiraServers[serverId];
        return selectedServer;
    };

    var getCurrentProject = function() {
        var projectData = projectDataCache[createIssueFormControls.serverSelect.val()];
        return projectData[createIssueFormControls.projectSelect.val()];
    };

    /**
     * get value in component without caring placeholder value
     * useful only with IE8
     * @param $inputComponent jquery component
     */
    var getValueWithoutPlaceHolder = function($inputComponent) {
        if($inputComponent.hasClass("placeholded")) {
            return "";
        }
        return $.trim($inputComponent.val());
    };

    var getCurrentJiraCreateIssueUrl = function() {
        var jiraCreateIssueUrl = getCurrentServer().url + "/secure/CreateIssueDetails!Init.jspa?pid="
                + getCurrentProject().id + "&issuetype=" + createIssueFormControls.issueTypeSelect.val();
        var summaryValue = getValueWithoutPlaceHolder($createIssueForm.find("#issue-summary"));
        if (summaryValue.length) {
            jiraCreateIssueUrl = jiraCreateIssueUrl + "&summary=" + encodeURIComponent(summaryValue);
        }
        if (currentDescriptionFieldStatus !== Confluence.CreateJiraContent.JiraIssue.FieldStatus.HIDDEN) {
            var descriptionValue = getValueWithoutPlaceHolder($createIssueForm.find("#issue-description"));
            if (descriptionValue.length) {
                jiraCreateIssueUrl = jiraCreateIssueUrl + "&description=" + encodeURIComponent(descriptionValue);
            }
        }
        return jiraCreateIssueUrl;
    };

    var showJiraServerWarning = function(message) {
        AJS.messages.warning($("#jiraserver-issue-messages"), {
            body : message,
            closeable : false
        });

        if (!$createIssueForm.find('#text-suggestion').is(':visible')) {
            $('#inline-dialog-create-issue-dialog > #arrow-create-issue-dialog').removeClass(BOTTOM_ARROW_CLASS);
        }
    };
    
    var createOauthForm = function(successCallback){
        var selectedServer = getCurrentServer();
        var oauthCallbacks = {
            onSuccess : function() {
                selectedServer.authUrl = null;
                successCallback();
            },
            onFailure : function() {
            }
        };
        showJiraServerWarning(Confluence.CreateJiraContent.getOAuthMessage({'confluenceApplicationName' : 'Confluence'}));
        AJS.$('.oauth-init', $('#jiraserver-issue-messages')).click(function(e){
            AppLinks.authenticateRemoteCredentials(selectedServer.authUrl, oauthCallbacks.onSuccess, oauthCallbacks.onFailure);
            e.preventDefault();
        });
    };

    var bindHideEventToDialog = function() {
        createIssueFormControls.serverSelect.auiSelect2("close");
        createIssueFormControls.projectSelect.auiSelect2("close");
        createIssueFormControls.issueTypeSelect.auiSelect2("close");

        // Save current Value for next showing if the dialog is hidden by nature, not by clicking on Cancel button.
        if(Confluence.CreateJiraContent.displayDialog.isCancelButtonClicked !== true) {
            saveFormValues({
                serverId: createIssueFormControls.serverSelect.val(),
                projectId: createIssueFormControls.projectSelect.val(),
                issueTypeId: createIssueFormControls.issueTypeSelect.val()
            });
        }
    };
    /**
     * Storing input value of creating issue and display it for next time open dialog
     **/
    var saveFormValues = function(values) {
        storageManager.setItem(FORM_VALUE_STORAGE_KEY, JSON.stringify(values));
    };

    /**
     * Return form's value as Object
     * FALSE if values is null or invalid
     * StoredValue Object if it's valid
     *
     * @param String key
     */
    var loadFormValues = function(key) {
        // Json string may not valid, so it would be good to wrap it inside a try/catch
        try {
            key = key || FORM_VALUE_STORAGE_KEY;
            var selectedValue = JSON.parse(storageManager.getItem(key));

            if (selectedValue && selectedValue.serverId && selectedValue.projectId && selectedValue.issueTypeId) {
                return selectedValue;
            } else {
                return false;
            }
        } catch(err) {
            return false;
        }
    };

    /**
     * Set True to show SelectedValue and Hide two dropdowns
     * @param boolean visible
     */
    var setSummaryVisible = function(visible) {
        var $projectAndType = $createIssueForm.find(".dialog-content > .main-field"),
            $summaryEl = $createIssueForm.find(".dialog-content .summary-stored-value");

        if (visible) {
            var selectedProjectId = createIssueFormControls.projectSelect.val();
            var selectedIssueTypeId = createIssueFormControls.issueTypeSelect.val();
            // We only call summaryContent and show Summary when Project Select has already selected
            if (selectedProjectId != -1 && selectedProjectId != null && selectedIssueTypeId != -1 && selectedIssueTypeId != -1) {
                var summaryContent = extractSummaryContent();
                if (summaryContent) {
                    // bind text into summary
                    $summaryEl.remove();

                    var summaryHTML = Confluence.CreateJiraContent.summaryStoredValue({
                        storedValue: summaryContent
                    });
                    $(summaryHTML).insertBefore($createIssueForm.find('#issue-content'));
                    $projectAndType.hide();
                }
            }
        } else {
            $projectAndType.show();
            $summaryEl.hide();
        }

        // We have to call refresh to make sure the dialog still look good
        if (Confluence.CreateJiraContent.displayDialog) {
            Confluence.CreateJiraContent.displayDialog.refresh();
        }
    };

    /**
     * Extract information for Summary Div
     * @returns Object
     * Object = {
     *     projectText: <STRING>,
     *     issueTypeIcon: <STRING>,
     *     projectIcon: jQuery Object,
     *     issueTypeIcon: jQuery Object
     * }
     */
    var extractSummaryContent = function(){
        var projectSelect = createIssueFormControls.projectSelect,
            issueTypeSelect = createIssueFormControls.issueTypeSelect;

        // get project name and issueType name from select
        var projectText = projectSelect.auiSelect2('data').text,
            issueTypeText = issueTypeSelect.auiSelect2('data').text;

        // get Icon
        var $projectIcon = projectSelect.auiSelect2("container").find('.select2-chosen img'),
            $issueTypeIcon = issueTypeSelect.auiSelect2("container").find('.select2-chosen img');

        if (projectText && issueTypeText && $projectIcon && $issueTypeIcon) {
            return {
                projectText: projectText,
                issueTypeText: issueTypeText,
                projectIcon: $projectIcon.attr("src"),
                issueTypeIcon: $issueTypeIcon.attr("src")
            };
        } else {
            return null;
        }
    };

    /**
     * Check Select control if it has an option with special value or not
     * @param jQueryObject $select
     * @param String/Integer value
     * @returns True/False
     */
    var isOptionAvailable = function($select, value) {
        return ($select.find('option[value="'+ value +'"]').length);
    };

    /**
     * Set selected value into a Select2 control
     * @param jQueryObject $select
     * @param String/Integer value
     * @returns {boolean}
     */
    var setSelectFieldValue = function($select, value) {
        if (isOptionAvailable($select, value)) {
            $select.auiSelect2("val", value);
            return true;
        } else {
            storedDialogValues = false;
            return false;
        }
    };

    /**
     * Try to resolve epicIssueType and detect epicLink on page 
     */
    var detectEpicIssueOnPage = function(serverId) {
        var onSuccess = function(issueTypeId) {
            epicIssueTypeId = issueTypeId;
            epicLinkJiraIssuePageHandling(serverId);
        };
        var onError = function() {
            if (serverId == getCurrentServer().id) {
                epicIssueTypeId = null;
                $('#jira-epic-content').hide();
            }
        };
        Confluence.CreateJiraContent.JiraIssue.resolveEpicIssueType(serverId, onSuccess, onError);
    };
    
    /**
     * Detect if there is any Epic issue on the page with match with server selection
     * If have 1 ->display on Create-Jira-Issue dialog
     * If 0 or more than 2: ignore it
     */
    var epicLinkJiraIssuePageHandling = function(serverId) {
        var $jiraEpicContent = $('#jira-epic-content');
        if (epicCache[serverId] != null) {
            $jiraEpicContent.html(epicCache[serverId]);
            addEpicLinkTooltip($jiraEpicContent);
            return;
        }
        $.ajax({
            type : "GET",
            contentType : "application/json",
            url : AJS.Confluence.getBaseUrl() + '/rest/createjiracontent/1.0/find-epic-issue',
            data : {
                pageId : AJS.params.pageId,
                serverId : serverId,
                epicIssueTypeId: epicIssueTypeId
            },
            success : function(epicData) {
                if (!epicData.epicKey) {
                    $jiraEpicContent.empty();
                    epicCache[serverId] = "";
                    return;
                }
                var epicIssueHtml = Confluence.CreateJiraContent.renderEpicContent({
                    epicKey: epicData.epicKey,
                    epicHtmlPlaceHolder : epicData.epicHtmlPlaceHolder
                });
                $jiraEpicContent.html(epicIssueHtml);
                // get summary data before it removed out dom.
                epicSummaryText = $jiraEpicContent.find(".summary").text();
                //remove summary, status out of epic render
                var epicEl = $jiraEpicContent.find('a:first-child').clone();
                $jiraEpicContent.find('.jira-issue').html(epicEl);
                epicCache[serverId] = $jiraEpicContent.html();
                addEpicLinkTooltip($jiraEpicContent);
                Confluence.CreateJiraContent.displayDialog.refresh();
            },
            error : function(xhr) {
                $jiraEpicContent.empty();
                AJS.logError('confluence-jira-content:epicLinkJiraIssuePageHandling - Error when detect epic on page with status=' + xhr.status);
            }
        });

    };

    var addEpicLinkTooltip = function($jiraEpicContent) {
        $jiraEpicContent.find(".jira-issue").attr("title", epicSummaryText).tooltip({gravity: 'sw'});
    }

    /**
     * Hide epic link suggestion when select epic issue type.
     */
    var handlingEpicTypeIssue = function(issueType) {
        if (issueType == epicIssueTypeId) {
            $('#jira-epic-content').hide();
        } else {
            $('#jira-epic-content').show();
        }
    };
    
    var removeDialogLoading = function() {
        $createIssueForm.find("#create-issue-loading").remove();
    };

    var handleServerLoadingSuccess = function() {
        removeDialogLoading();
        var serverId = createIssueFormControls.serverSelect.val();
        // do not load project when server is empty
        if(serverId) {
            loadProjects();
            // display create isue dialog content when finished loading server data
            $createIssueForm.find("#jira-servers, .dialog-content, #prepare-issue-messages, .buttons-group, #text-suggestion").removeClass("hidden");
        }
    };

    var handleServerLoadingError = function() {
        removeDialogLoading();
        showJiraServerWarning(AJS.I18n.getText("createjiracontent.dialog.form.get.jiraserver.error"));
    };

    return {
        init : function($form) {
            init($form);
            createSelect2WithTooltip(createIssueFormControls.projectSelect,
                {
                    "containerCssClass": "select-container select-project-container",
                    "dropdownCssClass": "projects-dropdown",
                    "width": "360px",
                    "formatSelection": projectSelectFormat,
                    "formatResult": projectSelectFormat
                }
            );
            createSelect2WithTooltip(createIssueFormControls.issueTypeSelect,
                {
                    "containerCssClass": "select-container select-issuetype-container",
                    "dropdownCssClass": "issue-types-dropdown",
                    "width": "360px",
                    "minimumResultsForSearch": -1,
                    "formatSelection": issueTypeSelectFormat,
                    "formatResult": issueTypeSelectFormat
                }
            );

            enableSubmitButton(false);
            clearAndDisableSelectBox(createIssueFormControls.issueTypeSelect);
            storedDialogValues = loadFormValues(FORM_VALUE_STORAGE_KEY);
            bindElementEvents();
            setSummaryVisible(storedDialogValues !== false);
            loadServers().pipe(handleServerLoadingSuccess, handleServerLoadingError);
        },
        createIssues: createIssues,
        validateCreateIssueForm: validateCreateIssueForm,
        removeDisplayMessages: removeDisplayMessages, 
        createSelect2WithTooltip: createSelect2WithTooltip,
        bindHideEventToDialog: bindHideEventToDialog,
        changeIssuesTypeIconForPreviewPanel: changeIssuesTypeIconForPreviewPanel,
        loadFormValues: loadFormValues,
        BOTTOM_ARROW_CLASS: BOTTOM_ARROW_CLASS
    };
})(AJS.$);
