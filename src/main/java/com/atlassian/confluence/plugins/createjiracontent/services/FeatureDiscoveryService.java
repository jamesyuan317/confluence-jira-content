package com.atlassian.confluence.plugins.createjiracontent.services;

import com.atlassian.activeobjects.tx.Transactional;
import com.atlassian.confluence.user.ConfluenceUser;

/**
 * TODO: copied implementation from https://stash.atlassian.com/projects/CONF/repos/confluence-jira-metadata/pull-requests/28/overview
 * remove it when have generic feature discovery service:  https://jira.atlassian.com/browse/CONFDEV-21132
 */
@Transactional
public interface FeatureDiscoveryService
{
    boolean hasUserDiscovered(ConfluenceUser user);

    void setUserDiscovered(ConfluenceUser user, boolean discovered);
}
