package com.atlassian.confluence.plugins.createjiracontent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.application.jira.JiraApplicationType;
import com.atlassian.applinks.api.auth.Anonymous;
import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.confluence.plugins.createjiracontent.cache.CacheKey;
import com.atlassian.confluence.plugins.createjiracontent.rest.beans.CachableJiraServerBean;
import com.atlassian.sal.api.net.Request.MethodType;
import com.atlassian.sal.api.net.ResponseStatusException;

public class CachedJiraResourcesManager implements JiraResourcesManager
{
    private static final Logger LOG = LoggerFactory.getLogger(CachedJiraResourcesManager.class);
    private static final String REST_URL_SERVER_INFO = "/rest/api/2/serverInfo";
    private static final String JSON_PATH_BUILD_NUMBER = "buildNumber";
    private static final Long NOT_SUPPORTED_BUILD_NUMBER = -1L;
    private final CacheManager cacheManager;
    private final ApplicationLinkService appLinkService;
    private final String cacheName;
    private final Long defaultMinBuildNumber;

    public CachedJiraResourcesManager(ApplicationLinkService applicationLinkService, CacheManager cacheManager, Long minBuildNumber)
    {
        this.appLinkService = applicationLinkService;
        this.cacheManager = cacheManager;
        this.cacheName = this.getClass().getCanonicalName();
        this.defaultMinBuildNumber = minBuildNumber;
    }

    public List<CachableJiraServerBean> getJiraServers()
    {
        return Collections.unmodifiableList(getServersFromAppLinks());
    }

    public List<CachableJiraServerBean> getSupportedJiraServers()
    {
        List<CachableJiraServerBean> servers = getServersFromAppLinks();

        List<CachableJiraServerBean> result = new ArrayList<CachableJiraServerBean>();
        for (CachableJiraServerBean server: servers)
        {
            if (server.isSupportedVersion())
            {
                result.add(server);
            }
        }

        return Collections.unmodifiableList(result);
    }

    private List<CachableJiraServerBean> getServersFromAppLinks()
    {
        Iterable<ApplicationLink> appLinks = appLinkService.getApplicationLinks(JiraApplicationType.class);
        List<CachableJiraServerBean> servers = new ArrayList<CachableJiraServerBean>();

        if (appLinks != null)
        {
            for (ApplicationLink link : appLinks)
            {
                String appId = link.getId().toString();
                CacheKey key = new CacheKey(appId);
                Cache cache = cacheManager.getCache(cacheName);
                CachableJiraServerBean server = (CachableJiraServerBean) cache.get(key);
                if (server == null)
                {
                    server = getRemoteJiraServer(link);
                    if (server != null) {
                        cache.put(key, server);
                    }
                }

                if (server != null) // check supported and add to the list only if we can get server either from cache or remote
                {
                    boolean serverSupported = server.getBuildNumber().compareTo(defaultMinBuildNumber) > 0 ? true : false;
                    server.setSupportedVersion(serverSupported);
                    servers.add(server);
                }
            }
        }

        return servers;
    }

    private CachableJiraServerBean getRemoteJiraServer(ApplicationLink appLink)
    {
        if (appLink != null)
        {
            String authUrl = null;
            try
            {
                appLink.createAuthenticatedRequestFactory().createRequest(MethodType.GET, "");
            }
            catch (CredentialsRequiredException e)
            {
                // if an exception is thrown, we need to prompt for oauth
                authUrl = e.getAuthorisationURI().toString();
            }

            Long buildNumber = getServerBuildNumber(appLink);
            CachableJiraServerBean serverBean = new CachableJiraServerBean(appLink.getId().toString(),
                    appLink.getRpcUrl().toString(), appLink.getName(), appLink.isPrimary(), authUrl, buildNumber);
            return serverBean;
        }

        return null;
    }

    private Long getServerBuildNumber(ApplicationLink appLink)
    {
        Long buildNumber = Long.MAX_VALUE;
        try
        {
            ApplicationLinkRequest request = createRequest(appLink, MethodType.GET, REST_URL_SERVER_INFO);
            request.addHeader("Content-Type", MediaType.APPLICATION_JSON);
            String responseString = request.execute();
            ObjectMapper mapper = new ObjectMapper();
            JsonNode rootNode = mapper.readTree(responseString);
            return rootNode.path(JSON_PATH_BUILD_NUMBER).getLongValue();
        }
        catch (ResponseStatusException e) // We could connect to JIRA Server but the REST URL provided is version 4.x
        {
            buildNumber = NOT_SUPPORTED_BUILD_NUMBER;
            LOG.warn(e.getMessage());
        }
        catch (Exception e) // In other cases we assume that it is supported version
        {
            LOG.warn(e.getMessage());
        }

        return buildNumber;
    }

    private ApplicationLinkRequest createRequest(ApplicationLink appLink, MethodType methodType, String baseRestUrl) throws CredentialsRequiredException 
    {
        String url = appLink.getRpcUrl() + baseRestUrl;
        ApplicationLinkRequestFactory requestFactory = appLink.createAuthenticatedRequestFactory();
        ApplicationLinkRequest request = null;
        try
        {
            request = requestFactory.createRequest(methodType, url);
        }
        catch (CredentialsRequiredException e)
        {
            requestFactory = appLink.createAuthenticatedRequestFactory(Anonymous.class);
            request = requestFactory.createRequest(methodType, url);
        }

        return request;
    }

}