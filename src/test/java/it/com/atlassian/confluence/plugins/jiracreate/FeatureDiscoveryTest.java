package it.com.atlassian.confluence.plugins.jiracreate;

import it.com.atlassian.confluence.plugins.jiracreate.pageobjects.CreateIssueDialog;
import it.com.atlassian.confluence.plugins.jiracreate.pageobjects.FeatureDiscoveryDialog;
import junit.framework.Assert;

import org.junit.Test;

import com.atlassian.confluence.it.User;
import com.atlassian.pageobjects.elements.query.Poller;

public class FeatureDiscoveryTest extends AbstractJIRACreateTest
{
    @Test
    public void displayFeatureDiscoveryDialog()
    {
        product.loginAndView(User.TEST, createPageWithText());
        FeatureDiscoveryDialog featureDiscoveryDialog = openFeatureDiscoveryDialog("#main-content>p", 0, 17);
        // check show Feature Discovery dialog
        Poller.waitUntilTrue(featureDiscoveryDialog.isFeatureDiscoveryDialogVisible());

        // check close Feature Discovery dialog
        featureDiscoveryDialog.close();
        Poller.waitUntilFalse(featureDiscoveryDialog.isFeatureDiscoveryDialogVisible());
    }

    @Test
    public void stillDisplayFeatureDiscoveryDialogWhenClickOutside()
    {
        product.loginAndView(User.TEST, createPageWithText());
        FeatureDiscoveryDialog featureDiscoveryDialog = openFeatureDiscoveryDialog("#main-content>p", 0, 17);
        Poller.waitUntilTrue(featureDiscoveryDialog.isFeatureDiscoveryDialogVisible());

        // click outside by select another text
        featureDiscoveryDialog = openFeatureDiscoveryDialog("#main-content>p", 2, 15);
        // check still show Feature Discovery dialog
        Poller.waitUntilTrue(featureDiscoveryDialog.isFeatureDiscoveryDialogVisible());
    }

    @Test
    public void stillDisplayFeatureDiscoveryDialogWhenRefreshPage()
    {
        product.loginAndView(User.TEST, createPageWithText());
        FeatureDiscoveryDialog featureDiscoveryDialog = openFeatureDiscoveryDialog("#main-content>p", 0, 17);
        Poller.waitUntilTrue(featureDiscoveryDialog.isFeatureDiscoveryDialogVisible());

        // refresh page 
        product.refresh();
        // still show Feature Discovery dialog
        featureDiscoveryDialog = openFeatureDiscoveryDialog("#main-content>p", 2, 15);
        Poller.waitUntilTrue(featureDiscoveryDialog.isFeatureDiscoveryDialogVisible());
    }

    @Test
    public void displayCreateIssueDialogFromFeatureDiscoveryDialog()
    {
        product.loginAndView(User.TEST, createPageWithText());
        FeatureDiscoveryDialog featureDiscoveryDialog = openFeatureDiscoveryDialog("#main-content>p", 0, 17);
        Poller.waitUntilTrue(featureDiscoveryDialog.isFeatureDiscoveryDialogVisible());

        // check show Create Issue dialog when click Show me button
        CreateIssueDialog createIssueDialog = featureDiscoveryDialog.showCreateIssueDialog();
        Poller.waitUntilTrue(createIssueDialog.isCreateIssueDialogVisible());

        // check summary field value had selected text
        Assert.assertEquals("This is test page", createIssueDialog.getSummaryValue());
    }

    @Test
    public void dismissFeatureDiscoveryDialogByClickShowCreateIssueDialog()
    {
        product.loginAndView(User.TEST, createPageWithText());
        FeatureDiscoveryDialog featureDiscoveryDialog = openFeatureDiscoveryDialog("#main-content>p", 0, 17);
        Poller.waitUntilTrue(featureDiscoveryDialog.isFeatureDiscoveryDialogVisible());

        CreateIssueDialog createIssueDialog = featureDiscoveryDialog.showCreateIssueDialog();
        Poller.waitUntilTrue(createIssueDialog.isCreateIssueDialogVisible());

        createIssueDialog.close();
        // dismiss Feature Discovery dialog, show Create Issue dialog
        createIssueDialog = openCreateIssueDialog("#main-content>p", 2, 15);
        Poller.waitUntilTrue(createIssueDialog.isCreateIssueDialogVisible());
        
        // Feature Discovery dialog was dismissed when refresh page
        product.refresh();
        createIssueDialog = openCreateIssueDialog("#main-content>p", 0, 17);
        Poller.waitUntilTrue(createIssueDialog.isCreateIssueDialogVisible());
    }

    @Test
    public void dismissFeatureDiscoveryDialogByClickCloseButton()
    {
        product.loginAndView(User.TEST, createPageWithText());
        FeatureDiscoveryDialog featureDiscoveryDialog = openFeatureDiscoveryDialog("#main-content>p", 0, 17);
        Poller.waitUntilTrue(featureDiscoveryDialog.isFeatureDiscoveryDialogVisible());

        featureDiscoveryDialog.close();

        // dismiss Feature Discovery dialog when click close button
        CreateIssueDialog createIssueDialog = openCreateIssueDialog("#main-content>p", 2, 15);
        Poller.waitUntilTrue(createIssueDialog.isCreateIssueDialogVisible());

        // Feature Discovery dialog was dismissed when refresh page
        product.refresh();
        createIssueDialog = openCreateIssueDialog("#main-content>p", 0, 17);
        Poller.waitUntilTrue(createIssueDialog.isCreateIssueDialogVisible());
    }
}
